FROM nginx

MAINTAINER ALEYRON SYSTEMS INC 

RUN rm -rf /usr/share/nginx/html
COPY build /usr/share/nginx/html
RUN ls -al /usr/share/nginx/html

EXPOSE 80