import React, { Component, Fragment } from 'react';
import './App.css';
import Login from './ui/Login';
import Main from './ui/Main';
import { Dimmer, DimmerDimmable, Loader, Message } from 'semantic-ui-react';
import endpoints from './config/endpoints';
import axios from 'axios/index';
import EasterEgg from 'react-easter';

const toastStyle = {
  position: 'fixed',
  zIndex  : 100000,
  top     : '40px',
  right   : '40px',
  minWidth: '200px'
};

const dimmableStyle = {
  position: 'fixed',
  margin  : 0,
  top     : 0,
  left    : 0,
  width   : '100%',
  height  : '100%',
  zIndex  : 10000000000
};

class App extends Component {
  toast = async (state) => {
    let { toast } = this.state;

    toast = { ...toast, color: 'white', delay: 0 };

    if(state.hasOwnProperty('header')) {
      toast.header = state.header;
    }
    if(state.hasOwnProperty('body')) {
      toast.body = state.body;
    }
    if(state.hasOwnProperty('visible')) {
      toast.visible = state.visible;
    }
    if(state.hasOwnProperty('color')) {
      toast.color = state.color;
    }
    if(state.hasOwnProperty('delay')) {
      if(toast.timeout >= 0) {
        clearTimeout(toast.timeout);
      }
      if(state.delay > 0) {
        toast.timeout = setTimeout(() => this.toast({ visible: false }), state.delay);
      }
    }

    this.setState({ toast });
  };

  constructor(props) {
    super(props);

    this.state       = {
      toast  : {
        visible: false,
        header : '',
        body   : '',
        style  : toastStyle,
        color  : '',
        timeout: -1,
      },
      loading: false,
      utils  : {},
    };
    this.login       = this.login.bind(this);
    this.logout      = this.logout.bind(this);
    this.loading     = this.loading.bind(this);
    this.backToMain  = this.backToMain.bind(this);
    this.toast.error =
        async (header, body) => await this.toast({ visible: true, delay: 3000, color: 'red', header, body });

    //utils
    this.state.utils       = {
      loading   : this.loading,
      login     : this.login,
      logout    : this.logout,
      backToMain: this.backToMain,
      toast     : this.toast,
    };
    this.state.currentPage = (
        <Login onLogin={this.login} utils={this.state.utils} />);
  }

  logout() {
    this.setState({
                    user       : {},
                    currentPage: (<Login onLogin={this.login}
                                         utils={this.state.utils} />),
                  });
  }

  backToMain() {
    this.setState({
                    currentPage: (<Main changePage={(newPage) => {
                      this.setState({ currentPage: newPage });
                    }}
                                        utils={this.state.utils}
                                        logout={this.logout}
                                        user={this.state.user} />),
                  });
  }

  login(user) {
    this.loading(true);
    axios({
            method: 'get',
            url   : endpoints.api.users.getByUsername,
            params: {
              username: user.username
            },
            auth  : user,
          }).then((res) => {
      user.data     = res.data;
      let { utils } = this.state;
      utils.user    = user;
      this.setState({
                      utils: utils,
                      user : user,
                    });
      this.setState({
                      currentPage: (<Main changePage={(newPage) => {
                        this.setState({ currentPage: newPage });
                      }} utils={this.state.utils} logout={this.logout}
                                          user={this.state.user} />),
                    });
      this.loading(false);
    }).catch((e) => {
      console.error(e);
      this.loading(false);
    });
  }

  loading(bool) {
    this.setState({
                    loading: bool,
                  });
  }

  render() {
    let { toast } = this.state;
    return (
        <Fragment>
          <EggRickAstley />
          <EggBoy />
          <DimmerDimmable style={{...dimmableStyle, display: (this.state.loading ? 'block' : 'none')}} blurring dimmed={this.state.loading}>
            <Dimmer active={this.state.loading} inverted>
              <Loader />
            </Dimmer>
          </DimmerDimmable>
          {toast.visible ?
              <Message
                  style={toast.style}
                  onDismiss={() => {this.toast({ visible: false })}}
                  header={toast.header}
                  content={toast.body}
                  color={toast.color}
              /> :
              <Fragment />}
          {this.state.currentPage}
        </Fragment>
    );
  }
}

export default App;

class EggRickAstley extends React.Component {
  render() {
    const konamiCode = [
      'r',
      'a',
      'enter',
    ];

    return (
        <EasterEgg keys={konamiCode}
                   timeout={10000}>
          <div style={{
            position : 'absolute',
            bottom   : '10px',
            left     : 0,
            width    : '100%',
            textAlign: 'center',
            zIndex   : '100',
          }}>
            <iframe src="https://www.youtube.com/embed/DLzxrzFCyOs?autoplay=1"
                    width={'512px'}
                    height={'256px'}
                    frameBorder="0"
                    allowFullScreen
                    title={'egg'} />
          </div>
        </EasterEgg>);
  }
}

class EggBoy extends React.Component {
  render() {
    const konamiCode = [
      'q',
      'w',
      'enter',
    ];

    return (
        <EasterEgg keys={konamiCode}
                   timeout={3000}>
          <div style={{
            position : 'absolute',
            bottom   : '10px',
            left     : 0,
            width    : '100%',
            textAlign: 'center',
            zIndex   : '100',
          }}>
            <iframe width="256px" height="128px"
                    src="https://www.youtube.com/embed/Acjf66Qdj2U?start=10&autoplay=1"
                    frameBorder="0"
                    allowFullScreen
                    title={'egg'} />
          </div>
        </EasterEgg>);
  }
}