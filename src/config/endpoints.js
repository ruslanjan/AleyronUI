import { status } from './env';

export const ip = (
    status === 'DEBUG' ?
        'http://localhost:8080' :
        'https://api.aleyron.com'
);

export const api = {
  login        : `${ip}/login`,
  users        : {
    getByUsername : `${ip}/api/users/getByUsername`,
    getAllWithRole: `${ip}/api/users/getAllWithRole`,
    deleteById    : `${ip}/api/users/deleteById`,
    getById       : `${ip}/api/users/getById`,
    update        : `${ip}/api/users/update`,
    getPage       : `${ip}/api/users/getPage`,
    register      : `${ip}/api/users/register`,
    roles         : {
      deleteById: `${ip}/api/users/roles/deleteById`,
      getById   : `${ip}/api/users/roles/getById`,
      getAll    : `${ip}/api/users/roles/getAll`,
      getByName : `${ip}/api/users/roles/getByName`,
      create    : `${ip}/api/users/roles/create`,
      getPage   : `${ip}/api/users/roles/getPage`,
    },
    privileges    : {
      getAll    : `${ip}/api/users/privileges/getAll`,
      getByName : `${ip}/api/users/privileges/getByName`,
      getById   : `${ip}/api/users/privileges/getById`,
      create    : `${ip}/api/users/privileges/create`,
      deleteById: `${ip}/api/users/privileges/deleteById`,
      getPage   : `${ip}/api/users/privileges/getPage`,
    },
  },
  digital_print: {
    storage      : {
      items     : {
        create                  : `${ip}/api/digital_print/storage/items/create`,
        delete                  : `${ip}/api/digital_print/storage/items/delete`,
        getPage                 : `${ip}/api/digital_print/storage/items/getPage`,
        getAll                  : `${ip}/api/digital_print/storage/items/getAll`,
        getById                 : `${ip}/api/digital_print/storage/items/getById`,
        getAllWithOrderType     : `${ip}/api/digital_print/storage/items/getAllWithOrderType`,
        getTextSearchResultsPage: `${ip}/api/digital_print/storage/items/getTextSearchResultsPage`,
      },
      groups    : {
        add    : `${ip}/api/digital_print/storage/groups/add`,
        delete : `${ip}/api/digital_print/storage/groups/delete`,
        getPage: `${ip}/api/digital_print/storage/groups/getPage`,
        getAll : `${ip}/api/digital_print/storage/groups/getAll`
      },
      operations: {
        perform: `${ip}/api/digital_print/storage/operations/perform`,
      },
      history   : {
        getPage: `${ip}/api/digital_print/storage/history/getPage`,
      },
    },
    order_manager: {
      getDigitalPrintOrderById: `${ip}/api/digital_print/order_manager/getDigitalPrintOrderById`,
      setPrintOrderStatus     : `${ip}/api/digital_print/order_manager/setPrintOrderStatus`,
      getPage                 : `${ip}/api/digital_print/order_manager/getPage`,
    }
  },
  utils        : {
    plate_placement: {
      tasks: {
        create    : `${ip}/api/utils/plate_placement/create`,
        getPage   : `${ip}/api/utils/plate_placement/getPage`,
        deleteById: `${ip}/api/utils/plate_placement/deleteById`,
      }
    }
  },
  manager      : {
    invoices: {
      getPage             : `${ip}/api/manager/invoices/getPage`,
      create              : `${ip}/api/manager/invoices/create`,
      update              : `${ip}/api/manager/invoices/update`,
      cancel              : `${ip}/api/manager/invoices/cancel`,
      form                : `${ip}/api/manager/invoices/form`,
      getById             : `${ip}/api/manager/invoices/getById`,
      getBy1c             : `${ip}/api/manager/invoices/getBy1c`,
      addDigitalPrintOrder: `${ip}/api/manager/invoices/addDigitalPrintOrder`,
      addDesignOrder      : `${ip}/api/manager/invoices/addDesignOrder`,
    },
    orders  : {
      calculatePrintWorkPrice: `${ip}/api/manager/orders/calculatePrintWorkPrice`,
      getById                : `${ip}/api/manager/orders/getById`,
      deleteById             : `${ip}/api/manager/orders/deleteById`,
      getByIds               : `${ip}/api/manager/orders/getByIds`,
      getPage                : `${ip}/api/manager/orders/getPage`,
    }
  },
  booker       : {
    invoices: {
      status : {
        setPaid: `${ip}/api/booker/invoices/status/setPaid`
      },
      getPage: `${ip}/api/booker/invoices/getPage`,
      getById: `${ip}/api/booker/invoices/getById`
    }
  },
  order        : {
    print_work  : {
      create              : `${ip}/api/order/print_work/create`,
      getPage             : `${ip}/api/order/print_work/getPage`,
      getAllPostPrintWork : `${ip}/api/order/print_work/getAllPrintWork`,
      getAll              : `${ip}/api/order/print_work/getAllPrintWork`,
      deletePrintWorkById : `${ip}/api/order/print_work/deletePrintWorkById`,
      getPostPrintWorkById: `${ip}/api/order/print_work/getPrintWorkById`,
      getAllOrderType     : `${ip}/api/order/print_work/getAllOrderType`,
      getAllPrintWorkType : `${ip}/api/order/print_work/getAllPrintWorkType`,
    },
    print_order : {
      create: ip + `/api/order/print_order/create`
    },
    design_order: {
      update: `${ip}/api/order/DesignOrder/update`,
    },
  },
  configs      : {
    create: `${ip}/api/configs/savePriceCalculationConfig`,
    get   : `${ip}/api/configs/getPriceCalculationConfig`,
  },
};
export default { ip, api };
