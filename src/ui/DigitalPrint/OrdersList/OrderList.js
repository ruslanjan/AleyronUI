import React, {Component, Fragment} from 'react'
import {
  Button,
  Dropdown,
  Grid,
  GridColumn,
  GridRow,
  Message,
  Table,
  TableCell,
  TableHeader,
  TableHeaderCell,
  TableRow,
} from 'semantic-ui-react'
import axios from "axios/index";
import endpoints from "../../../config/endpoints";
import OrderRow from "./modals/Rows/OrderRow";
import qs from 'qs';

export default class OrderList extends Component {

  changeType = async (e, {value}) => {
    await this.setState({type: value, page: 0});
    await this.loadPage(0, 12, true);
  };

  constructor(props) {
    super(props);
    this.loadPage = this.loadPage.bind(this);
    this.state = {
      orders: [],
      page: 0,
      success: true,
      type: ''
    };
  }

  async forward(delta_page = 1, size = 12) {
    this.size = size;
    let {page} = this.state;
    await this.loadPage(page + delta_page);
  }

  async back(delta_page = -1, size = 12) {
    this.size = size;
    let {page} = this.state;
    await this.loadPage(Math.max(page + delta_page, 0));
  }

  async loadPage(page = this.state.page, size = 12, isStatusChanged = false) {
    let {success} = this.state;
    await this.props.utils.loading(true);
    try {
      let orders = await axios({
        method: 'get',
        url: endpoints.api.digital_print.order_manager.getPage,
        params: {
          page: page,
          size: size,
          sorts: [('created:DESC')],
          filters: [`type=$eq|${this.state.type}`]
        },
        paramsSerializer: (params) => qs.stringify(params, { arrayFormat: 'brackets' }),
        auth: this.props.utils.user
      });
      if (orders.data.length !== 0 || isStatusChanged || page === 0) {
        await this.setState({orders: []});
        await this.setState({
          orders: orders.data,
          success: success,
          page: page,
        });
      } else {
        await this.back();
      }
    } catch (e) {
      console.error(e);
      await this.setState({success: false});
    } finally {
      await this.props.utils.loading(false);
    }
  }

  render() {
    if (!this.state.success) {
      this.props.utils.toast({
        visible: true,
        header: 'Цифровая печать',
        color: 'red',
        body: 'Не известный ошибка случатся моя чинить, однако твоя не надейся, моя плохо чинить!'
      });
    }
    return (
      <Fragment>
        {
          this.state.success ?
            <Grid container padded='vertically'>
              <GridRow>
                <GridColumn>
                  <Table celled compact definition selectable>
                    <TableHeader fullWidth>
                      <TableRow>
                        <TableHeaderCell colSpan={10}>
                          <Grid columns={2}>
                            <GridRow>
                              <GridColumn textAlign={'left'}>
                                <Dropdown
                                  selection
                                  options={[{key: 1, value: 'STICKER', text: 'Стикер'},
                                    {key: 2, value: 'WIDE_FORMAT_PRINT', text: 'Широко-форматная'}]}
                                  placeholder='Тип заказа'
                                  value={this.state.type}
                                  onChange={this.changeType}
                                />
                              </GridColumn>
                              <GridColumn textAlign={'right'}>
                                <Button content={'перезагрузить'}
                                        primary
                                        onClick={async () => await this.loadPage()}
                                        labelPosition='left'
                                        icon='redo'/>
                              </GridColumn>
                            </GridRow>
                          </Grid>
                        </TableHeaderCell>
                      </TableRow>
                      <TableRow>
                        <TableHeaderCell>ID</TableHeaderCell>
                        <TableHeaderCell>Статус</TableHeaderCell>
                        <TableHeaderCell>Менеджер</TableHeaderCell>
                        <TableHeaderCell>Клиент</TableHeaderCell>
                        <TableHeaderCell>Заказ</TableHeaderCell>
                        <TableHeaderCell>Время исполнения</TableHeaderCell>
                        <TableHeaderCell>Тип</TableHeaderCell>
                        <TableHeaderCell>Количество</TableHeaderCell>
                        <TableHeaderCell>Создан</TableHeaderCell>
                        <TableHeaderCell>Закончен</TableHeaderCell>
                      </TableRow>
                    </TableHeader>
                    <Table.Body>
                      {!this.state.type.length || !this.state.orders.length ?
                        <TableRow>
                          <TableCell colSpan={10}
                                     content={!this.state.type.length ? 'Выберите тип заказа' : 'Заказов нет'}/>
                        </TableRow>
                        :
                        this.state.orders.map(order => (
                          <OrderRow key={order.id}
                                    utils={this.props.utils}
                                    order={order}
                                    reload={this.loadPage}/>))
                      }
                    </Table.Body>
                    <Table.Footer fullWidth>
                      <Table.Row>
                        <Table.HeaderCell colSpan={10}>
                          <GridRow>
                            <GridColumn textAlign={'right'}>
                              <Button primary
                                      onClick={async () => await this.back()}
                                      labelPosition='left'
                                      content='Назад'
                                      icon='chevron left'/>
                              <Button primary onClick={async () => await this.forward()} labelPosition='right'
                                      content='Вперед' icon='chevron right'/>
                            </GridColumn>
                          </GridRow>
                        </Table.HeaderCell>
                      </Table.Row>
                    </Table.Footer>
                  </Table>
                </GridColumn>
              </GridRow>
            </Grid> :
            <div>
              <Message error header='Error' content='can not load page '/>
            </div>
        }
      </Fragment>
    )
  }
}