import React, {Component, Fragment} from 'react';
import {
  Button,
  Divider,
  Grid,
  GridColumn,
  GridRow,
  Label,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader,
  Segment,
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableHeaderCell,
  TableRow
} from 'semantic-ui-react';
import RenderHelper from "../../../../../utils/RenderHelper";

export default class PreDigitalPrintWorkRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      printWork:  this.props.printWork,
      modalOpened: false,
      success: !(this.props.printWork.blockMaterials.filter(e => !e.item).length>0 ||
        this.props.printWork.perimeterMaterials.filter(e => !e.item).length>0 ||
        this.props.printWork.squareMeterMaterials.filter(e => !e.item).length>0 ||
        this.props.printWork.oneTimeMaterials.filter(e => !e.item).length>0
      )
    };
  }

  render() {
    let {modalOpened, printWork} = this.state;
    return <Modal
      style={{marginTop: 0}}
      trigger={this.renderButton()}
      open={modalOpened}
    >
      <ModalHeader content={'Id :' + printWork.id}/>
      {this.state.success ?
        <ModalContent scrolling>
          <div style={{marginTop: '20px', color: 'black'}}>
            <Table definition>
              <TableBody>
                {/*название*/}
                <TableRow>
                  <TableCell><h3>Название:</h3></TableCell>
                  <TableCell>{printWork.name}</TableCell>
                </TableRow>
                {/*Типы изд*/}
                <TableRow>
                  <TableCell>
                    <h3>Применимые изделия:</h3>
                  </TableCell>
                  <TableCell>
                    {this.state.printWork.orderTypes.map(type =>
                      <Label key={type} content={RenderHelper.renderOrderType(type)}/>
                    )}
                  </TableCell>
                </TableRow>
                {/*Тип работы*/}
                <TableRow>
                  <TableCell><h3>Тип работы:</h3></TableCell>
                  <TableCell>{RenderHelper.renderPrintWorkType(printWork.type)}</TableCell>
                </TableRow>
              </TableBody>
            </Table>

            {/*Цена*/}
            <Grid>
              <GridRow columns={1}>
                <GridColumn>
                  <h2>Параметры просчёта стоимости</h2>
                </GridColumn>
              </GridRow>
              <GridRow columns={2}>
                <GridColumn>
                  <Segment raised>
                    <Grid columns={2}>
                      <GridRow columns={2}>
                        <GridColumn>Размер блока:</GridColumn>
                        <GridColumn>{RenderHelper.renderBlockSize(printWork.blockSize)}</GridColumn>
                      </GridRow>
                      <GridRow>
                        <GridColumn>Стоимость блока:</GridColumn>
                        <GridColumn>{printWork.blockPrice}</GridColumn>
                      </GridRow>
                    </Grid>
                  </Segment>
                </GridColumn>
                <GridColumn>
                  <Segment raised>
                    <Grid columns={2}>
                      <GridRow columns={2}>
                        <GridColumn>Цена за квадратный метр:</GridColumn>
                        <GridColumn>{printWork.squareMeterPrice}</GridColumn>
                      </GridRow>
                    </Grid>
                  </Segment>
                </GridColumn>
              </GridRow>
              <GridRow columns={2}>
                <GridColumn>
                  <Segment raised>
                    <Grid columns={2}>
                      <GridRow columns={2}>
                        <GridColumn>Цена за периметр:</GridColumn>
                        <GridColumn>{printWork.perimeterPrice}</GridColumn>
                      </GridRow>
                    </Grid>
                  </Segment>
                </GridColumn>
                <GridColumn>
                  <Segment raised>
                    <Grid columns={2}>
                      <GridRow columns={2}>
                        <GridColumn>Разовая Цена:</GridColumn>
                        <GridColumn>{printWork.oneTimePrice}</GridColumn>
                      </GridRow>
                    </Grid>
                  </Segment>
                </GridColumn>
              </GridRow>
            </Grid>
            <Divider horizontal/>
            <h2>Материалы</h2>
            <Table celled columns={3}>
              <TableHeader>
                <TableRow>
                  <TableHeaderCell>Id</TableHeaderCell>
                  <TableHeaderCell>Название</TableHeaderCell>
                  <TableHeaderCell>Количество</TableHeaderCell>
                </TableRow>
              </TableHeader>
              <TableBody>
                {printWork.blockMaterials !== [] ? printWork.blockMaterials.map(material => (
                  <TableRow key={material.id}>
                    <TableCell>{material.id}</TableCell>
                    <TableCell>{material.item.name}</TableCell>
                    <TableCell>{material.quantity}</TableCell>
                  </TableRow>
                )) : <Fragment/>
                }
                {printWork.squareMeterMaterials !== [] ? printWork.squareMeterMaterials.map(material => (
                  <TableRow key={material.id}>
                    <TableCell>{material.id}</TableCell>
                    <TableCell>{material.item.name}</TableCell>
                    <TableCell>{material.quantity}</TableCell>
                  </TableRow>
                )) : <Fragment/>
                }
                {printWork.oneTimeMaterials !== [] ? printWork.oneTimeMaterials.map(material => (
                  <TableRow key={material.id}>
                    <TableCell>{material.id}</TableCell>
                    <TableCell>{material.item.name}</TableCell>
                    <TableCell>{material.quantity}</TableCell>
                  </TableRow>
                )) : <Fragment/>
                }
                {printWork.perimeterMaterials !== [] ? printWork.perimeterMaterials.map(material => (
                  <TableRow key={material.id}>
                    <TableCell>{material.id}</TableCell>
                    <TableCell>{material.item.name}</TableCell>
                    <TableCell>{material.quantity}</TableCell>
                  </TableRow>
                )) : <Fragment/>
                }
              </TableBody>
            </Table>
          </div>
          <div style={{paddingTop: '10px', marginTop: '64px'}}/>
        </ModalContent>
        :
        <ModalContent scrolling>
          <h1>Печатная работа повреждена обратитесь к админу</h1>
        </ModalContent>

      }
      <ModalActions>
        <Button positive content='Закрыть' onClick={async () => await this.modalOpened(false)}/>
      </ModalActions>
    </Modal>
  }

  async modalOpened(state) {
    await this.setState({modalOpened: state});
  }

  renderButton() {
    let {printWork} = this.state;
    return (<TableRow key={printWork.id} onClick={async () => await this.modalOpened(true)}>
        <TableCell>{printWork.id}</TableCell>
        <TableCell>{printWork.name}</TableCell>
        <TableCell>{printWork.cost}</TableCell>
      </TableRow>
    )
  }
}