import React, {Component, Fragment} from 'react';
import {
  Button,
  Grid,
  GridColumn,
  GridRow,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader,
  TableCell,
  TableRow
} from 'semantic-ui-react';
import StickerTable from "../orderTabls/StickerTable";
import RenderHelper from "../../../../../utils/RenderHelper";
import PrintModal from "../PrintModal";
import Man from "../../../../../utils/elements/Man";

export default class OrderRow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      order: this.props.order,
      modalOpened: false,
      modalLoading: false,
      PrintModalOpened: false,
    };
  }

  async componentDidMount() {
    await this.setState({order: this.props.order});
  }

  render() {
    let {modalOpened, order} = this.state;
    return <Modal
      style={{marginTop: 0}}
      trigger={this.renderButton()}
      open={modalOpened}
    >
      <ModalHeader>
        <Grid columns={2} container>
          <GridRow columns={2}>
            <GridColumn>
              {order.type === 'STICKER' ? 'Заказ Стикера' : 'Заказ Широко форматной печати'}
            </GridColumn>
            <GridColumn textAlign={'right'}>
              {this.state.order.status !== 'FINISHED' ?
                <PrintModal status={this.state.order.status} id={this.state.order.id} reload={async () => {
                  await this.props.reload();
                  await this.modalOpened(false);
                }}
                            utils={this.props.utils}
                /> :
                <Fragment/>
              }
            </GridColumn>
          </GridRow>
        </Grid>
      </ModalHeader>
      <ModalContent scrolling content={
        // order.type === 'STICKER' ?
        <StickerTable order={order}/>
        // :
        // так как  WideFormatTable не отлмчается от StickerTable для обоиз используется StickerTable
        //            <WideFormatTable order={order}/>
      }
      />
      <ModalActions>

        < Button positive content='Закрыть'
                 onClick={() => this.modalOpened(false)}/>
      </ModalActions>
    </Modal>
  }

  async modalOpened(state) {
    await this.setState({modalOpened: state});
  }

  renderButton() {
    let {order} = this.state;
    return (<TableRow key={order.id} onClick={async () => {
        await this.modalOpened(true)
      }}>
        <TableCell>{order.id}</TableCell>
        <TableCell>{RenderHelper.renderOrderStatus(order.status)}</TableCell>
        <TableCell>{order.invoice.manager}</TableCell>
        <TableCell>{<Man invoice={order.invoice}/>}</TableCell>
        <TableCell>{order.invoice.title}</TableCell>
        <TableCell>{order.estimatedTime}</TableCell>
        <TableCell>{RenderHelper.renderOrderType(order.type)}</TableCell>
        <TableCell>{order.count}</TableCell>
        <TableCell>{RenderHelper.renderDate(order.created)}</TableCell>
        <TableCell>{order.finished === null ? 'ещё не завершен' : order.finished}</TableCell>
      </TableRow>
    )
  };
}