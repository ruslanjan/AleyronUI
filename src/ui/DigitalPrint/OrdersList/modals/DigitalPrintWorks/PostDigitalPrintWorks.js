import React, {Component} from 'react'
import {Table, TableHeader, TableHeaderCell, TableRow,} from 'semantic-ui-react'
import PostDigitalPrintWorkRow from "../Rows/PostDigitalPrintWorkRow";

export default class PreDigitalPrintWorks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postDigitalPrintWorks: this.props.postDigitalPrintWorks,
    };
  }

  render() {
    let {postDigitalPrintWorks} = this.state;
    return (
      <Table celled compact definition selectable>
        <TableHeader fullWidth>
          <TableRow>
            <TableHeaderCell>id</TableHeaderCell>
            <TableHeaderCell>Название</TableHeaderCell>
            <TableHeaderCell>Цена</TableHeaderCell>
          </TableRow>
        </TableHeader>
        <Table.Body>
          {postDigitalPrintWorks.map((postDigitalPrintWork) => (
            <PostDigitalPrintWorkRow key={postDigitalPrintWork.id} printWork={postDigitalPrintWork}/>
          ))}
        </Table.Body>
      </Table>
    )
  }
}