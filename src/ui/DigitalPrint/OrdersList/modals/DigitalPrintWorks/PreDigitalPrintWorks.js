import React, {Component} from 'react'
import {Table, TableHeader, TableHeaderCell, TableRow,} from 'semantic-ui-react'
import PreDigitalPrintWorkRow from "../Rows/PreDigitalPrintWorkRow";

export default class PreDigitalPrintWorks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      preDigitalPrintWorks: this.props.preDigitalPrintWorks,
    };
  }

  render() {
    let {preDigitalPrintWorks} = this.state;
    return (
      <Table celled compact definition selectable>
        <TableHeader fullWidth>
          <TableRow>
            <TableHeaderCell>id</TableHeaderCell>
            <TableHeaderCell>Название</TableHeaderCell>
            <TableHeaderCell>Цена</TableHeaderCell>
          </TableRow>
        </TableHeader>
        <Table.Body>
          {preDigitalPrintWorks.map((preDigitalPrintWork) => (
            <PreDigitalPrintWorkRow key={preDigitalPrintWork.id} printWork={preDigitalPrintWork}/>
          ))}
        </Table.Body>
      </Table>
    )
  }
}