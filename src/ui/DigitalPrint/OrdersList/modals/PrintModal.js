import React, {Component, Fragment} from 'react';
import axios from 'axios/index';
import {
  Button,
  Container, Dropdown,
  Form,
  FormField,
  Header,
  Icon,
  Input,
  Label,
  Modal,
  ModalActions,
  ModalContent
} from 'semantic-ui-react';
import endpoints from "../../../../config/endpoints";
import './large text.css'
import RenderHelper from "../../../../utils/RenderHelper";
import _ from "lodash";
export default class PrintModal extends Component {
  toast = async (toast) => {
    await this.props.utils.toast(toast);
  };

  print = async () => {
    await this.props.utils.loading(true);
    await this.setState({modalLoading: true});
    try {
      await axios({
        method: "post",
        url: endpoints.api.digital_print.order_manager.setPrintOrderStatus,
        data: {orderId: this.state.id, orderStatus: this.state.status},
        auth: this.props.utils.user,
        headers: {
          'Content-Type': 'application/json'
        },
      });
      await this.toast({
        visible: true,
        header: 'Цифровая печать',
        color: 'green',
        body: 'Статус успешно обнавлен!'
      });
      await this.props.reload();
    } catch (e) {
      await this.toast({
        visible: true,
        header: 'Цифровая печать',
        color: 'red',
        body: 'Не удалось обнавить статус!'
      });
      console.error(e);
      await this.props.utils.loading(false);
    } finally {
      await this.setState({modalLoading: false, modalOpened: false});
    }
  };

  handleRef = (c) => {
    if (c) {
      c.focus()
    } else {
    }
  };


  constructor(props) {

    super(props);

    this.state = {
      id: this.props.id,
      modalOpened: false,
      modalLoading: false,
      disabled: true,
      status: this.props.status,
    };
    this.allStatus = _.map(['WAITING', 'PICKING_UP_MATERIALS', 'PROCESSING', 'PRE_PRINT_WORK', 'PRINT', 'POST_PRINT_WORK', 'FINISHED', 'PAUSED', 'FAILURE'], e => ({
      key: e,
      value: e, ...RenderHelper.renderOrderStatusForDropdown(e)
    }));
  }

  render() {
    let {id} = this.state;
    return(<Fragment>
      <Dropdown placeholder={'Статус'} scrolling search selection options={this.allStatus}
                defaultValue={this.state.status}
                onChange={async (e, {value}) => {
                  await this.setStatus(value);
                }}/>
      <Modal
                   open={this.state.modalOpened}
                   onClose={() => this.modalOpened(false)}>
        <Header icon='print' content="Печать"/>
        <ModalContent>
          <Container>
            <p>Вы уверены, что хотите выстовить статус напечатанно для заказа?</p>
            <p>Для подтверждения введите последние 4 символа id заказа.</p>
            <p class="large text"> id:  <b>{id.substr(0, id.length - 4)}</b><u><b>{id.substr(-4)}</b></u></p>
          </Container>
          {this.state.disabled ?
            <Form>
              <FormField>
                <Input ref={this.handleRef}
                       onChange={(e) => {
                         if (e.target.value === id.substr(-4)) {
                           this.setState({disabled: false});
                         }
                       }}/>
              </FormField>
            </Form>
            :
            <Label color={'green'} content={'подтверждено'}/>
          }
        </ModalContent>
        <ModalActions>
          <Button color={'red'} onClick={async () => {
            await this.setState({disabled: true});
            await this.modalOpened(false);
          }}>
            <Icon name='remove'/> Нет
          </Button>
          <Button color='green'
                  disabled={this.state.disabled}
                  loading={this.state.modalLoading}
                  onClick={async () => await this.print()}
                  content={'Да'}
          />
        </ModalActions>
      </Modal>
      </Fragment>
    );
  }

  async modalOpened(state) {
    await this.setState({modalOpened: state});
  }

  async modalLoading(state) {
    await this.setState({modalLoading: state});
  }
  async setStatus(status){
    await this.setState({status: status});
    if(status==='FINISHED'){
      await this.modalOpened(true);
    } else {
      await this.print();
    }
  }

}