import React, {Component} from 'react';
import {
  Accordion,
  AccordionTitle,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableRow
} from "semantic-ui-react";
import PreDigitalPrintWorks from "../DigitalPrintWorks/PreDigitalPrintWorks";
import PostDigitalPrintWorks from "../DigitalPrintWorks/PostDigitalPrintWorks";

export default class WideFormatTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      order: this.props.order,
      commentActive: false,
      prePWActive: false,
      postPWActive: false,
    };
  }

  render() {
    let {order, commentActive, prePWActive, postPWActive} = this.state;
    return (
      <Grid container>
        <GridRow>
          <GridColumn>
            <Table basic='very'>
              <TableBody>
                <TableRow>
                  <TableCell>Id:</TableCell>
                  <TableCell>{order.id}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Статус</TableCell>
                  <TableCell>{order.status}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Ширина:</TableCell>
                  <TableCell>{order.weight}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Высота:</TableCell>
                  <TableCell>{order.height}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Машина</TableCell>
                  <TableCell>{order.machine}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Время исполнения</TableCell>
                  <TableCell>{order.estimatedTime}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Тип</TableCell>
                  <TableCell>{order.type}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Дата создания:</TableCell>
                  <TableCell>{order.created}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Дата окончания:</TableCell>
                  <TableCell>{order.finished === null ? 'еще не закончен' : order.finished}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell><h2>Cost:</h2></TableCell>
                  <TableCell><h2>{order.cost}</h2></TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </GridColumn>
        </GridRow>
        <GridRow>
          <Accordion>
            <AccordionTitle active={prePWActive} index={0}
                            onClick={() => this.setState({prePWActive: !prePWActive})}>
              <Icon name='dropdown'/>
              До печатные работы
            </AccordionTitle>
            <Accordion.Content active={prePWActive}>
              <PreDigitalPrintWorks utils={this.props.utils} preDigitalPrintWorks={order.preDigitalPrintWorks}/>
            </Accordion.Content>
          </Accordion>
        </GridRow>
        <GridRow>
        <Accordion>
          <AccordionTitle active={postPWActive} index={0}
                          onClick={() => this.setState({postPWActive: !postPWActive})}>
            <Icon name='dropdown'/>
            После печатные работы
          </AccordionTitle>
          <Accordion.Content active={postPWActive}>
            <PostDigitalPrintWorks utils={this.props.utils} postDigitalPrintWorks={order.postDigitalPrintWorks}/>
          </Accordion.Content>
        </Accordion>
      </GridRow>
        <GridRow>
          <Accordion>
            <AccordionTitle active={commentActive} index={0}
                            onClick={() => this.setState({commentActive: !commentActive})}>
              <Icon name='dropdown'/>
              Комментарии
            </AccordionTitle>
            <Accordion.Content active={commentActive}>
              <p>{order.comments}</p>
            </Accordion.Content>
          </Accordion>
        </GridRow>
      </Grid>
    )
  }
}