import React, {Component} from 'react';
import {Grid, GridRow, Table, TableBody, TableCell, TableRow} from "semantic-ui-react";

export default class DesignOrderTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      order: this.props.order,
    };
  }


  render() {
    let {order} = this.state;
    return (
      <Grid container>
        <GridRow>
          <Table basic='very'>
            <TableBody>
              <TableRow>
                <TableCell>Id:</TableCell>
                <TableCell>{order.id}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Машина</TableCell>
                <TableCell>{order.machine}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Тип</TableCell>
                <TableCell>{order.type}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Дата создания:</TableCell>
                <TableCell>{order.created}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Дата окончания:</TableCell>
                <TableCell>{order.finished === null ? 'еще не завершен' : order.finished}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell><h2>Дизайнер: </h2></TableCell>
                <TableCell><h2>{order.designer.username}</h2></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><h2>Цена:</h2></TableCell>
                <TableCell><h2>{order.cost}</h2></TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </GridRow>
      </Grid>
    )
  }

}