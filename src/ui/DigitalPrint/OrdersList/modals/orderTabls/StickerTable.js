import React, {Component, Fragment} from 'react';
import {Grid, GridColumn, GridRow, Segment, Table, TableBody, TableCell, TableRow} from "semantic-ui-react";
import PreDigitalPrintWorks from "../DigitalPrintWorks/PreDigitalPrintWorks";
import PostDigitalPrintWorks from "../DigitalPrintWorks/PostDigitalPrintWorks";
import RenderHelper from "../../../../../utils/RenderHelper";
import RectangleSizeDisplayer from "../../../../../utils/elements/RectangleSizeDisplayer";
import Man from "../../../../../utils/elements/Man";

export default class StickerTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      order: this.props.order,
      commentActive: false,
      prePWActive: false,
      postPWActive: false,
      active: ''
    };
  }

  render() {
    let {order} = this.state;
    return (
      <Grid container>
        <GridRow>
          <GridColumn>
            <Table basic='very'>
              <TableBody>
                <TableRow>
                  <TableCell>Id:</TableCell>
                  <TableCell>{order.id}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Название заказа:</TableCell>
                  <TableCell>{order.invoice.title}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Менеджер:</TableCell>
                  <TableCell>{order.invoice.manager}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Имя клиента:</TableCell>
                  <TableCell><Man invoice={order.invoice}/></TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Статус</TableCell>
                  <TableCell>{RenderHelper.renderOrderStatus(order.status)}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Ширина x Высота</TableCell>
                  <TableCell>{this.props.order.width && this.props.order.height ?
                    <RectangleSizeDisplayer width={this.props.order.width} height={this.props.order.height}
                                            unit={'m'}/> :
                    <Fragment>недействительный</Fragment>
                  }</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Тех края (мм) Ширина x Высота</TableCell>
                  <TableCell>{this.props.order.width && this.props.order.height ?
                    <RectangleSizeDisplayer width={this.props.order.techCornersWidth}
                                            height={this.props.order.techCornersHeight} unit={'m'}/> :
                    <Fragment>недействительный</Fragment>
                  }</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Файл</TableCell>
                  <TableCell>{order.design}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Тип</TableCell>
                  <TableCell>{RenderHelper.renderOrderTypeLabel(order.type)}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Дата создания:</TableCell>
                  <TableCell>{RenderHelper.renderDate(order.created)}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Материал:</TableCell>
                  <TableCell>{RenderHelper.renderMaterialLabel(order.printMaterial)}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Дата окончания:</TableCell>
                  <TableCell>{order.finished === null ? 'нет даты окончания' : order.finished}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </GridColumn>
        </GridRow>
        {!!order.preDigitalPrintWorks.length ?
          <GridRow>
            <GridColumn>
              <h3>Допечатные работы</h3>
              <Segment>
                <PreDigitalPrintWorks utils={this.props.utils} preDigitalPrintWorks={order.preDigitalPrintWorks}/>
              </Segment>
            </GridColumn>
          </GridRow>
          : <Fragment><h3>Допечатных работ нет</h3></Fragment>}
        {!!order.postDigitalPrintWorks.length ?
          <GridRow>
            <GridColumn>
              <Segment>
                <h3>Послепечатные работы</h3>
                <PostDigitalPrintWorks utils={this.props.utils}
                                       postDigitalPrintWorks={order.postDigitalPrintWorks}/>
              </Segment>
            </GridColumn>
          </GridRow>
          : <Fragment><h3>Послепечатных работ нет</h3></Fragment>}
        {order.comments !== null ?
          <GridRow>
            <GridColumn>
              <Segment>
                <h3>Комментарий</h3>
                <p>{order.comments}</p>
              </Segment>
            </GridColumn>
          </GridRow>
          : <Fragment><h3>Коментария нет</h3></Fragment>}

      </Grid>

    )
  }
}