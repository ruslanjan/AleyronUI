 import React, { Component, Fragment } from 'react'
import { Menu, MenuItem } from "semantic-ui-react";
import OrderList from "./OrdersList/OrderList";

export default class DigitalPrint extends Component {

  handleItemClick = (e, { name }) => {
    if(name in this.functionItems) {
      this.functionItems[name]();
    } else {
      this.setState({ activeItem: name })
    }
  };

  constructor(props) {

    super(props);
    this.state         = {
      activeItem: 'OrderList'
    };
    this.itemsView     = { //      'Storage'  : <Storage utils={this.props.utils} />,
//      'StorageHistory': <StorageHistory utils={this.props.utils} />,
      'OrderList': <OrderList utils={this.props.utils} />
    };
    this.functionItems = {
      'BackToMain': () => {
        this.props.backToMain();
      }
    }
  }

  render() {
    const { activeItem } = this.state;
    //noinspection JSValidateTypes,JSValidateTypes
    return (
        <Fragment>
          <Menu inverted fixed={'top'} color={'blue'}>
            <MenuItem
                name='BackToMain'
                active={activeItem === 'BackToMain'}
                onClick={this.handleItemClick}
            >Назад</MenuItem>
            <MenuItem
                name='OrderList'
                active={activeItem === 'OrderList'}
                onClick={this.handleItemClick}
            >Список заказов</MenuItem>
            <MenuItem position={'right'} header>Цифровая Печать</MenuItem>
          </Menu>
          {this.itemsView[this.state.activeItem]}
        </Fragment>
    )
  }
}