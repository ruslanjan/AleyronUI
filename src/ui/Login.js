import React, { Component, Fragment } from 'react';
import { Button, Divider, Form, Icon, Input, Segment } from 'semantic-ui-react';
import Particles from 'react-particles-js';
import particlesjsConfig from './particlesjs-config';
import axios from 'axios';
import endpoints from '../config/endpoints'
import env from '../config/env';

export default class Login extends Component {
  constructor(props) {

    super(props);
    this.onSubmit         = this.onSubmit.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.state            = {
      username    : '',
      password    : '',
      openErrorBar: false,
      loading     : false,
    }
  }

  async componentDidMount() {
    if(env.status === 'DEBUG') {
      let { login } = env.debug.auto;
      await this.setState(login);
      await this.login();
    }
  }

  onChangeUsername(e) {
    this.setState({ username: e.target.value })
  }

  onChangePassword(e) {
    this.setState({ password: e.target.value })
  }

  async login() {
    let { username, password } = this.state;
    let user                   = {
      username: username,
      password: password,
    };
    this.props.utils.loading(true);
    let bodyFormData = new FormData();
    bodyFormData.set('username', username);
    bodyFormData.set('password', password);
    axios({
            method: 'post',
            url   : endpoints.api.login,
            data  : bodyFormData,
          }).then((res) => {

              if(res.data) {
                //noinspection JSUnresolvedFunction
                this.props.onLogin(user);
              }
            })
            .catch(() => {

              this.props.utils.loading(false);
            });
  }

  async onSubmit(e) {
    e.preventDefault();
    e.stopPropagation();
    await this.props.utils.loading(true);
    await this.login();
  }

  render() {
    return (
        <Fragment>
          <Particles params={particlesjsConfig} style={{ position: 'absolute', }} />
          <div style={{
            display       : 'flex',
            flexDirection : 'row',
            alignItems    : 'center',
            justifyContent: 'center',
            height        : '100%',
          }}>
            <div>
              <Segment>
                <h1>
                  Авторизация
                </h1>
                <Divider />
                <Form onSubmit={this.onSubmit} loading={this.state.loading}>
                  <div style={{
                    display       : 'flex',
                    justifyContent: 'center',
                    alignItems    : 'center',
                    flexDirection : 'column',
                  }}>
                    <div>
                      <Input placeholder={'username'} iconPosition={'left'} icon={<Icon name={'user'} />}
                             autoComplete={'username'}
                             onChange={this.onChangeUsername} />
                    </div>
                    <div style={{
                      paddingTop   : '10px',
                      paddingBottom: '10px',
                    }}>
                      <Input placeholder={'password'} iconPosition={'left'} icon={<Icon name={'key'} />}
                             type={'password'} autoComplete={'current-password'}
                             onChange={this.onChangePassword} />
                    </div>
                    <div>
                      <Button type='submit'>
                        Войти
                      </Button>
                    </div>
                  </div>
                </Form>
              </Segment>
            </div>
          </div>
        </Fragment>
    )
  }
}
