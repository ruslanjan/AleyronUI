import React, { Component } from 'react';
import { Button, Header, Icon, Modal, ModalActions, ModalContent } from 'semantic-ui-react';
import axios from 'axios';
import endpoints from '../../../../../config/endpoints';

export default class DeleteItem extends Component {
  constructor(props) {

    super(props);

    this.state = {
      id          : this.props.id,
      modalLoading: false
    }
  }

  async componentDidUpdate(prevProps) {
    if(this.props.id !== prevProps.id) {
      await this.setState({ id: this.props });
    }
  }

  render() {
    return (<Modal trigger={this.renderButton()}
                   open={this.state.modalOpened}
                   basic
                   onClose={() => this.modalOpened(false)}>
          <Header icon='trash alternate outline' content='Удаление элемента' />
          <ModalContent>
            Вы уверены что хотите удалить?
          </ModalContent>
          <ModalActions>
            <Button basic inverted onClick={() => this.modalOpened(false)}>
              <Icon name='remove' /> Отмена
            </Button>
            <Button color='red' inverted
                    loading={this.state.modalLoading}
                    onClick={() => this.delItem(this.state.id)}>
              <Icon name='trash alternate outline' /> Удалить
            </Button>
          </ModalActions>
        </Modal>
    );
  }

  async modalOpened(state) {
    await this.setState({ modalOpened: state });
  }

  async delItem(id) {
    await this.modalLoading(true);
    try {
      await axios({
                    method : 'post',
                    url    : endpoints.api.digital_print.storage.items.delete,
                    auth   : this.props.utils.user,
                    data   : id,
                    headers: {
                      'Content-Type': 'application/json'
                    }
                  })
    } finally {
      await this.modalLoading(false);
      await this.modalOpened(false);
      //noinspection JSUnresolvedFunction
      await this.props.onDeleted();
    }
  }

  async modalLoading(state) {
    await this.setState({ modalLoading: state });
  }

  renderButton() {
    return (<Button disabled={this.props.disabled} icon={'trash alternate outline'}
                    color={'red'}
                    onClick={() => this.modalOpened(true)} />);
  }

}