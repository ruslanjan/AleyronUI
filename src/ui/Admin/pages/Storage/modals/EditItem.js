import React, { Component } from 'react';
import {
  Button,
  Dropdown,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Input,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader
} from 'semantic-ui-react';
import UnitConverter from '../../../../../utils/UnitConverter';
import axios from 'axios';
import endpoints from '../../../../../config/endpoints';
import _ from 'lodash';
import RenderHelper from '../../../../../utils/RenderHelper';

export default class EditItem extends Component {

  constructor(props) {

    super(props);

    this.state = {
      modalOpen    : false,
      form         : {
        name         : '',
        unit         : 'pcs',
        quantity     : null,
        cost         : null,
        allowedOrders: []
      },
      allOrderTypes: [],
      modalLoading : false
    }
  }

  async componentDidMount() {
    await this.loadContext();
  }

  async loadOrderTypes() {
    let { data } = await axios({
                                 method: 'get',
                                 url   : endpoints.api.order.print_work.getAllOrderType,
                                 auth  : this.props.utils.user
                               });

    let allOrderTypes = _.map(data, e => ({ key: e, value: e, ...RenderHelper.renderOrderTypeForDropdown(e) }));

    await this.setState({ allOrderTypes });
  };

  render() {
    return (<Modal trigger={this.renderButton()}
                   open={this.state.modalOpened}
                   onClose={async () => await this.modalOpened(false)}>
          <ModalHeader>
            Edit item
          </ModalHeader>
          <ModalContent>
            <Form>
              <Grid>
                <GridRow columns={2}>
                  <GridColumn>
                    <FormField>
                      <label>Название</label>
                      <Input type={'text'}
                             defaultValue={this.state.form.name}
                             onChange={async (e) => await this.changeForm('name', e.target.value)} />
                    </FormField>
                  </GridColumn>
                  <GridColumn>
                    <FormField>
                      <label>Разрешенные типы заказа</label>
                      <Dropdown search multiple selection
                                options={this.state.allOrderTypes}
                                defaultValue={this.state.form.allowedOrders}
                                onChange={async (e, { value }) => await this.changeForm('allowedOrders', value)} />
                    </FormField>
                  </GridColumn>

                </GridRow>
                <GridRow columns={2}>
                  <GridColumn>
                    <FormField>
                      <label>Unit</label>
                      {this.renderUnitsDropdown()}
                    </FormField>
                  </GridColumn>
                  <GridColumn>
                    <FormField>
                      <label>Цена</label>
                      <Input type={'text'}
                             label={'KZT'}

                             labelPosition={'left'}
                             defaultValue={this.state.form.cost}
                             onChange={async (e) => await this.changeForm('cost', e.target.value)} />
                    </FormField>
                  </GridColumn>
                </GridRow>
              </Grid>
            </Form>
          </ModalContent>
          <ModalActions>
            <Button color={'red'} onClick={async () => await this.modalOpened(false)}>
              Отмена
            </Button>
            <Button icon
                    labelPosition={'right'}
                    loading={this.state.modalLoading}
                    color={'green'}
                    onClick={async () => await this.perform()}>
              <Icon name={'arrow circle down'} />
              Добавить
            </Button>
          </ModalActions>
        </Modal>
    );
  }

  renderUnitsDropdown() {
    return <Dropdown selection search
                     options={UnitConverter.dropdownUnits}
                     defaultValue={this.state.form.unit}
                     onChange={async (e, { value }) => await this.changeForm('unit', value)} />;
  }

  async modalOpened(state) {
    await this.setState({ modalOpened: state });
  }

  renderButton() {
    return (<Button disabled={this.props.disabled}
                    icon={'edit'}
                    color={'teal'}
                    onClick={async () => await this.modalOpened(true)} />);
  }

  async perform() {
    await this.setState({ modalLoading: true });
    await this.normalizeQuantity();
    let { form } = this.state;
    form.unit    = UnitConverter.writeUnit(form.unit);
    try {
      await axios({
                    method: 'post',
                    url   : endpoints.api.digital_print.storage.items.create,
                    auth  : this.props.utils.user,
                    data  : form
                  });
    }
    finally {
      await this.setState({ modalLoading: false });
      //noinspection JSUnresolvedFunction
      await this.props.onChange();
    }
  }

  async normalizeQuantity() {
    let [q, u] = UnitConverter.normalizeValue(this.state.form.quantity)
                              .withUnit(this.state.form.unit);
    await this.changeForm('quantity', q);
    await this.changeForm('unit', u);
  }

  async changeForm(name, value) {
    let { form } = this.state;
    form[name]   = value;
    await this.setState({ form });
  }

  async loadContext() {
//    await this.
    await this.loadItem();
    await this.loadOrderTypes();
  }

  async loadItem() {
    try {
      let { data } = await axios({
                                   url   : endpoints.api.digital_print.storage.items.getById,
                                   method: 'get',
                                   auth  : this.props.utils.user,
                                   params: {
                                     id: this.props.id
                                   }
                                 });

      data.unit =  UnitConverter.readUnit(data.unit);

      await this.setState({ form: data });
    } catch(e) {
      console.error(e);
      this.props.utils.toast.error('Price-list editor', 'Internal error occured');
    }
  }
}