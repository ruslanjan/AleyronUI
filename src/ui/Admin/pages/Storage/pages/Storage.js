import _ from 'lodash';
import React, { Component, Fragment } from 'react';
import {
  Button,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Input,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHeader,
  TableHeaderCell,
  TableRow
} from 'semantic-ui-react';
import axios from 'axios';
import endpoints from '../../../../../config/endpoints';
import AddItem from '../modals/AddItem';
import DeleteItem from '../modals/DeleteItem';
import UnitConverter from '../../../../../utils/UnitConverter';
import Permission from '../../../../../utils/Permission';
import RenderHelper from '../../../../../utils/RenderHelper';
import EditItem from '../modals/EditItem';

class Filter {
  constructor(name, value) {
    this._name  = name;
    this._value = value;
    if(!value) {
      this._name = null;
    }
  }

  get value() {
    return this._value;
  }

  set value(value) {

    if(!value) {
      this._name = null;
    }
    this._value = value
  }

  get name() {
    return this._name
  }

  set name(value) {
    this._name = value
  }
}

export default class Storage extends Component {
  constructor(props) {

    super(props);
    this.state = {
      items             : [],
      page              : 0,
      pageSize          : 10,
      filter            : new Filter(),
      timeout           : -1,
      filterInputEnabled: {
        group: true,
        name : true
      }
    };

    this.loadDataPage = this.loadDataPage.bind(this);
  }

  async componentDidMount() {
//    alert('DidMount');

    await this.props.utils.loading(true);
    await this.loadContext();
    await this.props.utils.loading(false);
  }

  async loadContext() {
    await this.loadDataPage();
  }

  async loadRegularPage() {
    await this.props.utils.loading(true);
    try {
      let { data } = await axios({
                                   method: 'get',
                                   url   : endpoints.api.digital_print.storage.items.getPage,
                                   params: {
                                     page: this.state.page,
                                     size: this.state.pageSize
                                   },
                                   auth  : this.props.utils.user
                                 });
      await this.setState({
                            items: data
                          });
    } finally {
      this.props.utils.loading(false);
    }
  }

  async loadFTSPage() {
    await this.props.utils.loading(true);
    try {
      let { data } = await axios({
                                   method: 'get',
                                   url   : endpoints.api.digital_print.storage.items.getPage,
                                   params: {
                                     page  : this.state.page,
                                     size  : this.state.pageSize,
                                     filters: [`$text=$search|${this.state.filter.value}`]
                                   },
                                   auth  : this.props.utils.user
                                 });
      await this.setState({
                            items: data
                          });
    } finally {
      this.props.utils.loading(false);
    }
  }

  async loadDataPage() {

    if(!this.state.filter.name) {
      await this.loadRegularPage();
    } else {
      await this.loadFTSPage();
    }
  }

  // region pager
  async forwardPage() {
    let { page } = this.state;
    await this.setState({ page: page + 1 });
    await this.loadDataPage();
    if(this.state.items.length <= 0) {
      await this.rewindPage();
    }
  }

  async rewindPage() {
    let { page } = this.state;
    await this.setState({ page: Math.max(page - 1, 0) });
    await this.loadDataPage();
  }

  // endregion

  async filterUpdated(reload = true) {
    let { filter, filterInputEnabled } = this.state;
    if(!filter.name) {
      for(let key of Object.keys(filterInputEnabled)) {
        filterInputEnabled[key] = true;
      }
    } else {
      for(let key of Object.keys(filterInputEnabled)) {
        filterInputEnabled[key] = false;
      }
      filterInputEnabled[filter.name] = true;
    }
    await this.setState({ filterInputEnabled });
    if(reload) await this.loadDataPage();
  }

  async onFilterInputChange(name, e) {
    clearTimeout(this.state.timeout);
    await this.setState({ filter: new Filter(name, e.target.value) });
    let timeout = setTimeout(async () => await this.loadDataPage(), 600);
    await this.setState({ timeout });
  }

  render() {
    return (
        <Fragment>
          <Grid container padded='vertically'>
            <GridRow>
              <GridColumn>
                <Table celled structured>
                  <TableHeader>
                    <TableRow>
                      <TableHeaderCell colSpan={8}>
                        <Grid>
                          <GridRow columns={2}>
                            <GridColumn>
                              <AddItem utils={this.props.utils}
                                       disabled={!Permission.user(this.props.utils.user)
                                                            .granted('digital_print.storage.items.add')}
                                       onChange={async () => await this.loadDataPage()} />
                              {/*<ButtonGroup>*/}
                              {/*<Income utils={this.props.utils}*/}
                              {/*disabled={!Permission.user(this.props.utils.user)*/}
                              {/*.granted('digital_print.storage.operations.perform')}*/}
                              {/*onChange={async () => await this.loadDataPage()} />*/}
                              {/*<Outcome utils={this.props.utils}*/}
                              {/*disabled={!Permission.user(this.props.utils.user)*/}
                              {/*.granted('digital_print.storage.operations.perform')}*/}
                              {/*onChange={async () => await this.loadDataPage()} />*/}
                              {/*</ButtonGroup>*/}
                            </GridColumn>
                            <GridColumn textAlign={'right'}>
                            </GridColumn>
                          </GridRow>
                        </Grid>
                      </TableHeaderCell>
                    </TableRow>
                    <TableRow>
                      <TableHeaderCell width={2} collapsing rowSpan={2}>ID</TableHeaderCell>
                      <TableHeaderCell width={5}>Имя</TableHeaderCell>
                      <TableHeaderCell collapsing rowSpan={2}>Количество</TableHeaderCell>
                      <TableHeaderCell collapsing rowSpan={2}>Цена</TableHeaderCell>
                      <TableHeaderCell collapsing rowSpan={2}>For order types</TableHeaderCell>
                      <TableHeaderCell collapsing rowSpan={2}>Действия</TableHeaderCell>
                    </TableRow>
                    <TableRow>
                      <TableHeaderCell>
                        <Input type={'search'}
                               size={'small'}
                               fluid
                               icon={{
                                 name    : 'search',
                                 circular: true,
                                 link    : true,
                                 onClick : async () => await this.filterUpdated()
                               }}
                               onKeyDown={(e) => ((e.keyCode === 13) ?
                                   this.filterUpdated() : null)}
                               onChange={async e => {
                                 let name = 'name';
                                 await this.onFilterInputChange(name, e);
                               }}
                               floated={'right'} />
                      </TableHeaderCell>
                    </TableRow>
                  </TableHeader>
                  <TableBody>
                    {this.state.items.map(item => (
                        <TableRow key={item.id}>
                          <TableCell>{item.id}</TableCell>
                          <TableCell>{item.name}</TableCell>
                          <TableCell>{UnitConverter.renderUnit(UnitConverter.readUnit(item.unit))}</TableCell>
                          <TableCell>{RenderHelper.renderCost(item.cost)}</TableCell>
                          <TableCell>{_.map(item.allowedOrders,
                                            orderType => RenderHelper.renderOrderTypeLabel(orderType))}</TableCell>
                          <TableCell>
                            <EditItem id={item.id}
                                        disabled={!Permission.user(this.props.utils.user)
                                                             .granted('digital_print.storage.items.delete')}
                                        onChange={async () => await this.loadDataPage()}
                                        utils={this.props.utils}
                            />
                            <DeleteItem id={item.id}
                                        disabled={!Permission.user(this.props.utils.user)
                                                             .granted('digital_print.storage.items.delete')}
                                        onDeleted={async () => await this.loadDataPage()}
                                        utils={this.props.utils}
                            />
                          </TableCell>
                        </TableRow>))
                    }
                  </TableBody>
                  <TableFooter fullWidth>
                    <TableRow>
                      <TableHeaderCell colSpan={8}><Grid>
                        <GridRow>
                          <GridColumn textAlign={'right'}>
                            <Button primary onClick={async () => {
                              await this.rewindPage();
                            }}>
                              <Icon name={'chevron left'} />
                              Назад
                            </Button>
                            <Button primary onClick={async () => {
                              await this.forwardPage();
                            }}>
                              Вперед
                              <Icon name={'chevron right'} />
                            </Button>
                          </GridColumn>
                        </GridRow>
                      </Grid>
                      </TableHeaderCell>
                    </TableRow>
                  </TableFooter>
                </Table>
              </GridColumn>
            </GridRow>
          </Grid>
        </Fragment>
    )
  }
}