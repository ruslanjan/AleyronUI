import React, { Component } from 'react'
import {
  Button,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHeader,
  TableHeaderCell,
  TableRow
} from 'semantic-ui-react';
import endpoints from '../../../../config/endpoints';
import axios from 'axios'
import DeleteRole from './modals/DeleteRole';
import NewRole from './modals/NewRole';
import EditRole from './modals/EditRole';
import RenderHelper from '../../../../utils/RenderHelper';

export default class RolesList extends Component {

  constructor(props) {

    super(props);
    this.state = {
      roles: [],
      page : 0,
    };

    this.loadDataPage = this.loadDataPage.bind(this);
  }

  async componentDidMount() {
    await this.loadDataPage();
  };


  async loadDataPage(size = 12) {
    let { page } = this.state;
    await this.props.utils.loading(true);
    try {
      let { data } = await axios(
          {
            method: 'get',
            url   : endpoints.api.users.roles.getPage,
            params: { page: page, size: size },
            auth  : this.props.utils.user
          });
      await this.setState({ roles: data });
      await this.props.utils.loading(false);
    } finally {
      await this.props.utils.loading(false);
    }
  }

  render() {
    return (
        <Grid container>
          <GridRow>
            <GridColumn>
              <Table celled>
                <TableHeader>
                  <TableRow>
                    <TableHeaderCell colSpan={5}>
                      <Grid>
                        <GridRow>
                          <GridColumn textAlign={'left'}>
                            <NewRole
                                utils={this.props.utils}
                                onChange={async () => await this.loadDataPage()}
                            />
                          </GridColumn>
                        </GridRow>
                      </Grid>
                    </TableHeaderCell>
                  </TableRow>
                  <TableRow>
                    <TableHeaderCell>ID</TableHeaderCell>
                    <TableHeaderCell>Кодовое Имя</TableHeaderCell>
                    <TableHeaderCell>Описание</TableHeaderCell>
                    <TableHeaderCell>Привилегии</TableHeaderCell>
                    <TableHeaderCell>Действия</TableHeaderCell>
                  </TableRow>
                </TableHeader>
                <TableBody>
                  {this.state.roles.map((role) => (
                      <TableRow key={role.id}>
                        <TableCell>{role.id}</TableCell>
                        <TableCell>{role.name}</TableCell>
                        <TableCell>{role.description}</TableCell>
                        <TableCell>{RenderHelper.renderPrivileges(role.privileges)}</TableCell>
                        <TableCell>
                          <EditRole utils={this.props.utils}
                                    id={role.id}
                                    onChange={async () => await this.loadDataPage()} />
                          <DeleteRole utils={this.props.utils}
                                      id={role.id}
                                      onDeleted={async () => await this.loadDataPage()} />
                        </TableCell>
                      </TableRow>
                  ))}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TableHeaderCell colSpan={5}>
                      <Grid>
                        <GridRow>
                          <GridColumn textAlign={'right'}>
                            <Button primary onClick={async () => {
                              await this.rewindPage();
                            }}>
                              <Icon name={'chevron left'} />
                              Назад
                            </Button>
                            <Button primary onClick={async () => {
                              await this.forwardPage();
                            }}>
                              Вперед
                              <Icon name={'chevron right'} />
                            </Button>
                          </GridColumn>
                        </GridRow>
                      </Grid>
                    </TableHeaderCell>
                  </TableRow>
                </TableFooter>
              </Table>
            </GridColumn>
          </GridRow>
        </Grid>
    )
  }

  async forwardPage() {
    let { page } = this.state;
    await this.setState({ page: page + 1 });
    await this.loadDataPage();
    if(this.state.roles.length <= 0) await this.rewindPage();
  }

  async rewindPage() {
    let { page } = this.state;
    await this.setState({ page: Math.max(page - 1, 0) });
    await this.loadDataPage();
  }
}