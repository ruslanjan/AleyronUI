import React, {Component, Fragment} from 'react';
import {
  Button,
  Dropdown,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader
} from 'semantic-ui-react';
import endpoints from '../../../../../config/endpoints';
import _ from 'lodash';
import axios from 'axios';


export default class NewRole extends Component {
  toast = async (state) => {
    this.props.utils.toast(state);
  };

  constructor(props) {
    super(props);

    this.state = {
      form         : {
        privileges : [],
        name       : '',
        description: ''
      },
      toast        : {
        visible: false,
        header : '',
        body   : ''
      },
      allRoles     : [],
      allPrivileges: [],
      modalOpened  : false,
      modalLoading : false
    };

    this.roles = this.privileges = {};

    this.createRole = this.createRole.bind(this);
    (this.loadPrivileges = this.loadPrivileges.bind(this));
  }

  async loadContext() {
    await this.modalLoading(true);
    await this.loadPrivileges();
  }

  async componentDidMount() {
    await this.loadContext();
    await this.modalLoading(false);
  }

  async loadPrivileges() {
    let { data } = await axios({
                                 method: 'get',
                                 url   : endpoints.api.users.privileges.getAll,
                                 auth  : this.props.utils.user
                               });

    let privileges = _.map(data, e => {
      this.privileges[e.id] = e;
      return { key: e.id, value: e.id, text: e.name };
    });

    await this.setState({ 'allPrivileges': privileges });
  }

  async createRole() {
    let { name, description, privileges } = this.state.form;

    privileges = _.map(privileges, e => this.roles[e]);

    let role = { name, description, privileges };

    await this.modalLoading(true);
    try {
      await axios({
                    method: "post",
                    url   : endpoints.api.users.roles.create,
                    data  : role,
                    auth  : this.props.utils.user
                  });
      await this.toast({
                         visible: true,
                         header : 'Панель Администратора',
                         color  : 'teal',
                         body   : 'Создание роли успешно!'
                       });
    } catch(e) {
      await this.toast({
                         visible: true,
                         header : 'Панель Администратора',
                         color  : 'red',
                         body   : 'Не удалось создать роль!'
                       });
    } finally {
      //noinspection JSUnresolvedFunction
      await this.props.onChange();
      await this.modalLoading(false);
      await this.modalOpened(false);
    }
  }

  async modalOpened(state) {
    await this.setState({ modalOpened: state });
  }

  renderButton() {
    return (<Button disabled={this.props.disabled} icon labelPosition={'right'} primary onClick={async () => await this.modalOpened(true)}>
      <Icon name={'shield alternate'} />
      Создать Роль
    </Button>);
  }

  render() {
    return (<Fragment>
          <Modal
              trigger={this.renderButton()}
              style={{ marginTop: 0 }}
              open={this.state.modalOpened}
              onClose={async () => await this.modalOpened(false)}><ModalHeader>
            Новая роль
          </ModalHeader>
            <ModalContent>
              <Form>
                <Grid>
                  <GridRow columns={2}>
                    <GridColumn>
                      <FormField>
                        <label>Название</label>
                        <input placeholder='напр. ROLE_ADMIN'
                               defaultValue={this.state.form.name}
                               onChange={(e) => this.updateForm('name', e.target.value)} />
                      </FormField>
                    </GridColumn>
                    <GridColumn>
                      <FormField>
                        <label>Описание</label>
                        <input placeholder='напр. Admin'
                               defaultValue={this.state.form.description}
                               onChange={(e) => this.updateForm('description', e.target.value)} />
                      </FormField>
                    </GridColumn>
                  </GridRow>
                  <GridRow>
                    <GridColumn>
                      <FormField>
                        <label>Привилегии</label>
                        <Dropdown placeholder={'Привилегии'}
                                  multiple search selection
                                  options={this.state.allPrivileges}
                                  defaultValue={this.state.form.privileges}
                                  onChange={(e, { value }) => this.updateForm('privileges', value)} />
                      </FormField>
                    </GridColumn>
                  </GridRow>
                </Grid>
              </Form>
            </ModalContent>
            <ModalActions>
              <Button
                  onClick={async () => await this.modalOpened(false)}>
                Отмена
              </Button>
              <Button color={'green'}
                      disabled={this.state.modalLoading}
                      loading={this.state.modalLoading}
                      icon
                      labelPosition={'right'}
                      onClick={async () => await this.createRole()}>
                <Icon name={'arrow circle down'} />
                Сохранить
              </Button>
            </ModalActions>

          </Modal>
        </Fragment>
    )
  }

  async updateForm(name, value) {
    let { form } = this.state;
    form[name]   = value;
    await this.setState({ form });
  }

  async modalLoading(state) {
    await this.setState({ modalLoading: state });
  }
}