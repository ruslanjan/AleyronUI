import React, { Component } from 'react';
import endpoints from '../../../../../config/endpoints';
import axios from 'axios/index';
import { Button, Header, Icon, Modal, ModalActions, ModalContent } from 'semantic-ui-react';

export default class DeleteRole extends Component {
  constructor(props) {

    super(props);

    this.state = {
      id          : this.props.id,
      modalOpened : false,
      modalLoading: false
    }
  }

  async delRole(id) {
    try {
      await axios({
                    method : 'post',
                    auth   : this.props.utils.user,
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    data   : id,
                    url    : endpoints.api.users.roles.deleteById
                  });
    } finally {
      //noinspection JSUnresolvedFunction
      await this.props.onDeleted();
      await this.modalLoading(false);
      await this.modalOpened(false);
    }
  }

  render() {
    return (<Modal trigger={this.renderButton()}
                   open={this.state.modalOpened}
                   basic
                   onClose={() => this.modalOpened(false)}>
          <Header icon='trash alternate outline' content="Удаление роли" />
          <ModalContent>
            Нажмите удалить, чтобы удалить роль
          </ModalContent>
          <ModalActions>
            <Button basic inverted onClick={() => this.modalOpened(false)}>
              <Icon name='remove' /> Отмена
            </Button>
            <Button color='red' inverted
                    loading={this.state.modalLoading}
                    onClick={() => this.delRole(this.state.id)}>
              <Icon name='trash alternate outline' /> Удалить
            </Button>
          </ModalActions>
        </Modal>
    );
  }

  async modalOpened(state) {
    await this.setState({ modalOpened: state });
  }

  async modalLoading(state) {
    await this.setState({ modalLoading: state });
  }

  renderButton() {
    return (<Button disabled={this.props.disabled} icon={'trash alternate outline'}
                    color={'red'}
                    onClick={() => this.modalOpened(true)} />);
  }

}