import React, { Component, Fragment } from 'react';
import {
  Button,
  Dropdown,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader
} from 'semantic-ui-react';
import endpoints from '../../../../../config/endpoints';
import _ from 'lodash';
import axios from 'axios';

export default class EditRole extends Component {
  toast = async (state) => {
    this.props.utils.toast(state);
  };

  constructor(props) {

    super(props);

    this.state = {
      form         : {
        id         : this.props.id,
        privileges : [],
        name       : '',
        description: ''
      },
      toast        : {
        visible: false,
        header : '',
        body   : ''
      },
      allPrivileges: []
    };

    this.privileges = {};

    this.alterRole = this.alterRole.bind(this);
    (this.loadPrivileges = this.loadPrivileges.bind(this));
    (this.loadRoleData = this.loadRoleData.bind(this));
  }

  async loadContext() {
    await this.modalLoading(true);
    await this.loadPrivileges();
    await this.loadRoleData();
  }

  async componentDidMount() {
    await this.loadContext();
    await this.modalLoading(false);
  }

  async loadRoleData() {
    let { id }   = this.state.form;
    let { data } = await axios({
                                 method: 'get',
                                 url   : endpoints.api.users.roles.getById,
                                 params: { id },
                                 auth  : this.props.utils.user
                               });

    data.privileges = _.map(data.privileges, e => e.id);

    await this.setState({ form: data });
  };

  async loadPrivileges() {
    let { data } = await axios({
                                 method: 'get',
                                 url   : endpoints.api.users.privileges.getAll,
                                 auth  : this.props.utils.user
                               });

    let privileges = _.map(data, e => {
      this.privileges[e.id] = e;
      return { key: e.id, value: e.id, text: e.name };
    });

    await this.setState({ 'allPrivileges': privileges });
  };

  async alterRole() {
    await this.modalLoading(true);

    let { id, name, description, privileges } = this.state.form;

    privileges = _.map(privileges, e => this.privileges[e]);

    let role = { id, name, description, privileges };
    try {
      await axios({
                    method: "post",
                    url   : endpoints.api.users.roles.create,
                    data  : role,
                    auth  : this.props.utils.user
                  });
      await this.toast({
                         visible: true,
                         header : 'Панель Администратора',
                         body   : 'Изменение роли успешно!'
                       });

    }
    catch(e) {
      console.error(e);
      await this.toast({
                         visible: true,
                         header : 'Панель Администратора',
                         body   : 'Не удалось изменить роль!'
                       });
    } finally {
      await this.modalLoading(false);
      //noinspection JSUnresolvedFunction
      await this.props.onChange();
      await this.modalOpened(false);
    }
  }

  async modalOpened(state) {
    await this.setState({ modalOpened: state });
  }

  renderButton() {
    return (
        <Button disabled={this.props.disabled} onClick={() => this.modalOpened(true)} color={'teal'} icon={'edit'} />);
  }

  render() {

    return <Fragment><Modal
        trigger={this.renderButton()}
        style={{ marginTop: 0 }}
        open={this.state.modalOpened}
        onClose={() => this.modalOpened(false)}>
      <ModalHeader>
        Редактирование
      </ModalHeader>
      <ModalContent>
        <Form>
          <Grid>
            <GridRow>
              <GridColumn>
                <FormField>
                  <label>Id</label>
                  <input defaultValue={this.state.form.id} readOnly />
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow columns={2}>
              <GridColumn>
                <FormField>
                  <label>Название</label>
                  <input placeholder='напр. ROLE_ADMIN'
                         defaultValue={this.state.form.name}
                         onChange={(e) => this.updateForm('name', e.target.value)} />
                </FormField>
              </GridColumn>
              <GridColumn>
                <FormField>
                  <label>Описание</label>
                  <input placeholder='напр. Admin'
                         defaultValue={this.state.form.description}
                         onChange={(e) => this.updateForm('description', e.target.value)} />
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow>
              <GridColumn>
                <FormField>
                  <label>Привилегии</label>
                  <Dropdown placeholder={'Привилегии'}
                            multiple search selection
                            options={this.state.allPrivileges}
                            defaultValue={this.state.form.privileges}
                            onChange={(e, { value }) => this.updateForm('privileges', value)} />
                </FormField>
              </GridColumn>
            </GridRow>
          </Grid>
        </Form>
      </ModalContent>
      <ModalActions>
        <Button onClick={async () => await this.modalOpened(false)} content='Отмена' />
        <Button color={'green'}
                disabled={this.state.modalLoading}
                loading={this.state.modalLoading}
                icon
                labelPosition={'right'}
                onClick={() => this.alterRole()}>
          <Icon name={'arrow circle down'} />
          Сохранить
        </Button>
      </ModalActions>

    </Modal>

    </Fragment>
  }

  async updateForm(name, value) {
    let { form } = this.state;
    form[name]   = value;
    await this.setState({ form });
  }

  async modalLoading(state) {
    await this.setState({ modalLoading: state });
  }
};

