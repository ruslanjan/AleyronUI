import React, {Component, Fragment} from 'react';
import {
  Button,
  Dropdown,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader
} from 'semantic-ui-react';
import endpoints from '../../../../../config/endpoints';
import _ from 'lodash';
import axios from 'axios';

export default class EditUser extends Component {
  toast = async (state) => {
    this.props.utils.toast(state);
  };

  constructor(props) {

    super(props);

    this.state = {
      form: {
        id: this.props.id,
        firstName: '',
        lastName: '',
        username: '',
        password: '',
        email: '',
        phone: '',
      },
      toast: {
        visible: false,
        header: '',
        body: ''
      },
      allRoles: [],
      allPrivileges: [],
      modalOpened: false,
      modalLoading: false,
    };

    this.roles = this.privileges = {};

    this.alterUser = this.alterUser.bind(this);
    (this.loadRoles = this.loadRoles.bind(this));
    (this.loadPrivileges = this.loadPrivileges.bind(this));
    (this.loadUserData = this.loadUserData.bind(this));
  }

  async componentDidUpdate(prevProps) {
    if (this.props.id !== prevProps.id) {
      await this.updateForm('id', this.props.id);
      await this.loadContext();
    }
  }

  async loadContext() {
    await this.modalLoading(true);
    await this.loadRoles();
    await this.loadPrivileges();
    await this.loadUserData();
  }

  async componentDidMount() {
    await this.loadContext();
    await this.modalLoading(false);
  }

  async loadUserData() {
    let {id} = this.state.form;
    let {data} = await axios({
      method: 'get',
      url: endpoints.api.users.getById,
      params: {id},
      auth: this.props.utils.user
    });

    data.roles = _.map(data.roles, e => e.id);
    data.privileges = _.map(data.privileges, e => e.id);

    await this.setState({form: data});
  };

  async loadRoles() {
    let {data} = await axios({
      method: 'get',
      url: endpoints.api.users.roles.getAll,
      auth: this.props.utils.user
    });

    let roles = _.map(data, e => {
      this.roles[e.id] = e;
      return {key: e.id, value: e.id, text: e.description};
    });

    await this.setState({'allRoles': roles});

  };

  async loadPrivileges() {
    let {data} = await axios({
      method: 'get',
      url: endpoints.api.users.privileges.getAll,
      auth: this.props.utils.user
    });

    let privileges = _.map(data, e => {
      this.privileges[e.id] = e;
      return {key: e.id, value: e.id, text: e.name};
    });

    await this.setState({'allPrivileges': privileges});
  };

  async alterUser() {
    await this.modalLoading(true);

    let {id, firstName, lastName, username, password, roles, privileges, email, phone} = this.state.form;

    roles = _.map(roles || [], item => this.roles[item]);
    privileges = _.map(privileges || [], item => this.privileges[item]);

    let user = {
      id, firstName, lastName, username, roles, privileges, email, phone
    };

    if ((password != null) && password.length > 0) {
      user.password = password;
    }

    try {
      await axios({
        method: "post",
        url: endpoints.api.users.update,
        data: user,
        auth: this.props.utils.user
      });
      await this.toast({
        visible: true,
        header: 'Панель Администратора',
        body: 'Изменение пользователя успешно!'
      });

    }
    catch (e) {
      await this.toast({
        visible: true,
        header: 'Панель Администратора',
        body: 'Не удалось изменить пользователя!'
      });
    } finally {
      await this.modalLoading(false);
      //noinspection JSUnresolvedFunction
      await this.props.onChange();
      await this.modalOpened(false);
    }
  }

  async modalOpened(state) {
    await this.setState({modalOpened: state});
  }

  renderButton() {
    return (
      <Button disabled={this.props.disabled} onClick={() => this.modalOpened(true)} color={'teal'} icon={'edit'}/>);
  }

  render() {

    return <Fragment><Modal
      trigger={this.renderButton()}
      style={{marginTop: 0}}
      open={this.state.modalOpened}
      onClose={() => this.modalOpened(false)}>
      <ModalHeader>
        Редактирование
      </ModalHeader>
      <ModalContent>
        <Form>
          <Grid container>
            <GridRow>
              <GridColumn>
                <FormField>
                  <label>Id</label>
                  <input placeholder='Id'
                         readOnly={true}
                         defaultValue={this.state.form.id}
                         onChange={(e) => this.updateForm('id', e.target.value)}/>
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow columns={2}>
              <GridColumn>
                <FormField>
                  <label>Имя</label>
                  <input placeholder='Имя'
                         defaultValue={this.state.form.firstName}
                         onChange={(e) => this.updateForm('firstName', e.target.value)}/>
                </FormField>
              </GridColumn>
              <GridColumn>
                <FormField>
                  <label>Фамилия</label>
                  <input placeholder='Фамилия'
                         defaultValue={this.state.form.lastName}
                         onChange={(e) => this.updateForm('lastName', e.target.value)}/>
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow columns={2}>
              <GridColumn>
                <FormField required>
                  <label>Имя Пользователя</label>
                  <input placeholder='Имя Пользователя'
                         defaultValue={this.state.form.username}
                         onChange={(e) => this.updateForm('username', e.target.value)}/>
                </FormField>
              </GridColumn>
              <GridColumn>
                <FormField required>
                  <label>Пароль</label>
                  <input placeholder='Ваш Пароль'
                         defaultValue={this.state.form.password}
                         onChange={(e) => this.updateForm('password', e.target.value)}/>
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow columns={2}>
              <GridColumn>
                <FormField>
                  <label>Роли</label>
                  <Dropdown placeholder={'Роли'}
                            multiple
                            search
                            selection
                            options={this.state.allRoles}
                            defaultValue={this.state.form.roles}
                            onChange={(e, {value}) => this.updateForm('roles', value)}/>
                </FormField>
              </GridColumn>
              <GridColumn>
                <FormField>
                  <label>Привилегии</label>
                  <Dropdown placeholder={'Привилегии'}
                            multiple
                            search
                            selection
                            options={this.state.allPrivileges}
                            defaultValue={this.state.form.privileges}
                            onChange={(e, {value}) => this.updateForm('privileges', value)}/>
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow columns={2}>
              <GridColumn>
                <FormField>
                  <label>Почта</label>
                  <input placeholder='email'
                         defaultValue={this.state.form.email}
                         onChange={(e) => this.updateForm('email', e.target.value)}/>
                </FormField>
              </GridColumn>
              <GridColumn>
                <FormField>
                  <label>Телефон</label>
                  <input placeholder='напр. +7 1234567890'
                         defaultValue={this.state.form.clientPhoneNumber}
                         onChange={(e) => this.updateForm('phone', e.target.value)}/>
                </FormField>
              </GridColumn>
            </GridRow>

          </Grid>
        </Form>
      </ModalContent>
      <ModalActions>
        <Button onClick={async () => await this.modalOpened(false)}>
          Cancel
        </Button>
        <Button color={'green'}
                disabled={this.state.modalLoading}
                loading={this.state.modalLoading}
                icon
                labelPosition={'right'}
                onClick={() => this.alterUser()}>
          <Icon name={'arrow circle down'}/>
          Save
        </Button>
      </ModalActions>

    </Modal>

    </Fragment>
  }

  async updateForm(name, value) {
    let {form} = this.state;
    form[name] = value;
    await this.setState({form});
  }

  async modalLoading(state) {
    await this.setState({modalLoading: state});
  }
};

