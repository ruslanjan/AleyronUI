import React, {Component, Fragment} from 'react';
import {
  Button,
  Dropdown,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader
} from 'semantic-ui-react';
import endpoints from '../../../../../config/endpoints';
import _ from 'lodash';
import axios from 'axios';

export default class NewUser extends Component {
  toast = async (state) => {
    this.props.utils.toast(state);
    // let { toast } = this.state;
    //
    // if(state.hasOwnProperty('header')) {
    //   toast.header = state.header;
    // }
    // if(state.hasOwnProperty('body')) {
    //   toast.body = state.body;
    // }
    // if(state.hasOwnProperty('visible')) {
    //   toast.visible = state.visible;
    // }
    //
    // this.setState({ toast: toast });
  };

  constructor(props) {

    super(props);

    this.state = {
      form: {
        firstName: '',
        lastName: '',
        username: '',
        password: '',
        email: '',
        phone: '',
        roles: [],
        privileges: []
      },
      toast: {
        visible: false,
        header: '',
        body: ''
      },
      allRoles: [],
      allPrivileges: [],
      modalOpened: false,
      modalLoading: false
    };

    this.roles = this.privileges = {};

    this.createUser = this.createUser.bind(this);
    (this.loadRoles = this.loadRoles.bind(this));
    (this.loadPrivileges = this.loadPrivileges.bind(this));
  }

  async loadContext() {
    await this.modalLoading(true);
    await this.loadRoles();
    await this.loadPrivileges();
  }

  async componentDidMount() {
    await this.loadContext();
    await this.modalLoading(false);
  }

  async loadRoles() {
    let {data} = await axios({
      method: 'get',
      url: endpoints.api.users.roles.getAll,
      auth: this.props.utils.user
    });

    let roles = _.map(data, e => {
      this.roles[e.id] = e;
      return {key: e.id, value: e.id, text: e.description};
    });

    await this.setState({'allRoles': roles, 'asd': this.roles});
  }

  async loadPrivileges() {
    let {data} = await axios({
      method: 'get',
      url: endpoints.api.users.privileges.getAll,
      auth: this.props.utils.user
    });

    let privileges = _.map(data, e => {
      this.privileges[e.id] = e;
      return {key: e.id, value: e.id, text: e.name};
    });

    await this.setState({'allPrivileges': privileges});
  }

  async createUser() {
    let {firstName, lastName, username, password, roles, privileges, email, phone} = this.state.form;

    roles = _.map(roles, e => this.roles[e]);
    privileges = _.map(privileges, e => this.roles[e]);

    let user = {
      firstName, lastName, username, password, roles: (roles || []), privileges: (privileges || []), email, phone
    };

    await this.modalLoading(true);
    try {
      await axios({
        method: "post",
        url: endpoints.api.users.register,
        data: user,
        auth: this.props.utils.user
      });
      await this.toast({
        visible: true,
        header: 'Панель администратора',
        body: 'Создание пользователя успешно!'
      });
    } catch (e) {
      await this.toast({
        visible: true,
        header: 'Панель Администратора',
        body: 'Не удалось создать пользователя!'
      });
    } finally {
      //noinspection JSUnresolvedFunction
      await this.props.onChange();
      await this.modalLoading(false);
      await this.modalOpened(false);
    }
  }

  async modalOpened(state) {
    await this.setState({modalOpened: state});
  }

  renderButton() {
    return (<Button disabled={this.props.disabled}
                    icon
                    labelPosition={'right'}
                    primary
                    onClick={async () => await this.modalOpened(true)}>
      <Icon name={'user plus'}/>
      Новый пользователь
    </Button>);
  }

  render() {
    return (<Fragment>
        <Modal
          trigger={this.renderButton()}
          style={{marginTop: 0}}
          open={this.state.modalOpened}
          onClose={async () => await this.modalOpened(false)}>
          <ModalHeader>
            Новый пользователь
          </ModalHeader>
          <ModalContent>
            <Form>
              <Grid container>
                <GridRow columns={2}>
                  <GridColumn>
                    <FormField>
                      <label>Имя</label>
                      <input placeholder='Имя'
                             defaultValue={this.state.form.firstName}
                             onChange={(e) => this.updateForm('firstName', e.target.value)}/>
                    </FormField>
                  </GridColumn>
                  <GridColumn>
                    <FormField>
                      <label>Фамилия</label>
                      <input placeholder='Фамилия'
                             defaultValue={this.state.form.lastName}
                             onChange={(e) => this.updateForm('lastName', e.target.value)}/>
                    </FormField>
                  </GridColumn>
                </GridRow>
                <GridRow columns={2}>
                  <GridColumn>
                    <FormField required>
                      <label>Имя Пользователя</label>
                      <input placeholder='Имя Пользователя'
                             defaultValue={this.state.form.username}
                             onChange={(e) => this.updateForm('username', e.target.value)}/>
                    </FormField>
                  </GridColumn>
                  <GridColumn>
                    <FormField required>
                      <label>Пароль</label>
                      <input placeholder='Ваш Пароль'
                             defaultValue={this.state.form.password}
                             onChange={(e) => this.updateForm('password', e.target.value)}/>
                    </FormField>
                  </GridColumn>
                </GridRow>
                <GridRow columns={2}>
                  <GridColumn>
                    <FormField>
                      <label>Роли</label>
                      <Dropdown placeholder={'Роли'}
                                multiple
                                search
                                selection
                                options={this.state.allRoles}
                                defaultValue={this.state.form.roles}
                                onChange={(e, {value}) => this.updateForm('roles', value)}/>
                    </FormField>
                  </GridColumn>
                  <GridColumn>
                    <FormField>
                      <label>Привилегии</label>
                      <Dropdown placeholder={'Привилегии'}
                                multiple
                                search
                                selection
                                options={this.state.allPrivileges}
                                defaultValue={this.state.form.privileges}
                                onChange={(e, {value}) => this.updateForm('privileges', value)}/>
                    </FormField>
                  </GridColumn>
                </GridRow>
                <GridRow columns={2}>
                  <GridColumn>
                    <FormField>
                      <label>Почта</label>
                      <input placeholder='email'
                             defaultValue={this.state.form.email}
                             onChange={(e) => this.updateForm('email', e.target.value)}/>
                    </FormField>
                  </GridColumn>
                  <GridColumn>
                    <FormField>
                      <label>Телефон</label>
                      <input placeholder='напр. +7 1234567890'
                             defaultValue={this.state.form.clientPhoneNumber}
                             onChange={(e) => this.updateForm('phone', e.target.value)}/>
                    </FormField>
                  </GridColumn>
                </GridRow>

              </Grid>
            </Form>
          </ModalContent>
          <ModalActions>
            < Button
              onClick={async () => await this.modalOpened(false)}>
              Отмена
            </Button>
            <Button color={'green'}
                    disabled={this.state.modalLoading}
                    loading={this.state.modalLoading}
                    icon
                    labelPosition={'right'}
                    onClick={async () => await this.createUser()}>
              <Icon name={'arrow circle down'}/>
              Сохранить
            </Button>
          </ModalActions>

        </Modal>
      </Fragment>
    )
  }

  async updateForm(name, value) {
    let {form} = this.state;
    form[name] = value;
    await this.setState({form});
  }

  async modalLoading(state) {
    await this.setState({modalLoading: state});
  }
}