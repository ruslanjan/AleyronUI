import React, {Component, Fragment} from 'react';
import {Accordion, Button, Grid, GridColumn, GridRow, Icon, Input, Segment} from 'semantic-ui-react';
import axios from "axios/index";
import endpoints from "../../../../config/endpoints";

export default class Config extends Component {

  handleClick1 = (e, titleProps) => {
    const {index} = titleProps;

    const {activeIndex1} = this.state;
    const newIndex = activeIndex1 === index ? -1 : index;
    this.setState({activeIndex1: newIndex})
  };
  handleClick2 = (e, titleProps) => {
    const {index} = titleProps;

    const {activeIndex2} = this.state;
    const newIndex = activeIndex2 === index ? -1 : index;

    this.setState({activeIndex2: newIndex})
  };
  toast = async (state) => {
    this.props.utils.toast(state);
  };
  save = async () => {
    let {PriceConfig} = this.state;
    await this.props.utils.loading(true);
    delete PriceConfig.name;
    try {
      await axios({
        method: "post",
        url: endpoints.api.configs.create,
        data: PriceConfig,
        auth: this.props.utils.user
      });
      await this.toast({
        visible: true,
        header: 'Панель администратора',
        color: 'green',
        body: 'Создание пользователя успешно!'
      });
    } catch (e) {
      await this.toast({
        visible: true,
        color: 'red',
        header: 'Панель Администратора',
        body: 'Не удалось создать пользователя!'
      });
    } finally {
      // await this.loadDataPage();
      await this.props.utils.loading(false);
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      activeIndex1: 0,
      activeIndex2: 0,
      PriceConfig: {
        id: 'noId',
        name: 'noName',
        additionPricePerSquareMeter: {}
      },
      page: 0

    };

    this.loadDataPage = this.loadDataPage.bind(this);

  }

  handleChange(type, event) {
    let {PriceConfig} = this.state;
    if (type === "WIDE_FORMAT_PRINT") {
      PriceConfig.additionPricePerSquareMeter.WIDE_FORMAT_PRINT = event;
    }
    else if (type === "STICKER") {
      PriceConfig.additionPricePerSquareMeter.STICKER = event;
    }
    else if (type === "DIGITAL_PRINT") {
      PriceConfig.additionPricePerSquareMeter.DIGITAL_PRINT = event;
    }
    this.setState({PriceConfig: PriceConfig});

  }

  async componentDidMount() {
    await this.loadDataPage();
    console.log("конфиги", this.state.PriceConfig)
  }

  async loadDataPage() {
    this.props.utils.loading(true);
    try {
      let {data} = await axios(
        {
          method: 'get',
          url: endpoints.api.configs.get,
          auth: this.props.utils.user
        });
      await this.setState({PriceConfig: data});
    }
    finally {
      this.props.utils.loading(false);
    }
  }

  render() {
    let {PriceConfig, activeIndex1, activeIndex2} = this.state;

    return (
      <Fragment>
        <Grid container columns={1}>
          <GridRow>
            <GridColumn>
              <Segment>
                <Accordion>
                  <Accordion.Title active={activeIndex1 === 0} index={0} onClick={this.handleClick1}>
                    <Icon name='dropdown'/>
                    Настройки стоимости заказов
                  </Accordion.Title>
                  <Accordion.Content active={activeIndex1 === 0}>

                    <Accordion id="Accordion2">
                      <Accordion.Title active={activeIndex2 === 0} index={0}
                                       onClick={this.handleClick2}>
                        <Icon name='dropdown'/>
                        Стоимость за квадратный метр
                      </Accordion.Title>

                      <Accordion.Content id="secondAccordionContent" active={activeIndex2 === 0}>

                        <Grid columns={2} celled>
                          <GridRow>
                            <GridColumn>Широкоформатная печать</GridColumn>
                            <GridColumn>
                              <Input fluid type="number"
                                     value={PriceConfig.additionPricePerSquareMeter.WIDE_FORMAT_PRINT}
                                     onChange={(e) => {
                                       this.handleChange("WIDE_FORMAT_PRINT", e.target.value)
                                     }}/>
                            </GridColumn>
                          </GridRow>


                          <GridRow>
                            <GridColumn>Стикеры</GridColumn>
                            <GridColumn>
                              <Input fluid type="number"
                                     value={PriceConfig.additionPricePerSquareMeter.STICKER}
                                     onChange={(e) => {
                                       this.handleChange("STICKER", e.target.value)
                                     }}
                              />
                            </GridColumn>
                          </GridRow>


                          <GridRow>
                            <GridColumn>Цифровая печать</GridColumn>
                            <GridColumn>
                              <Input fluid type="number"
                                     value={PriceConfig.additionPricePerSquareMeter.DIGITAL_PRINT}
                                     onChange={(e) => {
                                       this.handleChange("DIGITAL_PRINT", e.target.value)
                                     }}
                              />
                            </GridColumn>
                          </GridRow>

                          <GridRow>
                            <GridColumn>
                              <Button id="saveButton" onClick={async () => await this.save()}> Сохранить </Button>
                            </GridColumn>
                          </GridRow>


                        </Grid>

                      </Accordion.Content>

                    </Accordion>

                  </Accordion.Content>

                </Accordion>
              </Segment>
            </GridColumn>
          </GridRow>
        </Grid>

      </Fragment>
    )
  }

  /*async updateForm(n,m, a, value) {

      let {configs} = this.state;
      a.dub = value;
      configs[n].ad[m] = a;
      await this.setState({ configs:configs });
  }*/
}