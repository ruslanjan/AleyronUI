import React, {Component} from 'react';
import {Dropdown, FormField, GridRow, Table, TableBody, TableCell, TableHeader, TableRow} from 'semantic-ui-react';

//noinspection JSUnusedGlobalSymbols
export default class MaterialTable extends Component {

  setMaterials = async (materials) => {
    await this.setState(materials);
    await this.props.setMaterials(this.state.materials);
  };

  constructor(props) {
    super(props);
    this.state = {
      allDropDownMaterials: this.props.allDropDownMaterials,
      materials: this.props.materials,
      text: this.props.text,
    };
  }

  render() {

    return <GridRow>
      <Table>
        <TableHeader>
          <TableRow>
            <TableCell>
              <FormField>
                <label>blockMaterials</label>
                <Dropdown placeholder={this.state.text}
                          multiple
                          search
                          selection
                          options={this.state.allDropDownMaterials}
                          defaultValue={this.state.materials}
                          onChange={async (e, {value}) =>{



                          // await this.setMaterials({materials: this.allMaterials[value]})
                          }}/>
              </FormField>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell content={'id'}/>
            <TableCell content={'name'}/>
            <TableCell content={'quantity'}/>
          </TableRow>
        </TableHeader>
        <TableBody>
          {this.state.materials.map(material => (
            <TableRow key={material.id}>
              <TableCell>
                material.id
              </TableCell>
              <TableCell>
                material.name
              </TableCell>
              <TableCell>
                <FormField>
                  <input placeholder='quantity'
                         defaultValue={material.quantity}
                         onChange={async (e) => {
                           //noinspection JSUndefinedPropertyAssignment
                           material.quantity = e.target.value;
                           await this.setMaterials({materials: this.state.materials});
                         }}/>
                </FormField>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </GridRow>
  }
}