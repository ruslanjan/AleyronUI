import React, {Component, Fragment} from 'react';
import {
  Button,
  Dropdown,
  Form,
  FormCheckbox,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader,
  Segment,
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableHeaderCell,
  TableRow
} from 'semantic-ui-react';
import endpoints from '../../../../../config/endpoints';
import axios from 'axios';
import _ from 'lodash';
import RenderHelper from "../../../../../utils/RenderHelper";

const defaultState = {
  form: {
    name: '',
    blockPrice: 0,
    squareMeterPrice: 0,
    perimeterPrice: 0,
    oneTimePrice: 0,
    estimatedTime: null,
    type: '',
    orderTypes: [],
    blockMaterials: [],
    squareMeterMaterials: [],
    oneTimeMaterials: [],
    perimeterMaterials: [],
  },
  modalOpened: false,
  modalLoading: false,
  isBlock: false,
  isSquareMeter: false,
  isOneTime: false,
  isPerimeter: false,
};

export default class NewPrintWork extends Component {
  toast = async (state) => {
    this.props.utils.toast(state);
  };
  updateForm = async (name, value) => {
    let {form} = this.state;
    form[name] = value;
    await this.setState({form});
  };
  refreshForm = async () => {
    await this.modalLoading(true);
    await this.setState(defaultState);
    await this.modalLoading(false);
  };

  state = defaultState;

  constructor(props) {
    super(props);


    this.allOrderTypes = [];
    this.allWorkTypes = [];
    this.allMaterials = {};
    this.allBlockMaterials = [];
    this.allSquareMeterMaterials = [];
    this.allOneTimeMaterials = [];
    this.allPerimeterMaterials = [];
    this.createPrintWork = this.createPrintWork.bind(this);
    (this.loadOrderTypes = this.loadOrderTypes.bind(this));
    (this.loadPrintWorkType = this.loadPrintWorkType.bind(this));
    (this.loadAllMaterials = this.loadAllMaterials.bind(this));
  }

  async loadContext() {
    await this.modalLoading(true);
    await this.loadOrderTypes();
    await this.loadAllMaterials();
    await this.loadPrintWorkType();
  }

  async componentDidMount() {
    await this.loadContext();
    await this.modalLoading(false);
  };

  async loadPrintWorkType() {
    let {data} = await axios({
      method: 'get',
      url: endpoints.api.order.print_work.getAllPrintWorkType,
      auth: this.props.utils.user
    });

    this.allWorkTypes = await _.map(data, e => ({key: e, value: e, ...RenderHelper.renderPrintWorkTypeForDropdown(e)}));
  };

  async loadAllMaterials() {
    let {data} = await axios({
      method: 'get',
      url: endpoints.api.digital_print.storage.items.getAll,
      auth: this.props.utils.user
    });

    await _.map(data, e => this.allBlockMaterials[e.id] = {id: e.id, name: e.name, quantity: e.quantity});
    await _.map(data, e => this.allSquareMeterMaterials[e.id] = {id: e.id, name: e.name, quantity: e.quantity});
    await _.map(data, e => this.allOneTimeMaterials[e.id] = {id: e.id, name: e.name, quantity: e.quantity});
    await _.map(data, e => this.allPerimeterMaterials[e.id] = {id: e.id, name: e.name, quantity: e.quantity});
    this.allMaterials = await _.map(data, e => ({key: e.id, value: e.id, text: e.name}));
  };

  async loadOrderTypes() {
    let {data} = await axios({
      method: 'get',
      url: endpoints.api.order.print_work.getAllOrderType,
      auth: this.props.utils.user
    });

    this.allOrderTypes = await _.map(data, e => ({key: e, value: e, ...RenderHelper.renderOrderTypeForDropdown(e)}));
  };

  async createPrintWork() {
    let {name, blockPrice, squareMeterPrice, oneTimePrice, perimeterPrice, estimatedTime, type, orderTypes, blockMaterials, squareMeterMaterials, oneTimeMaterials, perimeterMaterials} = this.state.form;

    let printWork = {
      name,
      blockPrice,
      squareMeterPrice,
      oneTimePrice,
      perimeterPrice,
      estimatedTime,
      type: (type || []),
      orderTypes: (orderTypes || []),
      blockMaterials: (blockMaterials || []),
      squareMeterMaterials: (squareMeterMaterials || []),
      oneTimeMaterials: (oneTimeMaterials || []),
      perimeterMaterials: (perimeterMaterials || []),
    };

    await this.modalLoading(true);
    try {
      await axios({
        method: "post",
        url: endpoints.api.order.print_work.create,
        data: printWork,
        auth: this.props.utils.user,
        headers: {
          'Content-Type': 'application/json'
        },
      });
      await this.toast({
        visible: true,
        header: 'Панель Администратора',
        color: 'green',
        body: 'Создание печатной работы успешно!'
      });
      await this.modalOpened(false);
      await this.refreshForm();
    } catch (e) {
      console.error(e);
      await this.toast({
        visible: true,
        header: 'Панель Администратора',
        color: 'red',
        body: 'Не удалось создать печатную работу!'
      });
    } finally {
      this.props.onClose();
      await this.modalLoading(false);
    }
  }

  numOfPrice() {
    let tmp = 0;
    if (this.state.isPerimeter) {
      tmp++;
    }
    if (this.state.isOneTime) {
      tmp++;
    }
    if (this.state.isSquareMeter) {
      tmp++;
    }
    if (this.state.isBlock) {
      tmp++;
    }
    return tmp;
  };

  render() {
    return (
      <Modal
        trigger={this.renderButton()}
        style={{marginTop: 0}}
        open={this.state.modalOpened}
        onClose={async () => await this.modalOpened(false)}>
        <ModalHeader content={'Новая печатная работа'}/>
        <ModalContent scrolling>
          <Form>
            <Grid container>
              <GridRow columns={2}>
                <GridColumn>
                  <FormField required>
                    <label>Название</label>
                    <input placeholder='Имя'
                           defaultValue={this.state.form.name}
                           onChange={(e) => this.updateForm('name', e.target.value)}/>
                  </FormField>
                </GridColumn>
                <GridColumn>
                  <GridRow>
                    <FormCheckbox label='Блок' defaultChecked={this.state.isBlock} onChange={async (e, {checked}) => {
                      if (!checked) {
                        await this.updateForm('blockPrice', 0);
                        await this.updateForm('blockMaterials', []);
                      }
                      await this.setState({isBlock: checked});
                    }}/>
                  </GridRow>
                  <GridRow>
                    <FormCheckbox label='Квадратный метр' defaultChecked={this.state.isSquareMeter}
                                  onChange={async (e, {checked}) => {
                                    if (!checked) {
                                      await this.updateForm('squareMeterPrice', 0);
                                      await this.updateForm('squareMeterMaterials', []);
                                    }
                                    await this.setState({isSquareMeter: checked});
                                  }}/>
                  </GridRow>
                  <GridRow>
                    <FormCheckbox label='Одноразовый' defaultChecked={this.state.isOneTime}
                                  onChange={async (e, {checked}) => {
                                    if (!checked) {
                                      await this.updateForm('oneTimePrice', 0);
                                      await this.updateForm('oneTimeMaterials', []);
                                    }
                                    await this.setState({isOneTime: checked});
                                  }}/>
                  </GridRow>
                  <GridRow>
                    <FormCheckbox label='Периметр' defaultChecked={this.state.isPerimeter}
                                  onChange={async (e, {checked}) => {
                                    if (!checked) {
                                      await this.updateForm('perimeterPrice', 0);
                                      await this.updateForm('perimeterMaterials', []);
                                    }
                                    await this.setState({isPerimeter: checked});
                                  }}/>
                  </GridRow>
                </GridColumn>
              </GridRow>
              <GridRow columns={2}>
                <GridColumn>
                  <FormField>
                    <label>Применимыее типы заказа</label>
                    <Dropdown placeholder={'Применимыее типы заказа'} multiple search selection
                              defaultValue={this.state.form.orderTypes} options={this.allOrderTypes}
                              onChange={(e, {value}) => this.updateForm('orderTypes', value)}/>
                  </FormField>
                </GridColumn>
                <GridColumn>
                  <FormField required>
                    <label>Тип работы</label>
                    <Dropdown placeholder={'Тип работы'} selection label={{icon: 'asterisk'}}
                              defaultValue={this.state.form.type} options={this.allWorkTypes}
                              onChange={(e, {value}) => this.updateForm('type', value)}/>
                  </FormField>
                </GridColumn>
              </GridRow>
              <GridRow>
                {this.numOfPrice() !== 0 ?

                  <Segment raised>
                    <Grid columns={this.numOfPrice()} divided>
                      <GridRow>
                        {this.state.isBlock ? <GridColumn>
                          <FormField>
                            <label>Цена за блок</label>
                            <input placeholder='Цена за блок' defaultValue={this.state.form.blockPrice}
                                   onChange={(e) => this.updateForm('blockPrice', e.target.value)}/>
                          </FormField>
                        </GridColumn> : <Fragment/>}
                        {this.state.isOneTime ? <GridColumn>
                          <FormField>
                            <label>Одноразовая цена</label>
                            <input placeholder='Одноразовая цена' defaultValue={this.state.form.oneTimePrice}
                                   onChange={(e) => this.updateForm('oneTimePrice', e.target.value)}/>
                          </FormField>
                        </GridColumn> : <Fragment/>}
                        {this.state.isSquareMeter ? <GridColumn>
                          <FormField>
                            <label>Цена за квадратный метр</label>
                            <input placeholder='Цена за квадратный метр' defaultValue={this.state.form.squareMeterPrice}
                                   onChange={(e) => this.updateForm('squareMeterPrice', e.target.value)}/>
                          </FormField>
                        </GridColumn> : <Fragment/>}
                        {this.state.isPerimeter ? <GridColumn>
                          <FormField>
                            <label>Цена за Периметр</label>
                            <input placeholder='Цена за Периметр' defaultValue={this.state.form.perimeterPrice}
                                   onChange={(e) => this.updateForm('perimeterPrice', e.target.value)}/>
                          </FormField>
                        </GridColumn> : <Fragment/>}
                      </GridRow>
                    </Grid>
                  </Segment>
                  : <Fragment/>
                }

              </GridRow>
              {this.state.isBlock ? <GridRow>
                <Table celled>
                  <TableHeader>
                    <TableRow>
                      <TableHeaderCell colSpan={3}>
                        <FormField>
                          <label>Материалыы для блока</label>
                          <Dropdown placeholder={'Материалыы для блока'} multiple search selection
                                    options={this.allMaterials}
                                    defaultValue={this.state.form.blockMaterials.map(e => e.id)}
                                    onChange={async (e, {value}) => await this.updateForm('blockMaterials', value.map(value => this.allBlockMaterials[value]))}/>
                        </FormField>
                      </TableHeaderCell>
                    </TableRow>
                    <TableRow>
                      <TableHeaderCell content={'id'}/>
                      <TableHeaderCell content={'Название'}/>
                      <TableHeaderCell content={'Количество'}/>
                    </TableRow>
                  </TableHeader>
                  <TableBody>
                    {this.state.form.blockMaterials.map(material => (
                      <TableRow key={material.id}>
                        <TableCell>
                          {material.id}
                        </TableCell>
                        <TableCell>
                          {material.name}
                        </TableCell>
                        <TableCell>
                          <FormField>
                            <input placeholder='quantity'
                                   defaultValue={material.quantity}
                                   onChange={async (e) => {
                                     material.quantity = e.target.value;
                                     await this.updateForm('blockMaterials', this.state.form.blockMaterials);
                                   }}/>
                          </FormField>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </GridRow> : <Fragment/>}
              {this.state.isSquareMeter ? <GridRow>
                <Table celled>
                  <TableHeader>
                    <TableRow>
                      <TableHeaderCell colSpan={3}>
                        <FormField>
                          <label>Материалы для площади</label>
                          <Dropdown placeholder={'Материалы для площади'} multiple search selection
                                    options={this.allMaterials}
                                    defaultValue={this.state.form.squareMeterMaterials.map(e => e.id)}
                                    onChange={async (e, {value}) => await this.updateForm('squareMeterMaterials', await value.map(value => this.allSquareMeterMaterials[value]))}/>
                        </FormField>
                      </TableHeaderCell>
                    </TableRow>
                    <TableRow>
                      <TableHeaderCell content={'id'}/>
                      <TableHeaderCell content={'Название'}/>
                      <TableHeaderCell content={'Количество'}/>
                    </TableRow>
                  </TableHeader>
                  <TableBody>
                    {this.state.form.squareMeterMaterials.map(material => (
                      <TableRow key={material.id}>
                        <TableCell>
                          {material.id}
                        </TableCell>
                        <TableCell>
                          {material.name}
                        </TableCell>
                        <TableCell>
                          <FormField>
                            <input placeholder='quantity'
                                   defaultValue={material.quantity}
                                   onChange={async (e) => {
                                     material.quantity = e.target.value;
                                     await this.updateForm('squareMeterMaterials', this.state.form.squareMeterMaterials);
                                   }}/>
                          </FormField>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </GridRow> : <Fragment/>}
              {this.state.isOneTime ? <GridRow>
                <Table celled>
                  <TableHeader>
                    <TableRow>
                      <TableHeaderCell colSpan={3}>
                        <FormField>
                          <label>Материалы для одноразовой работы</label>
                          <Dropdown placeholder={'Материалы для одноразовой работы'} multiple search selection
                                    options={this.allMaterials}
                                    defaultValue={this.state.form.oneTimeMaterials.map(e => e.id)}
                                    onChange={async (e, {value}) => await this.updateForm('oneTimeMaterials', await value.map(value => this.allOneTimeMaterials[value]))}/>
                        </FormField>
                      </TableHeaderCell>
                    </TableRow>
                    <TableRow>
                      <TableHeaderCell content={'id'}/>
                      <TableHeaderCell content={'Название'}/>
                      <TableHeaderCell content={'Количество'}/>
                    </TableRow>
                  </TableHeader>
                  <TableBody>
                    {this.state.form.oneTimeMaterials.map(material => (
                      <TableRow key={material.id}>
                        <TableCell>
                          {material.id}
                        </TableCell>
                        <TableCell>
                          {material.name}
                        </TableCell>
                        <TableCell>
                          <FormField>
                            <input placeholder='quantity'
                                   defaultValue={material.quantity}
                                   onChange={async (e) => {
                                     material.quantity = e.target.value;
                                     await this.updateForm('oneTimeMaterials', this.state.form.oneTimeMaterials);
                                   }}/>
                          </FormField>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </GridRow> : <Fragment/>}
              {this.state.isPerimeter ? <GridRow>
                <Table celled>
                  <TableHeader>
                    <TableRow>
                      <TableHeaderCell colSpan={3}>
                        <FormField>
                          <label>Материалы для периметра</label>
                          <Dropdown placeholder={'Материалы для периметра'} multiple search selection
                                    options={this.allMaterials}
                                    defaultValue={this.state.form.perimeterMaterials.map(e => e.id)}
                                    onChange={async (e, {value}) => await this.updateForm('perimeterMaterials', await value.map(value => this.allPerimeterMaterials[value]))}/>
                        </FormField>
                      </TableHeaderCell>
                    </TableRow>
                    <TableRow>
                      <TableHeaderCell content={'id'}/>
                      <TableHeaderCell content={'Название'}/>
                      <TableHeaderCell content={'Количество'}/>
                    </TableRow>
                  </TableHeader>
                  <TableBody>
                    {this.state.form.perimeterMaterials.map(material => (
                      <TableRow key={material.id}>
                        <TableCell>
                          {material.id}
                        </TableCell>
                        <TableCell>
                          {material.name}
                        </TableCell>
                        <TableCell>
                          <FormField>
                            <input placeholder='quantity'
                                   defaultValue={material.quantity}
                                   onChange={async (e) => {
                                     material.quantity = e.target.value;
                                     await this.updateForm('perimeterMaterials', this.state.form.perimeterMaterials);
                                   }}/>
                          </FormField>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </GridRow> : <Fragment/>}
            </Grid>
          </Form>
        </ModalContent>
        <ModalActions>
          <Button onClick={async () => {
            await this.modalOpened(false);
            await this.refreshForm()
          }} content={'Отмена'}/>
          <Button color={'green'} disabled={this.state.modalLoading} loading={this.state.modalLoading}
                  icon={'arrow circle down'} labelPosition={'right'}
                  onClick={async () => await this.createPrintWork()}
                  content={'Сохранить'}/>
        </ModalActions>
      </Modal>
    )
  };

  async modalLoading(state) {
    await this.setState({modalLoading: state});
  };

  async modalOpened(state) {
    await this.setState({modalOpened: state});
  };

  renderButton() {
    return (<Button disabled={this.props.disabled} labelPosition={'right'} primary
                    onClick={async () => await this.modalOpened(true)} icon={'clipboard'}
                    content={'Создать печатную работу'}/>
    );
  };
}