import React, {Component, Fragment} from 'react';
import {
  Button,
  Divider,
  Grid,
  GridColumn,
  GridRow,
  Label,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader,
  Segment,
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableHeaderCell,
  TableRow
} from 'semantic-ui-react';
import DeletePrintWork from "./DeletePrintWork";
import EditPrintWork from "./EditPrintWork";
import RenderHelper from "../../../../../utils/RenderHelper";
import MaterialRow from "../../../../../utils/elements/MaterialRow";

export default class PrintWorkRow extends Component {
  reload = async () => {
    await this.props.reload();
  };

  constructor(props) {
    super(props);
    this.state = {
      printWork: this.props.printWork,
      modalOpened: false,
      success: !(this.props.printWork.blockMaterials.filter(e => !e.item).length>0 ||
        this.props.printWork.perimeterMaterials.filter(e => !e.item).length>0 ||
        this.props.printWork.squareMeterMaterials.filter(e => !e.item).length>0 ||
        this.props.printWork.oneTimeMaterials.filter(e => !e.item).length>0
      )
    };
  }

  async componentDidMount(){
    await this.setState({printWork: this.props.printWork});
  }

  render() {
    let {modalOpened, printWork} = this.state;
    return <Modal
      style={{marginTop: 0}}
      trigger={this.renderButton()}
      open={modalOpened}
    >
      <ModalHeader content={'Id :' + printWork.id}/>
        <ModalContent scrolling>
          <div style={{marginTop: '20px', color: 'black'}}>
            <Table definition>
              <TableBody>
                {/*название*/}
                <TableRow>
                  <TableCell><h3>Название:</h3></TableCell>
                  <TableCell>{printWork.name}</TableCell>
                </TableRow>
                {/*Типы изд*/}
                <TableRow>
                  <TableCell>
                    <h3>Применимые изделия:</h3>
                  </TableCell>
                  <TableCell>
                    {this.state.printWork.orderTypes.map(type =>
                      <Label key={type} content={RenderHelper.renderOrderType(type)}/>
                    )}
                  </TableCell>
                </TableRow>
                {/*Тип работы*/}
                <TableRow>
                  <TableCell><h3>Тип работы:</h3></TableCell>
                  <TableCell>{RenderHelper.renderPrintWorkType(printWork.type)}</TableCell>
                </TableRow>
              </TableBody>
            </Table>

            {/*Цена*/}
            <Grid>
              <GridRow columns={1}>
                <GridColumn>
                  <h2>Параметры просчёта стоимости</h2>
                </GridColumn>
              </GridRow>
              <GridRow columns={2}>
                <GridColumn>
                  { !printWork.blockPrice?
                    <Fragment/>:
                    <Segment raised>
                      <Grid columns={2}>
                        <GridRow>
                          <GridColumn>Стоимость блока:</GridColumn>
                          <GridColumn>{printWork.blockPrice}</GridColumn>
                        </GridRow>
                      </Grid>
                    </Segment>
                  }
                </GridColumn>
                <GridColumn>
                  {!printWork.squareMeterPrice ?
                    <Fragment/> :
                    <Segment raised>
                      <Grid columns={2}>
                        <GridRow columns={2}>
                          <GridColumn>Цена за квадратный метр:</GridColumn>
                          <GridColumn>{printWork.squareMeterPrice}</GridColumn>
                        </GridRow>
                      </Grid>
                    </Segment>
                  }
                </GridColumn>
              </GridRow>
              <GridRow columns={2}>
                <GridColumn>
                  {!printWork.perimeterPrice ?
                    <Fragment/> :
                    <Segment raised>
                      <Grid columns={2}>
                        <GridRow columns={2}>
                          <GridColumn>Цена за периметр:</GridColumn>
                          <GridColumn>{printWork.perimeterPrice}</GridColumn>
                        </GridRow>
                      </Grid>
                    </Segment>
                  }
                </GridColumn>
                <GridColumn>
                  {!printWork.oneTimePrice ?
                    <Fragment/> :
                    <Segment raised>
                      <Grid columns={2}>
                        <GridRow columns={2}>
                          <GridColumn>Разовая Цена:</GridColumn>
                          <GridColumn>{printWork.oneTimePrice}</GridColumn>
                        </GridRow>
                      </Grid>
                    </Segment>
                  }
                </GridColumn>
              </GridRow>
            </Grid>
            <Divider horizontal/>
            <h2>Материалы</h2>
            <Table celled columns={3}>
              <TableHeader>
                <TableRow>
                  <TableHeaderCell>Id</TableHeaderCell>
                  <TableHeaderCell>Название</TableHeaderCell>
                  <TableHeaderCell>Количество</TableHeaderCell>
                </TableRow>
              </TableHeader>
              <TableBody>
                {printWork.blockMaterials !== [] ? printWork.blockMaterials.map(material => (
                  <MaterialRow key={material.id} material={material}/>)) : <Fragment/>
                }
                {printWork.squareMeterMaterials !== [] ? printWork.squareMeterMaterials.map(material => (
                  <MaterialRow key={material.id} material={material}/>)) : <Fragment/>
                }
                {printWork.oneTimeMaterials !== [] ? printWork.oneTimeMaterials.map(material => (
                  <MaterialRow key={material.id} material={material}/>)) : <Fragment/>
                }
                {printWork.perimeterMaterials !== [] ? printWork.perimeterMaterials.map(material => (
                  <MaterialRow key={material.id} material={material}/>)) : <Fragment/>
                }
              </TableBody>
            </Table>
          </div>
          <div style={{paddingTop: '10px', marginTop: '64px'}}/>
        </ModalContent>
      <ModalActions>
        <Button positive content='Закрыть' onClick={async () => await this.modalOpened(false)}/>
        {this.state.success ?
          <EditPrintWork utils={this.props.utils} printWork={printWork} onClose={
            async () => {
              await this.modalOpened(false);
              await this.reload();
            }}/> :
          <Fragment/>
        }
        <DeletePrintWork utils={this.props.utils} id={printWork.id}
                         onDeleted={async () => {
                           await this.modalOpened(false);
                           await this.reload();
                         }}
        />
      </ModalActions>
    </Modal>
  }

  async modalOpened(state) {
    await this.setState({modalOpened: state});
  }

  renderButton() {
    let {printWork} = this.props;
    return (<TableRow key={printWork.id} onClick={async () => await this.modalOpened(true)}>
        <TableCell>{printWork.id}</TableCell>
        <TableCell>{printWork.name}</TableCell>
        <TableCell>{RenderHelper.renderPrintWorkType(printWork.type)}</TableCell>
      </TableRow>
    )
  }
}