import React, {Component} from 'react';
import endpoints from '../../../../../config/endpoints';
import axios from 'axios/index';
import {Button, Header, Icon, Modal, ModalActions, ModalContent} from 'semantic-ui-react';

export default class DeletePrintWork extends Component {
  toast = async (toast) => {
    await this.props.utils.toast(toast);
  };
  onDeleted = async () => {
    await this.props.onDeleted();
  };

  constructor(props) {

    super(props);

    this.state = {
      id: this.props.id,
      modalOpened: false,
      modalLoading: false
    }
  }

  async delPrintWork(id) {
    try {
      await this.modalLoading(true);
      await axios({
        url: endpoints.api.order.print_work.deletePrintWorkById,
        method: 'post',
        auth: this.props.utils.user,
        data: id,
        headers: {
          "Content-Type": "application/json",
        }
      });
      await this.toast({
        visible: true,
        color: 'green',
        header: 'Панель Администратора',
        body: 'Печатная работа удалена!'
      });
    } catch (e) {
      console.error(e);
      await this.toast({
        visible: true,
        color: 'red',
        header: 'Панель Администратора',
        body: 'Не удалось удалить печатную работу!'
      });
    } finally {
      await this.modalLoading(false);
      await this.modalOpened(false);
      await this.onDeleted();
    }
  };

  render() {
    return (<Modal trigger={this.renderButton()}
                   open={this.state.modalOpened}
                   basic
                   onClose={() => this.modalOpened(false)}>
        <Header icon='trash alternate outline' content="Удаление печатной работы‽"/>
        <ModalContent>
          Вы уверены, что хотите удалить печатную работу?
        </ModalContent>
        <ModalActions>
          <Button basic inverted onClick={() => this.modalOpened(false)}>
            <Icon name='remove'/> Нет
          </Button>
          <Button color='red' inverted
                  loading={this.state.modalLoading}
                  onClick={async () => await this.delPrintWork(this.state.id)}>
            <Icon name='trash alternate outline'/> Да
          </Button>
        </ModalActions>
      </Modal>
    );
  }

  async modalOpened(state) {
    await this.setState({modalOpened: state});
  }

  async modalLoading(state) {
    await this.setState({modalLoading: state});
  }

  renderButton() {
    return (<Button disabled={this.props.disabled} icon={'trash alternate outline'}
                    color={'red'}
                    onClick={() => this.modalOpened(true)}/>);
  }

}