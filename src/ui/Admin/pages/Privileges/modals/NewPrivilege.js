import React, { Component, Fragment } from 'react';
import {
  Button,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader
} from 'semantic-ui-react';
import endpoints from '../../../../../config/endpoints';
import axios from 'axios';

export default class NewPrivilege extends Component {
  toast = async (state) => {
    this.props.utils.toast(state);
  };

  constructor(props) {
    super(props);

    this.state = {
      form        : {
        name   : '',
        pattern: ''
      },
      modalOpened : false,
      modalLoading: false
    };

    this.createPrivilege = this.createPrivilege.bind(this);
  }

  async loadContext() {
    await this.modalLoading(true);
  }

  async componentDidMount() {
    await this.loadContext();
    await this.modalLoading(false);
  }

  async createPrivilege() {
    let privilege = this.state.form;

    await this.modalLoading(true);
    try {
      await axios({
                    method: "post",
                    url   : endpoints.api.users.privileges.create,
                    data  : privilege,
                    auth  : this.props.utils.user
                  });
      await this.toast({
                         visible: true,
                         header : 'Панель Администратора',
                         color  : 'teal',
                         body   : 'Создание привилегии успешно!'
                       });
    } catch(e) {
      await this.toast({
                         visible: true,
                         header : 'Панель Администратора',
                         color  : 'red',
                         body   : 'Не удалось создать привилегии!'
                       });
    } finally {
      //noinspection JSUnresolvedFunction
      await this.props.onChange();
      await this.modalLoading(false);
      await this.modalOpened(false);
    }
  }

  async modalOpened(state) {
    await this.setState({ modalOpened: state });
  }

  renderButton() {
    return (<Button disabled={this.props.disabled} icon labelPosition={'right'} primary onClick={async () => await this.modalOpened(true)}>
      <Icon name={'shield alternate'} />
      Создать привилегию
    </Button>);
  }

  render() {
    return (<Fragment>
          <Modal
              trigger={this.renderButton()}
              style={{ marginTop: 0 }}
              open={this.state.modalOpened}
              onClose={async () => await this.modalOpened(false)}><ModalHeader>
            Новая привилегия
          </ModalHeader>
            <ModalContent>
              <Form>
                <Grid>
                  <GridRow>
                    <GridColumn>
                      <FormField>
                        <label>Название</label>
                        <input placeholder='напр. admin'
                               defaultValue={this.state.form.name}
                               onChange={(e) => this.updateForm('name', e.target.value)} />
                      </FormField>
                    </GridColumn>
                  </GridRow>
                  <GridRow>
                    <GridColumn>
                      <FormField>
                        <label>Шаблон</label>
                        <input placeholder='напр. admin.*'
                               defaultValue={this.state.form.pattern}
                               onChange={(e) => this.updateForm('pattern', e.target.value)} />
                      </FormField>
                    </GridColumn>
                  </GridRow>
                </Grid>
              </Form>
            </ModalContent>
            <ModalActions>
              <Button
                  onClick={async () => await this.modalOpened(false)}>
                Cancel
              </Button>
              <Button color={'green'}
                      disabled={this.state.modalLoading}
                      loading={this.state.modalLoading}
                      icon
                      labelPosition={'right'}
                      onClick={async () => await this.createPrivilege()}>
                <Icon name={'arrow circle down'} />
                Save
              </Button>
            </ModalActions>

          </Modal>
        </Fragment>
    )
  }

  async updateForm(name, value) {
    let { form } = this.state;
    form[name]   = value;
    await this.setState({ form });
  }

  async modalLoading(state) {
    await this.setState({ modalLoading: state });
  }
}