import React, { Component, Fragment } from 'react';
import {
  Button,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader
} from 'semantic-ui-react';
import endpoints from '../../../../../config/endpoints';
import axios from 'axios';

export default class EditPrivilege extends Component {
  toast = async (state) => {
    this.props.utils.toast(state);
  };

  constructor(props) {
    super(props);

    this.state = {
      form        : {
        id     : this.props.id,
        name   : '',
        pattern: ''
      },
      modalOpened : false,
      modalLoading: false
    };

    this.alterPrivilege = this.alterPrivilege.bind(this);
  }

  async loadContext() {
    await this.modalLoading(true);
    await this.loadPrivilegeData();
  }

  async loadPrivilegeData() {
    let { id }   = this.state.form;
    let { data } = await axios({
                                 method: 'get',
                                 url   : endpoints.api.users.privileges.getById,
                                 params: { id },
                                 auth  : this.props.utils.user
                               });
    console.debug(data);
    await this.setState({ form: data });
  };

  async componentDidMount() {
    await this.loadContext();
    await this.modalLoading(false);
  }

  async alterPrivilege() {
    let privilege = this.state.form;

    await this.modalLoading(true);
    try {
      await axios({
                    method: "post",
                    url   : endpoints.api.users.privileges.create,
                    data  : privilege,
                    auth  : this.props.utils.user
                  });
      await this.toast({
                         visible: true,
                         header : 'Панель Администратора',
                         color  : 'teal',
                         body   : 'Изменение привилегии успешно!'
                       });
    } catch(e) {
      await this.toast({
                         visible: true,
                         header : 'Панель Администратора',
                         color  : 'red',
                         body   : 'Не удалось изменить привилегию!'
                       });
    } finally {
      //noinspection JSUnresolvedFunction
      await this.props.onChange();
      await this.modalLoading(false);
      await this.modalOpened(false);
    }
  }

  async modalOpened(state) {
    await this.setState({ modalOpened: state });
  }

  renderButton() {
    return (
        <Button disabled={this.props.disabled} onClick={() => this.modalOpened(true)} color={'teal'} icon={'edit'} />);
  }

  render() {
    return (<Fragment>
          <Modal
              trigger={this.renderButton()}
              style={{ marginTop: 0 }}
              open={this.state.modalOpened}
              onClose={async () => await this.modalOpened(false)}>
            <ModalHeader content='Редактирование' />
            <ModalContent>
              <Form>
                <Grid>
                  <GridRow>
                    <GridColumn>
                      <FormField>
                        <label>Id</label>
                        <input readOnly
                               defaultValue={this.state.form.id} />
                      </FormField>
                    </GridColumn>
                  </GridRow>
                  <GridRow>
                    <GridColumn>
                      <FormField>
                        <label>Название</label>
                        <input placeholder='напр. admin'
                               defaultValue={this.state.form.name}
                               onChange={(e) => this.updateForm('name', e.target.value)} />
                      </FormField>
                    </GridColumn>
                  </GridRow>
                  <GridRow>
                    <GridColumn>
                      <FormField>
                        <label>Шаблон</label>
                        <input placeholder='напр. admin.*'
                               defaultValue={this.state.form.pattern}
                               onChange={(e) => this.updateForm('pattern', e.target.value)} />
                      </FormField>
                    </GridColumn>
                  </GridRow>
                </Grid>
              </Form>
            </ModalContent>
            <ModalActions>
              <Button
                  onClick={async () => await this.modalOpened(false)}>
                Отмена
              </Button>
              <Button color={'green'}
                      disabled={this.state.modalLoading}
                      loading={this.state.modalLoading}
                      icon
                      labelPosition={'right'}
                      onClick={async () => await this.alterPrivilege()}>
                <Icon name={'arrow circle down'} />
                Сохранить
              </Button>
            </ModalActions>

          </Modal>
        </Fragment>
    )
  }

  async updateForm(name, value) {
    let { form } = this.state;
    form[name]   = value;
    await this.setState({ form });
  }

  async modalLoading(state) {
    await this.setState({ modalLoading: state });
  }
}