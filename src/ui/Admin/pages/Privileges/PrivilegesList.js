import React, { Component } from 'react'
import {
  Button,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHeader,
  TableHeaderCell,
  TableRow
} from 'semantic-ui-react';
import endpoints from '../../../../config/endpoints';
import axios from 'axios'
import EditPrivilege from './modals/EditPrivilege';
import DeletePrivilege from './modals/DeletePrivilege';
import NewPrivilege from './modals/NewPrivilege';
import RenderHelper from '../../../../utils/RenderHelper';

export default class PrivilegesList extends Component {

  constructor(props) {

    super(props);
    this.state = {
      privileges: [],
      page      : 0,
    };

    this.loadDataPage = this.loadDataPage.bind(this);
  }

  async componentDidMount() {
    await this.loadContext();
    await this.props.utils.loading(false);
  }

  async loadContext() {
    await this.props.utils.loading(true);
    await this.loadDataPage();
  }

  async loadDataPage(size = 12) {
    let { page } = this.state;
    await this.props.utils.loading(true);
    try {
      let { data } = await axios(
          {
            method: 'get',
            url   : endpoints.api.users.privileges.getPage,
            params: { page: page, size: size },
            auth  : this.props.utils.user
          });
      await this.setState({ privileges: data });
      await this.props.utils.loading(false);
    } finally {
      await this.props.utils.loading(false);
    }
  }

  render() {

    return (

        <Grid container>
          <GridRow>
            <GridColumn>
              <Table celled>
                <TableHeader>
                  <TableRow>
                    <TableHeaderCell colSpan={4}>
                      <Grid>
                        <GridRow>
                          <GridColumn textAlign={'left'}>
                            <NewPrivilege
                                utils={this.props.utils}
                                onChange={async () => await this.loadDataPage()}
                            />
                          </GridColumn>
                        </GridRow>
                      </Grid>
                    </TableHeaderCell>
                  </TableRow>
                  <TableRow>
                    <TableHeaderCell>ID</TableHeaderCell>
                    <TableHeaderCell>Имя</TableHeaderCell>
                    <TableHeaderCell>Шаблон Доступа</TableHeaderCell>
                    <TableHeaderCell>Действия</TableHeaderCell>
                  </TableRow>
                </TableHeader>
                <TableBody>
                  {this.state.privileges.map((privilege) => (
                      <TableRow key={privilege.id}>
                        <TableCell>{privilege.id}</TableCell>
                        <TableCell>{privilege.name}</TableCell>
                        <TableCell>{RenderHelper.renderPattern(privilege.pattern)}</TableCell>
                        <TableCell>
                          <EditPrivilege
                              utils={this.props.utils}
                              id={privilege.id}
                              onDeleted={async () => await this.loadDataPage()}
                          />
                          <DeletePrivilege
                              utils={this.props.utils}
                              id={privilege.id}
                              onChange={async () => await this.loadDataPage()}
                          />
                        </TableCell>
                      </TableRow>
                  ))}
                </TableBody>
                <TableFooter>
                  <TableRow>
                    <TableHeaderCell colSpan={4}>
                      <Grid>
                        <GridRow>
                          <GridColumn textAlign={'right'}>
                            <Button primary onClick={async () => {
                              await this.rewindPage();
                            }}>
                              <Icon name={'chevron left'} />
                              Назад
                            </Button>
                            <Button primary onClick={async () => {
                              await this.forwardPage();
                            }}>
                              Вперед
                              <Icon name={'chevron right'} />
                            </Button>
                          </GridColumn>
                        </GridRow>
                      </Grid>
                    </TableHeaderCell>
                  </TableRow>
                </TableFooter>
              </Table>
            </GridColumn>
          </GridRow>

        </Grid>
    )
  }

  async forwardPage() {
    let { page } = this.state;
    await this.setState({ page: page + 1 });
    await this.loadDataPage();
    if(this.state.privileges.length <= 0) await this.rewindPage();
  }

  async rewindPage() {
    let { page } = this.state;
    await this.setState({ page: Math.max(page - 1, 0) });
    await this.loadDataPage();
  }

}