/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * This file is part of com.maddyhome.idea.copyright.pattern.ProjectInfo@6edfcf60.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

import React, { Component, Fragment } from 'react';
import UsersList from "./pages/Users/UsersList";
import { Menu, MenuItem } from 'semantic-ui-react';
import RolesList from './pages/Roles/RolesList';
import PrivilegesList from './pages/Privileges/PrivilegesList';
import PrintWorkList from './pages/PrintWorks/PrintWorkList';
import Config from './pages/configs/Config';
import Storage from './pages/Storage/pages/Storage';

export default class Admin extends Component {

  handleItemClick = (e, { name, data }) => {

    if(name in this.functionItems) {
      this.functionItems[name]();
    } else {
      this.setState({ activeItem: name, data: data })
    }
  };

  constructor(props) {

    super(props);

    this.state         = {
      activeItem: 'UsersList',
      data      : null
    };
    this.itemsView     = {
      'UsersList'     : (
          <UsersList utils={this.props.utils} handler={this.handleItemClick} />),
      'PrivilegesList': (
          <PrivilegesList
              utils={this.props.utils}
              handler={this.handleItemClick} />),
      'RolesList'     : (
          <RolesList utils={this.props.utils} handler={this.handleItemClick} />),

      'PrintWorkList': (
          <PrintWorkList utils={this.props.utils} handler={this.handleItemClick} />),

      'Configs': (
          <Config utils={this.props.utils} handler={this.handleItemClick} />),

      'Storage': (
          <Storage utils={this.props.utils} handler={this.handleItemClick} />
      )
    };
    this.functionItems = {
      'BackToMain': () => {
        this.props.backToMain();
      }
    }
  }

  render() {
    let { activeItem } = this.state;
    return (
        <Fragment>
          <Menu pagination={true} fluid inverted={true} color={'blue'} fixed={'top'}>
            <MenuItem
                name='BackToMain'
                active={activeItem === 'BackToMain'}
                onClick={this.handleItemClick}
            >
              Назад
            </MenuItem>

            <MenuItem
                name='UsersList'
                active={activeItem === 'UsersList'}
                onClick={this.handleItemClick}
            >
              Пользователи
            </MenuItem>

            <MenuItem
                name='RolesList'
                active={activeItem === 'RolesList'}
                onClick={this.handleItemClick}
            >
              Роли
            </MenuItem>
            <MenuItem
                name='PrivilegesList'
                active={activeItem === 'PrivilegesList'}
                onClick={this.handleItemClick}
            >
              Именованные привилегии
            </MenuItem>
            <MenuItem
                name='Storage'
                active={activeItem === 'Storage'}
                onClick={this.handleItemClick}
            >
              Прайс-лист
            </MenuItem>
            <MenuItem
                name='PrintWorkList'
                active={activeItem === 'PrintWorkList'}
                onClick={this.handleItemClick}
            >
              Печатные работы
            </MenuItem>
            <MenuItem
                name='Configs'
                active={activeItem === 'Config'}
                onClick={this.handleItemClick}
            >
              Настройки
            </MenuItem>
            <MenuItem position={'right'} header>Панель администратора</MenuItem>
          </Menu>
          <div style={{ paddingTop: '10px', marginTop: '64px' }}>
            {this.itemsView[activeItem]}
          </div>
        </Fragment>
    );
  }
}