import React, { Component, Fragment } from 'react'
import { Menu, MenuItem } from 'semantic-ui-react'
import InvoiceList from './pages/Invoices/InvoiceList';

export default class  Manager extends Component {

  handleItemClick = (e, { name }) => {
    if(name in this.funtionItems) {
      this.funtionItems[name]();
    } else {
      this.setState({ activeItem: name })
    }
  };

  constructor(props) {
    super(props);
    this.state        = {
      activeItem: 'InvoiceList',
    };
    this.itemsView    = {
      'InvoiceList': (<InvoiceList utils={this.props.utils} />),
      'Admin'      : (<Fragment />)
    };
    this.funtionItems = {
      'BackToMain': () => this.props.backToMain(),
    }
  }

  render() {
    let { activeItem } = this.state;
    return (
        <Fragment>
          <Menu pagination={true} fluid inverted={true} color={'blue'} fixed={'top'}>
            <MenuItem
                name='BackToMain'
                active={activeItem === 'BackToMain'}
                onClick={this.handleItemClick}
                content='Назад'
            />

            <MenuItem
                name='InvoiceList'
                active={activeItem === 'InvoiceList'}
                onClick={this.handleIconClick}
                content='Список Счетов'
            />

            <MenuItem
                position={'right'}
                header
                content='Панель Менеджера'
            />
          </Menu>
          <div style={{ paddingTop: '10px', marginTop: '64px' }}>
            {this.itemsView[activeItem]}
          </div>
        </Fragment>
    );
  }
}