import _ from 'lodash';
import qs from 'qs';
import React, { Component, Fragment } from 'react'
import {
  Button,
  Grid,
  GridColumn,
  GridRow,
  Input,
  Message,
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderCell,
  TableRow,
} from 'semantic-ui-react'
import axios from "axios/index";
import endpoints from '../../../../config/endpoints';
import NewInvoice from "./modals/NewInvoice";
import InvoiceRow from "./modals/elements/InvoiceRow";

export default class InvoiceList extends Component {

  state = {
    invoices: [],
    page    : 0,
    success : true,
    filter  : {},
    timeout : null
  };

  constructor(props) {
    super(props);
    this.loadPage = this.loadPage.bind(this);
  }

  async componentDidMount() {
    await this.props.utils.loading(true);
    await this.loadContext();
    await this.props.utils.loading(false);
  }

  async loadContext() {
    await this.loadPage();
  }

  async forward(delta_page = 1, size = 12) {
    let { page } = this.state;
    await this.loadPage(page + delta_page, size);
  }

  async back(delta_page = -1, size = 12) {
    let { page } = this.state;
    await this.loadPage(Math.max(page + delta_page, 0), size);
  }

  //

  async loadPage(page = this.state.page, size = 12) {
    let { modalOpen, success } = this.state;
    //
    await this.props.utils.loading(true);
    try {
      let _filters = _.map(Object.entries(this.state.filter), e => e.join(''));

      let res = await axios({
                              method          : 'get',
                              url             : endpoints.api.manager.invoices.getPage,
                              params          : {
                                page   : page,
                                size   : size,
                                filters: _filters,
                                sorts  : [('dateCreated:DESC')]
                              },
                              paramsSerializer: (params) => qs.stringify(params, { arrayFormat: 'brackets' }),
                              auth            : this.props.utils.user

                            });
      //
      if(res.data.length !== 0) {
        await this.setState({
                              invoices : res.data,
                              success  : success,
                              page     : page,
                              modalOpen: modalOpen,
                            });
      }
    } catch(e) {
      console.error(e);
      await this.setState({ success: false });
    } finally {
      await this.props.utils.loading(false);
    }
  }

  render() {
    let { invoices } = this.state;
    if(!this.state.success) {
      this.props.utils.toast({
                               visible: true,
                               header : 'Панель менеджера',
                               color  : 'red',
                               body   : 'Не известный ошибка случатся моя чинить, однако твоя не надейся, моя плохо чинить!'
                             });
    }
    return (
        <Fragment>
          {
            this.state.success ?
                <Grid container>
                  <GridRow>
                    <GridColumn>
                      <Table celled compact definition selectable>
                        <TableHeader fullWidth>
                          <TableRow>
                            <TableHeaderCell colSpan={8}>
                              <Grid>
                                <GridRow>
                                  <GridColumn textAlign={'right'}>
                                    <NewInvoice utils={this.props.utils} refresh={async () => await this.loadPage()} />
                                  </GridColumn>
                                </GridRow>
                              </Grid>
                            </TableHeaderCell>
                          </TableRow>
                          <TableRow>
                            <TableHeaderCell rowSpan={2}>ID</TableHeaderCell>
                            <TableHeaderCell rowSpan={2}>Название</TableHeaderCell>
                            <TableHeaderCell rowSpan={2}>Date</TableHeaderCell>
                            <TableHeaderCell>Клиент</TableHeaderCell>
                            <TableHeaderCell>Менеджер</TableHeaderCell>
                            <TableHeaderCell rowSpan={2}>Id 1c</TableHeaderCell>
                            <TableHeaderCell rowSpan={2}>Цена</TableHeaderCell>
                            <TableHeaderCell rowSpan={2}>Статус</TableHeaderCell>
                          </TableRow>
                          <TableRow>
                            <TableHeaderCell>
                              <Input type={'text'}
                                     value={this.state.filter['$text=$search|']}
                                     onChange={async (e, { value }) => await this.setFilter('$text=$search|',
                                                                                            value)} />
                            </TableHeaderCell>
                            <TableHeaderCell>
                              <Input type={'text'}
                                     value={this.state.filter['manager=$eq|']}
                                     onChange={async (e, { value }) => await this.setFilter('manager=$eq|',
                                                                                            value)} />
                            </TableHeaderCell>
                          </TableRow>
                        </TableHeader>

                        <TableBody>
                          {invoices.map((invoice) => (
                              <InvoiceRow key={invoice.id} utils={this.props.utils} invoice={invoice} />)
                          )}
                        </TableBody>

                        <TableFooter fullWidth>
                          <TableRow>
                            <TableHeaderCell colSpan={8}>
                              <Grid>
                                <GridRow>
                                  <GridColumn textAlign={'right'}>
                                    <Button size='small'
                                            primary
                                            icon={'chevron left'}
                                            labelPosition={'left'}
                                            onClick={async () => await this.back()}
                                            content='Назад' />
                                    <Button size='small'
                                            primary
                                            icon={'chevron right'}
                                            labelPosition={'right'}
                                            onClick={async () => await this.forward()}
                                            content='Вперед' />
                                  </GridColumn>
                                </GridRow>
                              </Grid>
                            </TableHeaderCell>
                          </TableRow>
                        </TableFooter>
                      </Table>

                    </GridColumn>
                  </GridRow>
                </Grid> :
                <div>
                  <Message error header='Error' content='can not load page ' />
                </div>
          }
        </Fragment>
    )
  }

  async setFilter(name, value) {
    let { filter } = this.state;

    if(value) {
      filter[name] = value;
    } else {
      delete filter[name];
    }
    await this.setState({ filter });

    clearTimeout(this.state.timeout);

    let timeout = setTimeout(async () => {
      await this.loadPage();
    }, 500);

    await this.setState({ timeout });
  }
}