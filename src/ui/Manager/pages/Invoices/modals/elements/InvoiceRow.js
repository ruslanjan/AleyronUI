import React, { Component } from 'react';
import {
  Button,
  Grid,
  GridColumn,
  GridRow,
  Input,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader,
  Popup,
  TableCell,
  TableRow
} from 'semantic-ui-react';
import InvoiceMeta from './InvoiceMeta';
import RenderHelper from '../../../../../../utils/RenderHelper';
import * as axios from 'axios';
import endpoints from '../../../../../../config/endpoints';
import InvoiceOrders from './InvoiceOrders';
import Man from "../../../../../../utils/elements/Man";

export default class InvoiceRow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      invoice    : this.props.invoice,
      modalOpened: false,
    };
  }

  async reloadRow() {
    try {
      let { data } = await axios({
                                   method: 'get',
                                   url   : endpoints.api.manager.invoices.getById,
                                   params: { id: this.state.invoice.id },
                                   auth  : this.props.utils.user
                                 });
      await this.setState({ invoice: (data) });
    } catch(e) {
      console.error(e);
      await this.props.utils.toast({
                                     visible: true,
                                     header : 'Панель менеджера',
                                     body   : 'Внутреняя ошибка.',
                                     color  : 'red',
                                     delay  : 3000
                                   });
    } finally {
    }
  }

  async componentDidMount(){
    await this.setState({invoice    : this.props.invoice});
  }

  render() {
    let { modalOpened, invoice } = this.state;
    return <Modal
        onClose={() => this.modalOpened(false)}
        style={{ marginTop: 0 }}
        trigger={this.renderButton()}
        open={modalOpened}
    >
      <ModalHeader>
        Счет
      </ModalHeader>
      <ModalContent scrolling>
        <Grid container>
          <GridRow>
            <GridColumn>
              <InvoiceMeta invoice={invoice}
                           utils={this.props.utils}
                           disabled={this.state.invoice.status !== 'FORMING'} />
            </GridColumn>
          </GridRow>
          <GridRow>
            <GridColumn>
              <InvoiceOrders onChange={async () => await this.reloadRow()}
                             invoice={invoice}
                             utils={this.props.utils}
                             disabled={this.state.invoice.status !== 'FORMING'} />
            </GridColumn>
          </GridRow>
        </Grid>
        <div style={{ paddingTop: '10px', marginTop: '64px' }} />
      </ModalContent>
      <ModalActions>
        <Button color={'red'} inverted floated={'left'} content='закрыть' onClick={() => this.modalOpened(false)} />
        <Popup trigger={<Button negative
                                disabled={this.state.invoice.status !== 'FORMING'}
                                content='Отменить'
                                loading={this.state.declineButtonLoading} />} on={'click'}
               content={<Button negative
                                content='Вы уверены?'
                                loading={this.state.declineButtonLoading}
                                onClick={() => this.declineInvoice()} />} />
        <Popup wide trigger={<Button positive
                                     disabled={this.state.invoice.status !== 'FORMING'}
                                     content='Сформировать'
                                     loading={this.state.approveButtonLoading} />} on={'click'}
               content={
                 <Input
                     onChange={async (e, { value }) => await this.setState({ invoice1C: value })}
                     placeholder={'ИД 1C счета'}
                     action={{
                       positive: true, content: 'Вы уверены?', loading: this.state.approveButtonLoading,
                       onClick : async () => await this.approveInvoice()
                     }} />}
        />
      </ModalActions>
    </Modal>
  }

  async modalOpened(state) {
    await this.setState({ modalOpened: state });
  }

  renderButton() {
    let { invoice } = this.state;

    return (<TableRow key={invoice.id} onClick={async () => {
          await this.modalOpened(true)
        }}>
          <TableCell>{invoice.id}</TableCell>
          <TableCell>{invoice.title}</TableCell>
        <TableCell>{RenderHelper.renderDate(invoice.created || invoice.dateCreated)}</TableCell>
        <TableCell>{<Man invoice={invoice}/>}</TableCell>
          <TableCell>{invoice.manager}</TableCell>
          <TableCell>{invoice.invoice1C}</TableCell>
          <TableCell>{invoice.cost}</TableCell>
          <TableCell>{RenderHelper.renderInvoiceStatus(invoice.status)}</TableCell>
        </TableRow>
    )
  }

  async declineInvoice() {
    await this.declineButtonLoading(true);
    try {
      await axios({
                    method : 'post',
                    url    : endpoints.api.manager.invoices.cancel,
                    data   : this.state.invoice.id,
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    auth   : this.props.utils.user
                  });
    } catch(e) {
      console.error(e);
      await this.props.utils.toast({
                                     visible: true,
                                     header : 'Панель менеджера',
                                     body   : 'Внутреняя ошибка.',
                                     delay  : 3000,
                                     color  : 'red'
                                   });
    } finally {
      await this.declineButtonLoading(false);
    }
  }

  async approveInvoice() {
    await this.approveButtonLoading(true);
    try {
      await axios({
                    method : 'post',
                    url    : endpoints.api.manager.invoices.update,
                    data   : {
                      ...this.state.invoice,
                      status   : 'PENDING',
                      invoice1C: this.state.invoice1C
                    },
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    auth   : this.props.utils.user
                  });
//      await axios({
//                    method : 'post',
//                    url    : endpoints.api.manager.invoices.form,
//                    data   : this.state.invoice.id,
//                    headers: {
//                      'Content-Type': 'application/json'
//                    },
//                    auth   : this.props.utils.user
//                  });
    } catch(e) {
      console.error(e);
      await this.props.utils.toast({
                                     visible: true,
                                     header : 'Панель менеджера',
                                     body   : 'Внутреняя ошибка.',
                                     delay  : 3000,
                                     color  : 'red'
                                   });
    } finally {
      await this.approveButtonLoading(false);
    }
  }

  async declineButtonLoading(declineButtonLoading) {
    await this.setState({ declineButtonLoading });
  }

  async approveButtonLoading(approveButtonLoading) {
    await this.setState({ approveButtonLoading });
  }
}