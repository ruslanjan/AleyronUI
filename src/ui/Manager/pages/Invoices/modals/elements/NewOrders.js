import _ from 'lodash';
import React, { Component } from 'react';
import { Dropdown, DropdownMenu } from 'semantic-ui-react';
import AddDesignOrder from '../orders/adders/AddDesignOrder';
import AddDigitalPrintOrder from '../orders/adders/AddDigitalPrintOrder';

export default class NewOrders extends Component {
  render() {
    const orderTypes = [
      {
        id    : 'DESIGN',
        render: <AddDesignOrder onChange={async () => await this.props.onChange()}
                                invoiceId={this.props.invoiceId}
                                key={'DESIGN'}
                                utils={this.props.utils} />
      },
      {
        id    : 'STICKER',
        render: <AddDigitalPrintOrder onChange={async () => await this.props.onChange()}
                                      invoiceId={this.props.invoiceId}
                                      key={'STICKER'}
                                      type={'STICKER'}
                                      utils={this.props.utils} />
      },
      {
        id    : 'WIDE_FORMAT_PRINT',
        render: <AddDigitalPrintOrder onChange={async () => await this.props.onChange()}
                                      invoiceId={this.props.invoiceId}
                                      key={'WIDE_FORMAT_PRINT'}
                                      type={'WIDE_FORMAT_PRINT'}
                                      utils={this.props.utils} />
      }
    ];

    return (
        <Dropdown disabled={this.props.disabled}
                  text={'Новый заказ'}
                  icon={'plus'}
                  floating
                  labeled
                  button
                  className={'icon'}>
          <DropdownMenu>
            {_.map(orderTypes, type => type.render)}
          </DropdownMenu>
        </Dropdown>
    );
  }
}