import _ from 'lodash';
import React, { Component, Fragment } from 'react';
import RenderHelper from '../../../../../../utils/RenderHelper';
import {
  Button,
  Checkbox,
  Dimmer,
  DimmerDimmable,
  Grid,
  GridColumn,
  GridRow,
  Input,
  Loader,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHeader,
  TableHeaderCell,
  TableRow
} from 'semantic-ui-react';
import * as axios from 'axios';
import endpoints from '../../../../../../config/endpoints';
import Man from "../../../../../../utils/elements/Man";

export default class InvoiceMeta extends Component {
  state = {
    edit   : false,
    invoice: {
      id        : '',
      title     : '',
      manager   : '',
      cost      : 0,
      clientName: ''
    }
  };

  async componentDidMount() {
    await this.loading(true);
    await this.loadContext();
    await this.loading(false);
  }

  async loadContext() {
    await this.reloadOrder();
  }

  async reloadOrder() {
    try {
      let { id }   = this.props.invoice;
      let { data } = await axios({
                                   method: 'GET',
                                   url   : endpoints.api.manager.invoices.getById,
                                   params: { id },
                                   auth  : this.props.utils.user
                                 });
      await this.setState({ invoice: data });
    } catch(e) {
      console.error(e);
      await this.toast({
                         visible: true,
                         color  : 'red',
                         delay  : 3000,
                         header : 'Панель менеджера',
                         body   : 'Внутреняя ошибка!'
                       });
    }
  }

  async componentDidUpdate(prevProps) {
    if(_.isEqual(this.props, prevProps)) return;
    await this.loadContext();
  }

  async setInvoice(name, value) {
    let { invoice } = this.state;
    invoice[name]   = value;
    await this.setState({ invoice });
  }

  render() {
    return (
        <DimmerDimmable dimmed={this.state.loading} blurring>
          <Dimmer active={this.state.loading} inverted>
            <Loader />
          </Dimmer>
          <Table definition>
            <TableHeader fullWidth>
              <TableRow>
                <TableHeaderCell colSpan={2}>
                  <Grid>
                    <GridRow>
                      <GridColumn textAlign={'right'}>
                        <Checkbox toggle
                                  disabled={this.props.disabled}
                                  label={'Изменить'}
                                  checked={this.state.edit}
                                  onChange={async (e, { checked }) => {
                                    await this.edit(checked);
                                  }} />
                      </GridColumn>
                    </GridRow>
                  </Grid>
                </TableHeaderCell>
              </TableRow>
            </TableHeader>
            <TableBody>
              <TableRow>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  {(this.state.invoice).id}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  Название
                </TableCell>
                <TableCell>
                  {this.state.edit ?
                      <Input size={'small'} fluid value={(this.state.invoice).title}
                             onChange={(e) => (this.setInvoice('title', e.target.value))} /> :
                      <Fragment>{(this.state.invoice).title}</Fragment>}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  Клиент
                </TableCell>
                <TableCell>
                  {this.state.edit ?
                      <Input size={'small'} fluid value={(this.state.invoice).clientName}
                             onChange={(e) => (this.setInvoice('clientName', e.target.value))} />
                    : <Man invoice={this.state.invoice}/>
                  }
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  Мэнеджер
                </TableCell>
                <TableCell>
                  {<Fragment>{RenderHelper.withUtils(this.props.utils)
                                          .renderUser((this.state.invoice).manager)}</Fragment>}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  Статус
                </TableCell>
                <TableCell>
                  {<Fragment>{RenderHelper.renderInvoiceStatus((this.state.invoice).status)}</Fragment>}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  Цена
                </TableCell>
                <TableCell>
                  {<Fragment>{RenderHelper.renderCost((this.state.invoice).cost)}</Fragment>}
                </TableCell>
              </TableRow>
            </TableBody>
            {this.state.edit ? <TableFooter fullWidth>
              <TableRow>
                <TableHeaderCell colSpan={2}>
                  <Button primary
                          icon={'save'}
                          floated={'right'}
                          content={'сохранить'}
                          onClick={async () => await this.updateInvoiceMeta()} />
                </TableHeaderCell>
              </TableRow>
            </TableFooter> : <Fragment />}
          </Table>
        </DimmerDimmable>
    );
  }

  async edit(checked) {
    await this.setState({ edit: checked });
  }

  async loading(state) {
    await this.setState({ loading: state });
  }

  async updateInvoiceMeta() {
    let { invoice } = this.state;

    await this.loading(true);

    try {
      await axios({
                    method: 'post',
                    url   : endpoints.api.manager.invoices.update,
                    auth  : this.props.utils.user,
                    data  : invoice
                  });

      await this.props.utils.toast({
                                     header : 'Панель менеджера',
                                     body   : 'Изменения сохранены.',
                                     color  : 'green',
                                     delay  : 2000,
                                     visible: true
                                   });
      await this.edit(false);
      await this.setState({ invoice });

    } catch(e) {
      console.error(e);
      await this.props.utils.toast({
                                     header : 'Панель менеджера',
                                     body   : 'Не удалось сохранить.',
                                     color  : 'red',
                                     delay  : 2000,
                                     visible: true
                                   });
    } finally {
      await this.loading(false);
    }
  }
}