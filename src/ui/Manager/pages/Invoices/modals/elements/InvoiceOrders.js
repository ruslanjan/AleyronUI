import _ from 'lodash';
import React, { Component } from 'react';
import {
  Dimmer,
  DimmerDimmable,
  Grid,
  GridColumn,
  GridRow,
  Loader,
  Table,
  TableBody,
  TableHeader,
  TableHeaderCell,
  TableRow
} from 'semantic-ui-react';
import NewOrders from './NewOrders';
import * as axios from 'axios';
import endpoints from '../../../../../../config/endpoints';
import DigitalPrintOrderRow from '../orders/rows/DigitalPrintOrderRow';
import OrderRow from '../orders/OrderRow';
import DesignOrderRow from '../orders/rows/DesignOrderRow';

const orderTypeParents = {
  STICKER          : 'DIGITAL_PRINT',
  WIDE_FORMAT_PRINT: 'DIGITAL_PRINT',
  DESIGN           : 'DESIGN',
  DIGITAL_PRINT    : 'DIGITAL_PRINT'
};

export default class InvoiceOrders extends Component {
  state = {
    invoice: {
      orders: [],
    },
    loading: false
  };

  async componentDidUpdate(oldProps) {
    if(!_.isEqual(this.props.invoice, oldProps.invoice)) {
      await this.setState({ invoice: this.props.invoice });
    }
  }

  async componentDidMount() {
    await this.loadContext();
  }

  async loadContext() {
    await this.loading(true);
    await this.reloadInvoice();
    await this.loading(false);
  }

  async reloadInvoice() {
    await this.loading(true);
    try {
      let { id }   = this.props.invoice;
      let { data } = await axios({
                                   method: 'get',
                                   url   : endpoints.api.manager.invoices.getById,
                                   auth  : this.props.utils.user,
                                   params: { id }
                                 });

      await this.setState({ invoice: data });
    } catch(e) {
      console.error(e);
      this.props.utils.toast({
                               visible: true,
                               color  : 'red',
                               header : 'Панель менеджера',
                               body   : 'Неудалось перзагрузить!',
                               delay  : 3000
                             });
    } finally {
      await this.loading(false);
    }
  }

  async loading(loading) {
    await this.setState({ loading });
  }

  render() {
    return <DimmerDimmable dimmed={this.state.loading} blurring>
      <Dimmer active={this.state.loading} inverted>
        <Loader />
      </Dimmer>
      <Table celled selectable>
        <TableHeader fullWidth>
          <TableRow>
            <TableHeaderCell colSpan={7}>
              <Grid>
                <GridRow columns={2}>
                  <GridColumn>
                    Заказы
                  </GridColumn>
                  <GridColumn textAlign={'right'}>
                    <NewOrders onChange={() => this.onChange()}
                               disabled={this.props.disabled}
                               utils={this.props.utils}
                               invoiceId={this.state.invoice.id} />
                  </GridColumn>
                </GridRow>
              </Grid>
            </TableHeaderCell>
          </TableRow>
          <TableRow>
            <TableHeaderCell>
              ID
            </TableHeaderCell>
            <TableHeaderCell>
              Тип
            </TableHeaderCell>
            <TableHeaderCell>
              Дизайнер
            </TableHeaderCell>
            <TableHeaderCell>
              Количество
            </TableHeaderCell>
            <TableHeaderCell>
              Размер (ш/в, в метрах)
            </TableHeaderCell>
            <TableHeaderCell>
              Цена
            </TableHeaderCell>
          </TableRow>
        </TableHeader>
        <TableBody>
          {_.map(this.state.invoice.orders, (order) => {
            let type   = orderTypeParents[order.type];
            let _props = {
              disabled  : this.props.disabled,
              utils     : this.props.utils,
              order, key: order.id,
              onChange  : () => this.onChange()
            };
            switch(type) {
            case 'DIGITAL_PRINT':
              return <DigitalPrintOrderRow {..._props} />;
            case 'DESIGN':
              return <DesignOrderRow {..._props} />;
            default:
              return <OrderRow {..._props} />
            }
          })}
        </TableBody>
      </Table>
    </DimmerDimmable>
        ;
  }

  async onChange() {
    await
        this.reloadInvoice();
    await
        this.props.onChange();
  }
  ;

  // async loadOrders() {
//    try {
//      let { data } = await axios({
//                                   method: 'post',
//                                   auth  : this.props.utils.user,
//                                   url   : endpoints.api.manager.orders.getByIds,
//                                   params: {
//                                     ids: this.state.invoice.orderIds
//                                   }
//                                 });
//
//      let { invoice } = this.state;
//
//      this.setState({
//                      invoice: { ...invoice, orders: data }
//                    });
//    } catch(e) {
//      this.props.utils.toast({
//        visible: true,
//        header: ''
//                             });
//    } finally { }
//  }
}