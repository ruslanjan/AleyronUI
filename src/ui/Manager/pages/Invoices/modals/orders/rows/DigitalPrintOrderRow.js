import _ from 'lodash';
import React, { Component } from 'react';
import RenderHelper from '../../../../../../../utils/RenderHelper';
import {
  Button,
  Dropdown,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Input,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader,
  TextArea
} from 'semantic-ui-react';
import * as axios from 'axios';
import endpoints from '../../../../../../../config/endpoints';
import UnitConverter from '../../../../../../../utils/UnitConverter';
import OrderRow from '../OrderRow';
import DeleteOrder from '../../modals/DeleteOrder';
import PrintWorks from '../print_work/PrintWorks';

const orderTypes = {
  STICKER          : {
    unit              : 'mm',
    isTechCornersConst: true
  },
  WIDE_FORMAT_PRINT: {
    unit              : 'm',
    isTechCornersConst: false
  },
  UNKNOWN          : {
    unit              : 'm',
    isTechCornersConst: true
  }
};

export default class DigitalPrintOrderRow extends Component {
  state = {
    order             : {
      type             : 'UNKNOWN',
      design           : '',
      cost             : '',
      width            : 0,
      height           : 0,
      techCornersWidth : 1,
      techCornersHeight: 1,
      printMaterial    : ''
    },
    isTechCornersConst: false,
    modalOpened       : false,
    modalLoading      : false,
    allMaterials      : []
  };

  materials = [];

  async componentDidMount() {
    await this.modalLoading(true);
    await this.loadContext();
    await this.modalLoading(false);
  }

  async loadContext() {
    await this.reloadOrder();
    await this.setState({ sizeUnit: (orderTypes[this.state.order.type].isTechCornersConst) });
    await this.loadAllMaterials();
  }

  async reloadOrder() {
    try {
      let { data }             = await axios({
                                               method: 'get',
                                               url   : endpoints.api.manager.orders.getById,
                                               params: {
                                                 id: this.props.order.id
                                               },
                                               auth  : this.props.utils.user
                                             });
      //noinspection JSUnresolvedVariable
      data.printMaterial       = data.printMaterial.id;
      [data.width]             =
          UnitConverter.convertValue(data.width).withUnit('m').toUnit(orderTypes[data.type].unit);
      [data.height]            =
          UnitConverter.convertValue(data.height).withUnit('m').toUnit(orderTypes[data.type].unit);
      [data.techCornersWidth]  =
          UnitConverter.convertValue(data.techCornersWidth).withUnit('m').toUnit('cm');
      [data.techCornersHeight] =
          UnitConverter.convertValue(data.techCornersHeight).withUnit('m').toUnit('cm');

      await this.setState({ order: data });
    } catch(e) {
      console.error(e);
      this.props.utils.toast({
                               visible: true,
                               header : 'Order Manager',
                               body   : 'Внутреняя ошибка',
                               color  : 'red',
                               delay  : 3000
                             });
    }
  }

  async loadAllMaterials() {
    try {

      const orderType = this.state.order.type;
      let { data }    = await axios({
                                      method: 'GET',
                                      url   : endpoints.api.digital_print.storage.items.getAllWithOrderType,
                                      auth  : this.props.utils.user,
                                      params: { orderType: orderType }
                                    });

      let allMaterials = _.map(data, mat => {
        this.materials[mat.id] = mat;
        return {
          key: mat.id, value: mat.id, text: RenderHelper.renderMaterial(mat)
        };
      });

      await this.setState({ allMaterials });
    } catch(e) {
      console.error(e);
      this.props.utils.toast({
                               visible: true,
                               header : "Панель менеджера",
                               body   : "Внутреняя ошибка",
                               color  : "red",
                               delay  : 3000
                             });
    } finally {
      this.props.utils.loading(false);
    }
  }

  async modalOpened(modalOpened) {
    await this.setState({ modalOpened });
  }

  async modalLoading(modalLoading) {
    await this.setState({ modalLoading });
  }

  renderButton() {
    return (<OrderRow {...this.props} onClick={async () => this.modalOpened(!this.props.readOnly)} />);
  }

  async setOrder(name, value) {
    let order = { ...this.state.order, [name]: value };
    await this.setState({ order });
  }

  async addOrder() {
    await this.modalLoading(true);

    try {
      let { order } = _.cloneDeep(this.state);
      let invoiceId = `${order.invoice.id}`;

      console.log(order.preDigitalPrintWorks);

      order.printMaterial       = this.materials[order.printMaterial];
      [order.techCornersHeight] = UnitConverter.convertValue(order.techCornersHeight).withUnit('mm').toUnit('m');
      [order.techCornersWidth]  = UnitConverter.convertValue(order.techCornersWidth).withUnit('mm').toUnit('m');
      [order.width]             =
          UnitConverter.convertValue(order.width).withUnit(orderTypes[this.state.order.type].unit).toUnit('m');

      [order.height] =
          UnitConverter.convertValue(order.height).withUnit(orderTypes[this.state.order.type].unit).toUnit('m');
      await axios({
                    method : 'post',
                    auth   : this.props.utils.user,
                    data   : order.id,
                    headers: {
                      "Content-Type": "application/json",
                    },
                    url    : endpoints.api.manager.orders.deleteById
                  });

      delete order.id;
      delete order.invoice;

      console.dir(order);

      await axios({
                    method: 'post',
                    url   : endpoints.api.manager.invoices.addDigitalPrintOrder,
                    auth  : this.props.utils.user,
                    data  : { order, invoiceId }
                  });

      await this.props.utils.toast({
                                     visible: true,
                                     header : 'Добавление заказа цифровой печати',
                                     body   : 'заказ добавлен!',
                                     color  : 'green',
                                     delay  : 3000
                                   });
      await this.props.onChange();
      await this.modalOpened(false);
    } catch(e) {
      console.error(e);
      this.props.utils.toast({
                               visible: true,
                               header : 'Добавление заказа Цифровой печати',
                               body   : 'Заказ не был добавлен!',
                               color  : 'red',
                               delay  : 3000
                             });
    } finally {
      await this.modalLoading(false);
    }
  }

  isTechCornersConst() {
    return (orderTypes[this.state.order.type].isTechCornersConst);
  }

  render() {
    return <Modal onClose={async () => await this.modalOpened(false)}
                  trigger={this.renderButton()}
                  style={{ marginTop: 0 }}
                  open={this.state.modalOpened}>
      <ModalHeader>
        {RenderHelper.renderOrderType(this.state.order.type)}
      </ModalHeader>
      <ModalContent scrolling>
        <Form>
          <Grid>
            <GridRow columns={2}>
              <GridColumn>
                <FormField>
                  <label>Путь к файлу с дизайном</label>
                  <Input type={'text'}
                         search selection
                         value={this.state.order.design}
                         onChange={async (e, { value }) => await this.setOrder('design', value)} />
                </FormField>
              </GridColumn>
              <GridColumn>
                <FormField>
                  <label>Количество</label>
                  <Input type={'text'}
                         value={this.state.order.count}
                         onChange={async (e, { value }) => await this.setOrder('count', value)} />
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow columns={2}>
              <GridColumn>
                <Grid>
                  <GridRow columns={2}>
                    <GridColumn>
                      <FormField>
                        <label>Размер ({UnitConverter.renderUnit(orderTypes[this.state.order.type].unit)})</label>
                        <Input placeholder={'Width'}
                               value={this.state.order.width}
                               onChange={async (e, { value }) => await this.setOrder('width', value)} />
                      </FormField>
                    </GridColumn>
                    <GridColumn>
                      <FormField>
                        <label>&nbsp;</label>
                        <Input placeholder={'Height'}
                               value={this.state.order.height}
                               onChange={async (e, { value }) => await this.setOrder('height', value)} />
                      </FormField>
                    </GridColumn>
                  </GridRow>
                </Grid>
              </GridColumn>
              <GridColumn>
                <Grid>
                  <GridRow columns={2}>
                    <GridColumn>
                      <FormField>
                        <label>Тех края ({UnitConverter.renderUnit('mm')})</label>
                        <Input placeholder={'Width'}
                               value={this.state.order.techCornersWidth}
                               readOnly={this.isTechCornersConst()}
                               onChange={async (e, { value }) => await
                                   this.setOrder('techCornersWidth', value)}
                        />
                      </FormField>
                    </GridColumn>
                    <GridColumn>
                      <FormField>
                        <label>&nbsp;</label>
                        <Input placeholder={'Height'}
                               readOnly={this.isTechCornersConst()}
                               value={this.state.order.techCornersHeight}
                               onChange={async (e, { value }) => await
                                   this.setOrder('techCornersHeight', value)}
                        />
                      </FormField>
                    </GridColumn>
                  </GridRow>
                </Grid>
              </GridColumn>
            </GridRow>
            <GridRow>
              <GridColumn>
                <FormField>
                  <label>Материалы</label>
                  <Dropdown type={'text'}
                            fluid search selection
                            value={this.state.order.printMaterial}
                            options={this.state.allMaterials}
                            onChange={async (e, { value }) => await this.setOrder('printMaterial', value)} />
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow>
              <GridColumn>
                <FormField>
                  <label>Comment</label>
                  <TextArea value={this.state.order.comments}
                            autoHeight
                            rows={5}
                            onChange={async (e, { value }) => await this.setOrder('comments', value)} />
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow>
              <GridColumn>
                <PrintWorks order={this.state.order} utils={this.props.utils} onChange={async (value) => {
                  await this.setState({ order: { ...this.state.order, ...value } })
                }} />
              </GridColumn>
            </GridRow>
          </Grid>
        </Form>
      </ModalContent>
      <ModalActions>
        <DeleteOrder utils={this.props.utils} id={this.state.order.id} floated={'left'} onDeleted={async () => {
          await this.props.onChange();
          await this.modalOpened(false);
        }} />
        <Button
            color={'red'}
            onClick={async () => await this.modalOpened(false)
            }
            content={'Отмена'}
        />
        <Button color={'green'}
                loading={this.state.modalLoading}
                onClick={async () => await this.addOrder()}
                icon={'circle arrow down'}
                labelPosition={'right'}
                content={'Сохранить'} />
      </ModalActions>
    </Modal>;
  }

}