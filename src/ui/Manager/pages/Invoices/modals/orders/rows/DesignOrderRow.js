import _ from 'lodash';
import React, { Component } from 'react';
import RenderHelper from '../../../../../../../utils/RenderHelper';
import {
  Button,
  Dropdown,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Input,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader
} from 'semantic-ui-react';
import * as axios from 'axios';
import endpoints from '../../../../../../../config/endpoints';
import OrderRow from '../OrderRow';
import DeleteOrder from '../../modals/DeleteOrder';

export default class DesignOrderRow extends Component {
  state = {
    order       : {
      type    : 'DESIGN',
      designer: null,
      cost    : ''
    },
    modalOpened : false,
    modalLoading: false,
    allDesigners: []
  };

  designers = [];

  async componentDidMount() {
    await this.loadContext();
  }

  async loadContext() {
    await this.modalLoading(true);
    await this.reloadOrder();
    await this.loadAllDesigners();
    await this.modalLoading(false);
  }

  async modalOpened(modalOpened) {
    await this.setState({ modalOpened });
  }

  async modalLoading(modalLoading) {
    await this.setState({ modalLoading });
  }

  renderButton() {
    return (<OrderRow {...this.props} onClick={async () => this.modalOpened(!this.props.readOnly)} />);
  }

  async setOrder(name, value) {
    let order = { ...this.state.order, [name]: value };
    await this.setState({ order });
  }

  async addOrder() {
    let { order } = this.state;

    order.designer = this.designers[order.designer];

    try {
      await this.modalLoading(true);
      let invoiceId = this.props.order.invoice.id;

      await axios({
                    method : 'post',
                    url    : endpoints.api.manager.orders.deleteById,
                    auth   : this.props.utils.user,
                    data   : order.id,
                    headers: { 'Content-Type': 'application/json' }
                  });

      await axios({
                    method: 'post',
                    url   : endpoints.api.manager.invoices.addDesignOrder,
                    auth  : this.props.utils.user,
                    data  : { order, invoiceId }
                  });

      await this.props.utils.toast({
                                     visible: true,
                                     header : 'Modify design order',
                                     body   : 'заказ добавлен!',
                                     color  : 'green',
                                     delay  : 3000
                                   });
      await this.props.onChange();
      await this.modalOpened(false);
    } catch(e) {
      console.error(e);
      this.props.utils.toast({
                               visible: true,
                               header : 'Добавление заказа дизайна',
                               body   : 'не удалось создать заказ!',
                               color  : 'red',
                               delay  : 3000
                             });
    } finally {
      await this.modalLoading(false);
    }
  }

  async loadAllDesigners() {
    try {
      let { data } = await axios({
                                   method: 'get',
                                   url   : endpoints.api.users.getAllWithRole,
                                   params: { roleName: 'ROLE_DESIGNER' },
                                   auth  : this.props.utils.user
                                 });

      let allDesigners = _.map(data, e => {
        this.designers[e.id] = e;
        return {
          key : e.id, value: e.id,
          text: RenderHelper.withUtils(this.props.utils).renderUserText(`${e.firstName} ${e.lastName}`)
        };
      });

      await this.setState({ allDesigners });
    } catch(e) {

    }
  }

  render() {
    return <Modal onClose={async () => await this.modalOpened(false)}
                  trigger={this.renderButton()}
                  open={this.state.modalOpened}>
      <ModalHeader>
        {RenderHelper.renderOrderType('DESIGN')}
      </ModalHeader>
      <ModalContent>
        <Form>
          <Grid>
            <GridRow columns={1}>
              <GridColumn>
                <FormField>
                  <label>ID</label>
                  <Input value={this.state.order.id} type={'text'} />
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow columns={1}>
              <GridColumn>
                <FormField>
                  <label>Имя Дезайнера</label>
                  <Dropdown type={'text'}
                            search selection
                            value={this.state.order.designer}
                            options={this.state.allDesigners}
                            onChange={async (e, { value }) => await this.setOrder('designer', value)} />
                </FormField>
              </GridColumn>
            </GridRow>
          </Grid>
        </Form>
      </ModalContent>
      <ModalActions>
        <DeleteOrder utils={this.props.utils} id={this.state.order.id} floated={'left'} onDeleted={async () => {
          await this.props.onChange();
          await this.modalOpened(false);
        }} />
        <Button color={'red'} onClick={async () => await this.modalOpened(false)} content={'Отмена'} />
        <Button color={'green'}
                loading={this.state.modalLoading}
                onClick={async () => await this.addOrder()}
                icon={'circle arrow down'}
                labelPosition={'right'}
                content={'Сохранить'} />
      </ModalActions>
    </Modal>;
  }

  async reloadOrder() {
    try {
      let { data } = await axios({
                                   method: 'get',
                                   url   : endpoints.api.manager.orders.getById,
                                   params: {
                                     id: this.props.order.id
                                   },
                                   auth  : this.props.utils.user
                                 });
      console.dir(data);
      data.designer = (data.designer || { id: null }).id;
      await this.setState({ order: data });
    } catch(e) {
      console.error(e);
      this.props.utils.toast({
                               visible: true,
                               header : 'Панель менеджера',
                               body   : 'Внутреняя ошибка',
                               color  : 'red',
                               delay  : 3000
                             });
    }
  }
}