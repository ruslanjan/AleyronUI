import React, { Component, Fragment } from 'react';
import { TableCell, TableRow } from 'semantic-ui-react';
import RenderHelper from '../../../../../../utils/RenderHelper';
import RectangleSizeDisplayer from '../../../../../../utils/elements/RectangleSizeDisplayer';

export default class OrderRow extends Component {
  render = () => (<TableRow key={this.props.order.id} onClick={async () => await this.onClickFn()()}>
    <TableCell>
      {this.props.order.id}
    </TableCell>
    <TableCell>
      {RenderHelper.renderOrderType(this.props.order.type)}
    </TableCell>
    <TableCell>
      {this.props.order.designer ?
          RenderHelper.withUtils(this.props.utils).renderUser(this.props.order.designer.username) :
          <Fragment />}
    </TableCell>
    <TableCell>
      {this.props.order.count}
    </TableCell>
    <TableCell>
      {this.props.order.width && this.props.order.height ?
          <RectangleSizeDisplayer width={this.props.order.width} height={this.props.order.height} unit={'m'} /> :
          <Fragment>недействительный</Fragment>
      }
    </TableCell>
    <TableCell>
      {RenderHelper.renderCost(this.props.order.cost)}
    </TableCell>
  </TableRow>);

  onClickFn() {
    if(this.props.disabled) return () => {};
    return (this.props.onClick) || (() => {});
  }
}