import _ from 'lodash';
import React, { Component } from 'react';
import RenderHelper from '../../../../../../../utils/RenderHelper';
import {
  Button,
  Dropdown, DropdownItem,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader
} from 'semantic-ui-react';
import * as axios from 'axios';
import endpoints from '../../../../../../../config/endpoints';

export default class AddDesignOrder extends Component {
  state = {
    order       : {
      type    : 'DESIGN',
      designer: null,
      cost    : ''
    },
    modalOpened : false,
    modalLoading: false,
    allDesigners: []
  };

  designers = [];

  async componentDidMount() {
    await this.loadContext();
  }

  async loadContext() {
    await this.modalLoading(true);
    await this.loadAllDesigners();
    await this.modalLoading(false);
  }

  async modalOpened(modalOpened) {
    await this.setState({ modalOpened });
  }

  async modalLoading(modalLoading) {
    await this.setState({ modalLoading });
  }

  renderButton() {
    return (<DropdownItem onClick={async () => await this.modalOpened(true)}>{RenderHelper.renderOrderType('DESIGN')}</DropdownItem>);
  }

  async setOrder(name, value) {
    let order = { ...this.state.order, [name]: value };
    await this.setState({ order });
  }

  async addOrder() {
    let { order } = this.state;

    order.designer = this.designers[order.designer];

    try {
      await this.modalLoading(true);
      let { invoiceId } = this.props;
      await axios({
                    method: 'post',
                    url   : endpoints.api.manager.invoices.addDesignOrder,
                    auth  : this.props.utils.user,
                    data  : { order, invoiceId }
                  });

      await this.props.utils.toast({
                                     visible: true,
                                     header : 'Добавление заказа дизайна',
                                     body   : 'заказ добавлен!',
                                     color  : 'green',
                                     delay  : 3000
                                   });
      await this.props.onChange();
      await this.modalOpened(false);
    } catch(e) {
      console.error(e);
      this.props.utils.toast({
                               visible: true,
                               header : 'Добавление заказа дизайна',
                               body   : 'заказ не был добавлен!',
                               color  : 'red',
                               delay  : 3000
                             });
    } finally {
      await this.modalLoading(false);
    }
  }

  async loadAllDesigners() {
    try {
      let { data } = await axios({
                                   method: 'get',
                                   url   : endpoints.api.users.getAllWithRole,
                                   params: { roleName: 'ROLE_DESIGNER' },
                                   auth  : this.props.utils.user
                                 });

      let allDesigners = _.map(data, e => {
        this.designers[e.id] = e;
        return {
          key : e.id, value: e.id,
          text: RenderHelper.withUtils(this.props.utils).renderUserText(`${e.firstName} ${e.lastName}`)
        };
      });

      await this.setState({ allDesigners });
    } catch(e) {

    }
  }

  render() {
    return <Modal onClose={async () => await this.modalOpened(false)}
                  trigger={this.renderButton()}
                  open={this.state.modalOpened}>
      <ModalHeader>
        Создание закказа на дизайн
      </ModalHeader>
      <ModalContent>
        <Form>
          <Grid>
            <GridRow columns={1}>
              <GridColumn>
                <FormField>
                  <label>Дизайнер</label>
                  <Dropdown type={'text'}
                            search selection
                            value={this.state.order.designer}
                            options={this.state.allDesigners}
                            onChange={async (e, { value }) => await this.setOrder('designer', value)} />
                </FormField>
              </GridColumn>
              {/*<GridColumn>*/}
              {/*<FormField>*/}
              {/*<label>Cost</label>*/}
              {/*<Input type={'text'}*/}
              {/*value={this.state.order.cost}*/}
              {/*onChange={async e => await this.setOrder('cost', e.target.value)}*/}
              {/*label={RenderHelper.renderCost(null)} />*/}
              {/*</FormField>*/}
              {/*</GridColumn>*/}
            </GridRow>
          </Grid>
        </Form>
      </ModalContent>
      <ModalActions>
        <Button color={'red'} onClick={async () => await this.modalOpened(false)} content={'отмена'} />
        <Button color={'green'}
                loading={this.state.modalLoading}
                onClick={async () => await this.addOrder()}
                icon={'circle arrow down'}
                labelPosition={'right'}
                content={'Создать'} />
      </ModalActions>
    </Modal>;
  }

}