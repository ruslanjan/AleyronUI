import _ from 'lodash';
import React, { Component } from 'react';
import RenderHelper from '../../../../../../../utils/RenderHelper';
import {
  Button,
  Dropdown,
  DropdownItem,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Input,
  Message,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader,
  TextArea
} from 'semantic-ui-react';
import * as axios from 'axios';
import endpoints from '../../../../../../../config/endpoints';
import UnitConverter from '../../../../../../../utils/UnitConverter';

const orderTypes = {
  STICKER          : {
    unit              : 'mm',
    isTechCornersConst: true
  },
  WIDE_FORMAT_PRINT: {
    unit              : 'm',
    isTechCornersConst: false
  },
  UNKNOWN          : {
    unit              : 'm',
    isTechCornersConst: true
  }
};

export default class AddDigitalPrintOrder extends Component {
  state = {
    order       : {
      type             : 'UNKNOWN',
      design           : '',
      cost             : '',
      width            : 0,
      height           : 0,
      techCornersWidth : 1,
      techCornersHeight: 1,
      printMaterial    : null
    },
    modalOpened : false,
    modalLoading: false,
    allMaterials: []
  };

  materials = [];

  async componentDidMount() {
    await this.modalLoading(true);
    await this.loadContext();
    await this.modalLoading(false);
  }

  async loadContext() {
    await this.setOrder('type', this.props.type);
    await this.setState({ sizeUnit: (orderTypes[this.state.order.type].isTechCornersConst) });
    await this.loadAllMaterials();
  }

  isTechCornersConst() {
    return (orderTypes[this.state.order.type].isTechCornersConst);
  }

  async loadAllMaterials() {
    try {

      const orderType = this.state.order.type;
      let { data }    = await axios({
                                      method: 'GET',
                                      url   : endpoints.api.digital_print.storage.items.getAllWithOrderType,
                                      auth  : this.props.utils.user,
                                      params: { orderType: orderType }
                                    });

      let allMaterials = _.map(data, mat => {
        this.materials[mat.id] = mat;
        return {
          key: mat.id, value: mat.id, text: RenderHelper.renderMaterial(mat)
        };
      });

      await this.setState({ allMaterials });
    } catch(e) {
      console.error(e);
      this.props.utils.toast({
                               visible: true,
                               header : "Панель менеджера",
                               body   : "Внутреняя ошибка",
                               color  : "red",
                               delay  : 3000
                             });
    } finally {
      this.props.utils.loading(false);
    }
  }

  async modalOpened(modalOpened) {
    await this.setState({ modalOpened });
  }

  async modalLoading(modalLoading) {
    await this.setState({ modalLoading });
  }

  renderButton() {
    return (<DropdownItem onClick={async () => await this.modalOpened(true)}>{RenderHelper.renderOrderType(
        this.state.order.type)}</DropdownItem>);
  }

  async setOrder(name, value) {
    let order = { ...this.state.order, [name]: value };
    await this.setState({ order });
  }

  async addOrder() {
    let { order } = _.cloneDeep(this.state);

    order.printMaterial       = this.materials[order.printMaterial];
    [order.techCornersHeight] = UnitConverter.convertValue(order.techCornersHeight).withUnit('mm').toUnit('m');
    [order.techCornersWidth]  = UnitConverter.convertValue(order.techCornersWidth).withUnit('mm').toUnit('m');
    [order.width]             =
        UnitConverter.convertValue(order.width).withUnit(orderTypes[this.state.order.type].unit).toUnit('m');
    [order.height]            =
        UnitConverter.convertValue(order.height).withUnit(orderTypes[this.state.order.type].unit).toUnit('m');

    try {
      await this.modalLoading(true);
      let { invoiceId } = this.props;
      await axios({
                    method: 'post',
                    url   : endpoints.api.manager.invoices.addDigitalPrintOrder,
                    auth  : this.props.utils.user,
                    data  : { order, invoiceId }
                  });

      await this.props.utils.toast({
                                     visible: true,
                                     header : 'Добавление заказа дизайна',
                                     body   : 'заказ добавлен!',
                                     color  : 'green',
                                     delay  : 3000
                                   });
      await this.props.onChange();
      await this.modalOpened(false);
    } catch(e) {
      console.error(e);
      this.props.utils.toast({
                               visible: true,
                               header : 'Добавление заказа дизайна',
                               body   : 'заказ не был добавлен!',
                               color  : 'red',
                               delay  : 3000
                             });
    } finally {
      await this.modalLoading(false);
    }
  }

  render() {
    return <Modal onClose={async () => await this.modalOpened(false)}
                  trigger={this.renderButton()}
                  style={{ marginTop: 0 }}
                  open={this.state.modalOpened}>
      <ModalHeader>
        {RenderHelper.renderOrderType(this.state.order.type)}
      </ModalHeader>
      <ModalContent scrolling>
        <Form>
          <Grid>
            <GridRow columns={2}>
              <GridColumn>
                <FormField>
                  <label>Путь к файлу с дизайном</label>
                  <Input type={'text'}
                         search selection
                         value={this.state.order.design}
                         onChange={async (e, { value }) => await this.setOrder('design', value)} />
                </FormField>
              </GridColumn>
              <GridColumn>
                <FormField>
                  <label>Количество</label>
                  <Input type={'text'}
                         value={this.state.order.count}
                         onChange={async (e, { value }) => await this.setOrder('count', value)} />
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow columns={2}>
              <GridColumn>
                <Grid>
                  <GridRow columns={2}>
                    <GridColumn>
                      <FormField>
                        <label>Размер ({UnitConverter.renderUnit(orderTypes[this.state.order.type].unit)})</label>
                        <Input placeholder={'Width'}
                               value={this.state.order.width}
                               onChange={async (e, { value }) => await this.setOrder('width', value)} />
                      </FormField>
                    </GridColumn>
                    <GridColumn>
                      <FormField>
                        <label>&nbsp;</label>
                        <Input placeholder={'Высота'}
                               value={this.state.order.height}
                               onChange={async (e, { value }) => await this.setOrder('height', value)} />
                      </FormField>
                    </GridColumn>
                  </GridRow>
                </Grid>
              </GridColumn>
              <GridColumn>
                <Grid>
                  <GridRow columns={2}>
                    <GridColumn>
                      <FormField>
                        <label>Тех. края ({UnitConverter.renderUnit('mm')})</label>
                        <Input placeholder={'Width'}
                               value={this.state.order.techCornersWidth}
                               readOnly={this.isTechCornersConst()}
                               onChange={async (e, { value }) => await
                                   this.setOrder('techCornersWidth', value)}
                        />
                      </FormField>
                    </GridColumn>
                    <GridColumn>
                      <FormField>
                        <label>&nbsp;</label>
                        <Input placeholder={'Height'}
                               readOnly={this.isTechCornersConst()}
                               value={this.state.order.techCornersHeight}
                               onChange={async (e, { value }) => await
                                   this.setOrder('techCornersHeight', value)}
                        />
                      </FormField>
                    </GridColumn>
                  </GridRow>
                </Grid>
              </GridColumn>
            </GridRow>
            <GridRow>
              <GridColumn>
                <FormField>
                  <label>Материалы</label>
                  <Dropdown type={'text'}
                            fluid search selection
                            value={this.state.order.printMaterial}
                            options={this.state.allMaterials}
                            onChange={async (e, { value }) => await this.setOrder('printMaterial', value)} />
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow>
              <GridColumn>
                <FormField>
                  <label>Comment</label>
                  <TextArea value={this.state.order.comments}
                            autoHeight
                            rows={5}
                            onChange={async (e, { value }) => await this.setOrder('comments', value)} />
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow>
              <GridColumn>
                <Message floating positive>
                  <Message.Header>Вы сможете добавить печатные работы только после создания заказа.</Message.Header>
                </Message>
              </GridColumn>
            </GridRow>
          </Grid>
        </Form>
      </ModalContent>
      <ModalActions>
        <Button
            color={'red'}
            onClick={async () => await this.modalOpened(false)
            }
            content={'Отмена'}
        />
        <Button color={'green'}
                loading={this.state.modalLoading}
                onClick={async () => await this.addOrder()}
                icon={'circle arrow down'}
                labelPosition={'right'}
                content={'Сохранить'} />
      </ModalActions>
    </Modal>;
  }

}