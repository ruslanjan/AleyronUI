import Frac from 'fractions';
import React, { Component, Fragment } from 'react';
import { Dropdown, FormField, Input, Label } from 'semantic-ui-react';

const blockSizeTypes = [
  { key: 'SINGLE', value: 'SINGLE', text: 'Одно-на-несколько' },
  { key: 'MULTIPLE', value: 'MULTIPLE', text: 'Несколько-на-один' },
  { key: 'ADVANCED', value: 'ADVANCED', text: 'Продвинутый' }
];

export default class BlockSize extends Component {
  state               = {
    blockSizeType: 'SINGLE',
    blockSize    : {
      dividend: 1,
      divisor : 1
    }
  };
  render              = () =>
      <FormField>
        <label>Тип расчета (выбрать из списка): <Dropdown options={blockSizeTypes}
                         defaultValue={'SINGLE'}
                         inline
                         onChange={async (e, { value }) => {
                           console.log(value);
                           await this.setState({ blockSizeType: value })
                         }} />
        </label>
        {this.renderBlockSizeEdit()}
      </FormField>;
  renderBlockSizeEdit = () => (this[`renderBSE_${this.state.blockSizeType}`] || this.renderBSE_NONE)();
  renderBSE_NONE      = () => <Fragment/>;
  //noinspection JSUnusedGlobalSymbols
  renderBSE_SINGLE    = () => <Input fluid labelPosition={'right'} type={'number'} value={this.state.blockSize.dividend}
                                     onChange={async (e, { value }) => await this.setBlockSize('dividend',
                                                                                               value)}>
    <Label>Одно действие на</Label>
    <input />
    <Label>листов продукции</Label>
  </Input>;
  //noinspection JSUnusedGlobalSymbols
  renderBSE_MULTIPLE  = () => <Input fluid labelPosition={'right'} type={'number'} value={this.state.blockSize.divisor}
                                     onChange={async (e, { value }) => await this.setBlockSize('divisor',
                                                                                               value)}>
    <input />
    <Label>Действий на каждый листов продукции</Label>
  </Input>;
  //noinspection JSUnusedGlobalSymbols
  renderBSE_ADVANCED  = () => <Input fluid type={'text'} value={this.state.blockSize.string}
                                     onChange={async (e, { value }) => await this.setBlockSize('string', value)} />;

  async componentDidMount() {
    await this.setState({ blockSize: this.props.blockSize });
  }

  async setBlockSize(name, value) {
    let { blockSize } = this.state;

    if(name === 'string') {
      let fr = null;
      try {
        fr = new Frac(value);
      } catch(e) {/*ignore*/}
      if(fr) {
        blockSize.divisor  = fr.denominator;
        blockSize.dividend = fr.numerator;
      }
    }

    blockSize[name] = value;
    await this.setState({ blockSize });
  }
}
