import _ from 'lodash';
import axios from 'axios';
import React, { Component, Fragment } from 'react';
import {
  Button,
  Dropdown,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Input,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader
} from 'semantic-ui-react';
import RenderHelper from '../../../../../../../../utils/RenderHelper';
import endpoints from '../../../../../../../../config/endpoints';
import BlockSize from '../elements/BlockSize';

const printWorkTypes = _.map(['POST', 'PRE'],
                             type => ({ key: type, value: type, text: RenderHelper.renderPrintWorkType(type) }));

export default class AddPrintWork extends Component {

  templates = {};

  state = {
    template             : null,
    modalLoading         : false,
    modalOpened          : false,
    allPrintWorkTemplates: [],
    printWork            : {
      type: 'UNK'
    }
  };

  async componentDidMount() {
    await this.modalLoading(true);
    await this.loadContext();
    await this.modalLoading(false);
  }

  async loadContext() {
    await this.loadTemplates();
  }

  async modalOpened(modalOpened) {
    await this.setState({ modalOpened });
  }

  async modalLoading(modalLoading) {
    await this.setState({ modalLoading });
  }

  renderButton() {
    return (<Button
        {...this.props}
        onClick={async () => await this.modalOpened(true)}
        icon={'plus'}
        labelPosition={'left'}
        content={'Добавить печатную работу'} />);
  }

  render() {
    return <Modal onClose={async () => await this.modalOpened(false)}
                  trigger={this.renderButton()}
                  open={this.state.modalOpened}>
      <ModalHeader>
        Добавить печатную работу
      </ModalHeader>
      <ModalContent>
        <Form>
          <Grid>
            <GridRow columns={2}>
              <GridColumn>
                <FormField>
                  <label>Work type</label>
                  <Dropdown type={'text'}
                            selection
                            options={printWorkTypes}
                            value={this.state.printWork.type}
                            onChange={async (e, { value }) => {
                              (async () => {
                                await this.tplLoading(true);
                                await this.setPrintWork('type', value);
                                await this.updateTemplates();
                                await this.setPrintWork('type', value);
                                await this.tplLoading(false);
                              })();
                            }} />
                </FormField>
              </GridColumn>
              <GridColumn>
                <FormField>
                  <label>Загатовки</label>
                  <Dropdown type={'text'}
                            loading={this.state.tplLoading}
                            search selection
                            value={this.state.template || this.state.allPrintWorkTemplates[0]}
                            options={this.printWorkTemplates()}
                            onChange={async (e, { value }) => await this.setTemplate(value)} />
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow>
              <GridColumn>
                <Grid>
                  <GridRow>
                    {this.state.printWork.blockSize ?
                        <GridColumn>
                          <BlockSize blockSize={this.state.printWork.blockSize}
                                     onChange={async ({ value }) => await this.setPrintWork('blockSize', value)} />
                        </GridColumn>
                        : <Fragment />}
                  </GridRow>
                  <GridRow columns={3}>
                    {this.state.printWork.squareMeterPrice ?
                        <Fragment>
                          <GridColumn>
                            <FormField>
                              <label>Цена за кв. метр</label>
                              <Input type={'text'}
                                     readOnly
                                     value={this.state.printWork.squareMeterPrice} />
                            </FormField>
                          </GridColumn>
                        </Fragment>
                        : <Fragment />}
                    {this.state.printWork.perimeterPrice ?
                        <Fragment>
                          <GridColumn>
                            <FormField>
                              <label>Цена за периметр</label>
                              <Input type={'text'}
                                     readOnly
                                     value={this.state.printWork.perimeterPrice} />
                            </FormField>
                          </GridColumn>
                        </Fragment>
                        : <Fragment />}
                    {this.state.printWork.oneTimePrice ?
                        <Fragment>
                          <GridColumn>
                            <FormField>
                              <label>Разовая Цена</label>
                              <Input type={'text'}
                                     readOnly
                                     value={this.state.printWork.oneTimePrice} />
                            </FormField>
                          </GridColumn>
                        </Fragment>
                        : <Fragment />}
                  </GridRow>
                </Grid>
              </GridColumn>
            </GridRow>
          </Grid>
        </Form>
      </ModalContent>
      <ModalActions>
        <Button color={'red'} onClick={async () => await this.modalOpened(false)} content={'Отмена'} />
        <Button color={'green'}
                loading={this.state.modalLoading}
                onClick={async () => await this.changeValue()}
                icon={'circle arrow down'}
                labelPosition={'right'}
                content={'Сохранить'} />
      </ModalActions>
    </Modal>;
  };

  async updateTemplates() {
    await this.loadTemplates();
    if((this.templates[this.state.template] || {}).type === this.state.printWork.type) {
      console.log((this.templates[this.state.template] || {}).type, this.state.printWork.type);
      await this.setTemplate(this.state.template);
    } else {
      await this.setTemplate(null);
    }
  }

  async changeValue() {
    let { printWork } = _.cloneDeep(this.state);

    //
    await this.modalOpened(false);
    return await this.props.onChange(printWork);
  }

  setPrintWork(name, value) {
    let { printWork } = this.state;
    printWork[name]   = value;
    this.setState({ printWork });
  }

  printWorkTemplates() {
    if((this.state.printWork.type) === 'PRE' || this.state.printWork.type === 'POST') {
      return _.filter(this.state.allPrintWorkTemplates, ({ key }) => {
        let { type } = this.templates[key];
        return (type === this.state.printWork.type);
      });
    } else {
      return this.state.allPrintWorkTemplates;
    }
  }

  async loadTemplates() {
    try {
      let { data } = await axios({
                                   method: 'get',
                                   url   : endpoints.api.order.print_work.getAll,
                                   auth  : this.props.utils.user
                                 });

      data = _.map(data, (e) => {
        this.templates[e.id] = e;
        return { key: e.id, value: e.id, text: e.name };
      });

      await this.setState({ allPrintWorkTemplates: data });
    } catch(e) {
      console.error(e);
      await this.props.utils.toast.error("Панель менеджера", "Внутреняя ошибка!");
    }
  }

  async setTemplate(value) {
    await this.setState({ template: value });
    await this.setState({ printWork: (value ? this.templates[value] || {} : {}) });
  }

  async tplLoading(tplLoading) {
    await this.setState({ tplLoading });
  }
}