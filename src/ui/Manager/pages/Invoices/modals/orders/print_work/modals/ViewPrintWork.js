import _ from 'lodash';
import React, { Component, Fragment } from 'react';
import {
  Button,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader,
  Table,
  TableCell,
  TableHeader,
  TableRow
} from 'semantic-ui-react';
import RenderHelper from '../../../../../../../../utils/RenderHelper';

export default class ViewPrintWork extends Component {
  renderTable = () => <Table celled structured definition>
    <TableHeader>
      <TableRow>
        <TableCell colSpan={2}>
          ID
        </TableCell>
        <TableCell>
          {this.props.printWork.id}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell colSpan={2}>
          Название
        </TableCell>
        <TableCell>
          {this.props.printWork.name}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell colSpan={2}>
          Тип
        </TableCell>
        <TableCell>
          {RenderHelper.renderPrintWorkType(this.props.printWork.type)}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell rowSpan={3}>
          Цена за блок
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          Размер блока
        </TableCell>
        <TableCell>
          {RenderHelper.renderBlockSize(this.props.printWork.blockSize)}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          Цена за блок
        </TableCell>
        <TableCell>
          {RenderHelper.renderCost(this.props.printWork.blockPrice)}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell colSpan={2}>
          Цена за периметр (за m)
        </TableCell>
        <TableCell>
          {RenderHelper.renderCost(this.props.printWork.perimeterPrice)}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell colSpan={2}>
          Цена за площадь (за m<sup>2</sup>)
        </TableCell>
        <TableCell>
          {RenderHelper.renderCost(this.props.printWork.perimeterPrice)}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell colSpan={2}>
          Разовая цена (за печатную работу)
        </TableCell>
        <TableCell>
          {RenderHelper.renderCost(this.props.printWork.oneTimePrice)}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell rowSpan={5}>
          Материалы
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          {/*желание димы и русса*/}
          Материалы на блок
        </TableCell>
        <TableCell>
          {_.map(this.props.printWork.blockMaterials, mat => RenderHelper.renderMaterialLabel(mat.item))}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          Материалы на квадратный метр
        </TableCell>
        <TableCell>
          {_.map(this.props.printWork.squareMeterMaterials, mat => RenderHelper.renderMaterialLabel(mat.item))}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          Материалы на периметр
        </TableCell>
        <TableCell>
          {_.map(this.props.printWork.perimeterMaterials,
                 mat => <Fragment key={mat.id}>RenderHelper.renderMaterialLabel(mat.item)</Fragment>)}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          Материалы на одинраз
        </TableCell>
        <TableCell>
          {_.map(this.props.printWork.oneTimeMaterials,
                 mat => <Fragment key={mat.id}>RenderHelper.renderMaterialLabel(mat.item)</Fragment>)}
        </TableCell>
      </TableRow>
    </TableHeader>
  </Table>;

  render = () => <Modal onClose={async () => await this.modalOpened(false)}
                        trigger={this.renderButton()}
                        style={{ marginTop: 0 }}
                        open={this.state.modalOpened}>
    <ModalHeader>
      Просмотр печатной работы
    </ModalHeader>
    <ModalContent scrolling>
      {this.renderTable()}
    </ModalContent>
    <ModalActions>
      <Button color={'red'} inverted onClick={async () => await this.modalOpened(false)} content={'Отмена'} />
    </ModalActions>
  </Modal>;

  renderButton = () => <Button color={'violet'} icon={'eye'} onClick={async () => await this.modalOpened(true)} />;

  state = {
    modalOpened: false
  };

  async modalOpened(modalOpened) {
    await this.setState({ modalOpened });
  }
}