import _ from 'lodash';
import React, { Component } from 'react';
import {
  Button,
  Dimmer,
  DimmerDimmable,
  Loader,
  Popup,
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableHeaderCell,
  TableRow
} from 'semantic-ui-react';
import RenderHelper from '../../../../../../../utils/RenderHelper';
import endpoints from '../../../../../../../config/endpoints';
import * as axios from 'axios';
import AddPrintWork from './modals/AddPrintWork';
import ViewPrintWork from './modals/ViewPrintWork';

const GlobalUIDGenerator = new class $ {
  counter = 0;

  static __prefix() {
    return Math.round(Math.random() * 1e7).toString();
  }

  id() {
    return $.__prefix() + (this.counter++);
  }
}();

export default class PrintWorks extends Component {
  state = {
    order: {
      preDigitalPrintWorks : [],
      postDigitalPrintWorks: [],
    }
  };

  get allPrintWorks() {
    return this.state.order.postDigitalPrintWorks.concat(this.state.order.preDigitalPrintWorks);
  }

  async loading(loading) {
    await this.setState({ loading });
  }

  async componentDidMount() {
    await this.loading(true);
    await this.loadContext();
    await this.loading(false);
  }

  async loadContext() {
    await this.reloadOrder();
  }

  async reloadOrder() {
    try {
      let { data } = await axios({
                                   method: 'get',
                                   url   : endpoints.api.manager.orders.getById,
                                   params: {
                                     id: this.props.order.id
                                   },
                                   auth  : this.props.utils.user
                                 });

      data.preDigitalPrintWorks  =
          _.map(data.preDigitalPrintWorks, work => ({ ...work, id: GlobalUIDGenerator.id() }));
      data.postDigitalPrintWorks =
          _.map(data.postDigitalPrintWorks, work => ({ ...work, id: GlobalUIDGenerator.id() }));

      await this.setState({ order: data });
    } catch(e) {
      console.error(e);
      await this.props.utils.toast.error("Панель менеджера", "Внутреняя ошибка!");
    }
  }

  render() {
    return (
        <DimmerDimmable blurring dimmed={this.state.loading}>
          <Dimmer inverted active={this.state.loading}>
            <Loader />
          </Dimmer>
          <Table celled>
            <TableHeader>
              <TableRow>
                <TableHeaderCell colSpan={5}>
                  Печатная работа
                  <AddPrintWork floated={'right'}
                                utils={this.props.utils}
                                order={this.state.order}
                                onChange={async (newWork) => await this.addWork(await this.loadPricedWork(newWork))} />
                </TableHeaderCell>
              </TableRow>
              <TableRow>
                <TableHeaderCell>Название</TableHeaderCell>
                <TableHeaderCell>Тип</TableHeaderCell>
                <TableHeaderCell>Цена</TableHeaderCell>
                <TableHeaderCell>Действия</TableHeaderCell>
              </TableRow>
            </TableHeader>
            <TableBody>
              {_.map(this.allPrintWorks,
                     (printWork) => <TableRow key={Math.ceil(Math.random() * 1000000) + printWork.name}>
                       <TableCell>{printWork.name}</TableCell>
                       <TableCell>{RenderHelper.renderPrintWorkType(printWork.type)}</TableCell>
                       <TableCell>{RenderHelper.renderCost(printWork.cost || 0)}</TableCell>
                       <TableCell>
                         <ViewPrintWork printWork={printWork} />
                         <Popup trigger={<Button negative icon={'trash alternate outline'} />}
                                on={'click'}
                                content={<Button
                                    negative
                                    content={'Удалить'}
                                    onClick={async () => {
                                      await this.deleteWork(printWork)
                                    }} />} />
                       </TableCell>
                     </TableRow>)}
            </TableBody>
          </Table>
        </DimmerDimmable>);
  }

  async loadPricedWork(printWork) {
    let { order } = this.state;

    try {
      let { data }   = await axios({
                                     method : 'post',
                                     url    : endpoints.api.manager.orders.calculatePrintWorkPrice,
                                     data   : {
                                       order: order,
                                       work : printWork
                                     }, auth: this.props.utils.user
                                   });
      printWork.cost = data;
    } catch(e) {
      console.error(e);
      await this.props.utils.toast.error("Панель менеджера", "Внутреняя ошибка");
    }

    return printWork;
  }

  async addWork(work) {
    let { order } = this.state;

    work.id = GlobalUIDGenerator.id();

    if(work.type === 'PRE') {
      order.preDigitalPrintWorks.push(work);
    } else {
      order.postDigitalPrintWorks.push(work);
    }

    await this.setState({ order });

    let { preDigitalPrintWorks, postDigitalPrintWorks } = order;
    await this.props.onChange({ preDigitalPrintWorks, postDigitalPrintWorks });
  }

  async deleteWork(printWork) {
    console.info(printWork);
    let { order } = this.state;
    console.info(order);
    if(printWork.type === 'PRE') {
      _.remove(order.preDigitalPrintWorks, work => (work.id === printWork.id));
    }
    if(printWork.type === 'POST') {
      _.remove(order.postDigitalPrintWorks, work => (work.id === printWork.id));
    }
    console.info(order);

    await this.setState({ order });
  }
}