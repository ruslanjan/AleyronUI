import React, {Component, Fragment} from 'react';
import {
  Button,
  Form,
  FormCheckbox,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Input,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader,
  TextArea
} from 'semantic-ui-react';
import * as axios from "axios";
import endpoints from "../../../../../config/endpoints";

export default class NewInvoice extends Component {
  toast = async (text_color) => {
    await this.props.utils.toast(
      {header: 'Modal Panel', color: text_color.color, body: text_color.text, visible: true});
  };
  onClosed = async () => {
    await this.props.refresh();
  };
  addToInvoice = async (inv) => {
    let {invoice} = this.state;
    if (inv.hasOwnProperty('clientName')) {
      invoice.clientName = inv.clientName;
    }
    if (inv.hasOwnProperty('title')) {
      invoice.title = inv.title;
    }
    if (inv.hasOwnProperty('clientEmail')) {
      invoice.clientEmail = inv.clientEmail;
    }
    if (inv.hasOwnProperty('clientPhoneNumber')) {
      invoice.clientPhoneNumber = inv.clientPhoneNumber;
    }
    if (inv.hasOwnProperty('clientDescription')) {
      invoice.clientDescription = inv.clientDescription;
    }
    await this.setState({invoice: invoice})
  };

  constructor(props) {
    super(props);
    this.state = {
      invoice: {
        clientName: '',
        title: '',
        manager: this.props.utils.user.username,
        invoice_1c: '',
        cost: 0,
        clientEmail: '',
        clientPhoneNumber: '',
        clientDescription: '',
      },
      modalOpened: false,
      modalLoading: false,
      isMail: false,
      isPhone: false,
      isDescription: false,

    };
    this.createInvoice = this.createInvoice.bind(this);
  }

  async createInvoice() {
    let text_color;
    let {clientName, title, manager, invoice_1c, cost, clientEmail, clientPhoneNumber, clientDescription} = this.state.invoice;
    let invoice = {clientName, title, manager, invoice_1c, cost, clientEmail, clientPhoneNumber, clientDescription};

    if (clientName !== '') {
      await this.props.utils.loading(true);
      try {
        await this.modalLoading(true);
        await axios({
          method: "post",
          url: endpoints.api.manager.invoices.create,
          data: invoice,
          auth: this.props.utils.user
        });
        text_color = {text: 'Создание прошло успешно!', color: 'green'};
        await this.modalOpened(false);
      } catch (e) {
        console.error(e);
        text_color = {text: 'Внутреняя ошибка!', color: 'red'};
      } finally {
        await this.onClosed();
        await this.props.utils.loading(false);
        await this.modalLoading(false);
        this.setState({
          invoice: {
            clientName: '',
            title: '',
            manager: this.props.utils.user.username,
            invoice_1c: '',
            cost: 0,
            clientEmail: '',
            clientPhoneNumber: '',
            clientDescription: '',
          },
          modalOpened: false,
          modalLoading: false,
          isMail: false,
          isPhone: false,
          isDescription: false,

        });
      }
    } else {
      text_color = {text: 'Неверные данные', color: 'red'};
    }
    await this.toast(text_color);
  };

  render() {
    let {modalOpened} = this.state;
    return <Modal
      trigger={this.renderButton()}
      open={modalOpened}
      // onClose={() => this.modalOpened(false)}
    >
      <ModalHeader>
        Создание нового счета
      </ModalHeader>
      <ModalContent>
        <Form>
          <Grid container>
            <GridRow columns={2}>
              <GridColumn>
                <FormField>
                  <label>Название</label>
                  <input placeholder='напр. Кофэ'
                         onChange={async (e) => await this.addToInvoice({title: e.target.value})}/>
                </FormField>
              </GridColumn>
              <GridColumn>
                <FormField>
                  <label>Имя Клиента</label>
                  <input placeholder='напр. Котэ'
                         onChange={async (e) => await this.addToInvoice({clientName: e.target.value})}/>
                </FormField>
              </GridColumn>
            </GridRow>
            <GridRow columns={3}>
              <GridColumn>
                <FormCheckbox name={'clientEmail'} label='почта'
                              onChange={(e, {checked}) => this.setState({isMail: checked})}/>
              </GridColumn>
              <GridColumn>
                <FormCheckbox name={'phone'} label='тел. номер'
                              onChange={(e, {checked}) => this.setState({isPhone: checked})}/>
              </GridColumn>
              <GridColumn>
                <FormCheckbox name={'description'} label='Описание'
                              onChange={(e, {checked}) => this.setState({isDescription: checked})}/>
              </GridColumn>
            </GridRow>
            <GridRow columns={2}>

              {this.state.isPhone ?
                <GridColumn>
                  <FormField>
                    <label>Телефон</label>
                    <Input label='+7' placeholder='123 456 78 90'
                           onChange={async (e) => await this.addToInvoice({clientPhoneNumber: e.target.value})}/>
                  </FormField>
                </GridColumn>

                : <Fragment/>
              }
              {this.state.isMail ?
                <GridColumn>
                  <FormField>
                    <label>Почта</label>
                    <Input placeholder='client@gmail.com'
                           onChange={async (e) => await this.addToInvoice({clientEmail: e.target.value})}/>
                  </FormField>
                </GridColumn>
                : <Fragment/>
              }
            </GridRow>

            {this.state.isDescription ?
              <GridRow columns={1}>
                <FormField>
                  <label>Описание</label>
                  <TextArea onChange={async (e) => await this.addToInvoice({clientDescription: e.target.value})}/>
                </FormField>
              </GridRow>
              : <Fragment/>
            }
          </Grid>
        </Form>
      </ModalContent>
      <ModalActions>
        <Button onClick={async () => await this.modalOpened(false)}
                color='red'
                inverted
                content='Отмена'/>
        <Button onClick={async () => await this.createInvoice()}
                color='green'
                content='Сохранить'
                loading={this.state.modalLoading}/>
      </ModalActions>
    </Modal>
  }

  async modalOpened(state) {
    await this.setState({modalOpened: state});
  }

  async modalLoading(state) {
    await this.setState({modalLoading: state});
  }

  renderButton() {
    return <Button disabled={this.props.disabled}
                   icon labelPosition='left'
                   primary size='small'
                   onClick={async () => {
                     await this.props.utils.toast({visible: false});
                     await this.modalOpened(true);
                   }}
    >
      <Icon name='add'/>
      Новый заказ
    </Button>
  }
}