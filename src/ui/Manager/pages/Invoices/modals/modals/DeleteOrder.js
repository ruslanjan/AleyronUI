import React, { Component } from 'react';
import axios from 'axios/index';
import endpoints from '../../../../../../config/endpoints';
import { Button, Icon, Modal, ModalActions, ModalContent, ModalHeader } from 'semantic-ui-react';

export default class DeleteOrder extends Component {

  constructor(props) {

    super(props);

    this.state = {
      id          : this.props.id,
      modalOpened : false,
      modalLoading: false
    }
  }

  async delOrder(id) {
    try {
      await this.modalLoading(true);
      await axios({
                    method : 'post',
                    auth   : this.props.utils.user,
                    data   : id,
                    headers: {
                      "Content-Type": "application/json",
                    },
                    url    : endpoints.api.manager.orders.deleteById
                  });
    } catch(e) {
    } finally {
      //noinspection JSUnresolvedFunction
      await this.props.onDeleted();
      await this.modalLoading(false);
      await this.modalOpened(false);
    }
  };

  render() {
    return (<Modal trigger={this.renderButton()}
                   open={this.state.modalOpened}
                   basic
                   onClose={() => this.modalOpened(false)}>
          <ModalHeader>
            Удаление заказа
          </ModalHeader>
          <ModalContent>
            Вы уверены?
          </ModalContent>
          <ModalActions>
            <Button basic inverted onClick={() => this.modalOpened(false)}>
              <Icon name='remove' /> Нет
            </Button>
            <Button color='red' inverted
                    loading={this.state.modalLoading}
                    onClick={() => this.delOrder(this.state.id)}>
              <Icon name='trash alternate outline' /> Да
            </Button>
          </ModalActions>
        </Modal>
    );
  }

  async modalOpened(state) {
    await this.setState({ modalOpened: state });
  }

  async modalLoading(state) {
    await this.setState({ modalLoading: state });
  }

  renderButton() {
    return (<Button {...this.props} disabled={this.props.disabled} icon={'trash alternate outline'}
                    color={'red'}
                    onClick={() => this.modalOpened(true)} />);
  }
}