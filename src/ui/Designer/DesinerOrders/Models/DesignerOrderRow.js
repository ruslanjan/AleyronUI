import React, { Component, Fragment } from 'react';
import {
  Button,
  Grid,
  GridColumn,
  GridRow,
  Input,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader,
  TableCell,
  TableRow
} from 'semantic-ui-react';

import endpoints from "../../../../config/endpoints";
import axios from "axios/index";

export default class DesignerOrderRow extends Component {

  save = async () => {
    let { designerOrder } = this.state;
    await this.props.utils.loading(true);
    try {
      await axios({
                    method: "post",
                    url   : endpoints.api.order.design_order.update,
                    data  : designerOrder,
                    auth  : this.props.utils.user
                  });
      await this.toast({
                         visible: true,
                         header : 'Панель администратора',
                         color  : 'green',
                         body   : 'Создание пользователя успешно!'
                       });
    } catch(e) {
      console.error(e);
      await this.toast({
                         visible: true,
                         color  : 'red',
                         header : 'Панель Администратора',
                         body   : 'Не удалось создать пользователя!'
                       });
    } finally {
      // await this.loadDataPage();
      await this.props.utils.loading(false);
    }
  };

  toast = async (state) => {
    this.props.utils.toast(state);
  };

  constructor(props) {
    super(props);
    this.state = {
      designerOrder: this.props.designerOrder,
      modalOpened  : false,
      kub          : this.props.designerOrder.price !== "",
    };
  }

  handleChange(event) {
    let { designerOrder } = this.state;
    designerOrder.price   = event;
    this.setState(
        { PriceConfig: designerOrder }
    )
  }

  render() {
    let { modalOpened, designerOrder } = this.state;
    return <Modal
        onClose={() => this.modalOpened(false)}
        style={{ marginTop: 0 }}
        trigger={this.renderButton()}
        open={modalOpened}
    >
      <ModalHeader>
        DesignerOrder
      </ModalHeader>
      {this.state.kud ?
          <ModalContent scrolling>
            <Grid container columns={2}>
              <GridRow>
                <GridColumn>
                  Дизайнер
                </GridColumn>
                <GridColumn>
                  {this.state.designerOrder.name}
                </GridColumn>
              </GridRow>
              <GridRow>
                <GridColumn>
                  Стоимость
                </GridColumn>
                <GridColumn>
                  {this.state.designerOrder.price}
                </GridColumn>
              </GridRow>
            </Grid>
            <div style={{ paddingTop: '10px', marginTop: '64px' }} />
          </ModalContent>
          :
          <Fragment>
            <ModalContent scrolling>
              <Grid container columns={2}>
                <GridRow>
                  <GridColumn>
                    Дизайнер
                  </GridColumn>
                  <GridColumn>
                    {this.state.designerOrder.name}
                  </GridColumn>
                </GridRow>
                <GridRow>
                  <GridColumn>
                    Стоимость
                  </GridColumn>
                  <GridColumn>
                    <Input fluid type="number"
                           value={designerOrder.price}
                           onChange={(e) => {this.handleChange(e.target.value)}}
                    />
                  </GridColumn>
                </GridRow>
              </Grid>
              <div style={{ paddingTop: '10px', marginTop: '64px' }} />
            </ModalContent>
            <ModalActions>
              <Button positive content='Сохранить'
                      onClick={async () => {
                        await this.save();
                        this.modalOpened(false);

                      }} />
            </ModalActions>
          </Fragment>
      }

    </Modal>
  }

  async modalOpened(state) {
    await this.setState({ modalOpened: state });
  }

  renderButton() {
    let { designerOrder } = this.state;

    return (<TableRow key={designerOrder.id} onClick={async () => {
          await this.modalOpened(true)
        }}>
          <TableCell>{designerOrder.id}</TableCell>
          <TableCell>{designerOrder.name}</TableCell>
          <TableCell>{designerOrder.price}</TableCell>

        </TableRow>
    )
  }
}