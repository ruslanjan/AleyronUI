import React, {Component, Fragment} from 'react'
import {Menu, MenuItem} from 'semantic-ui-react';
import DesignerOrders from "./pages/DesinerOrders/DesignerOrders";


export default class Designer extends Component {

  handleItemClick = (e, {name}) => {
    if (name in this.funtionItems) {
      this.funtionItems[name]();
    } else {
      this.setState({activeItem: name})
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      activeItem: 'DesignerOrders',
    };
    this.itemsView = {
      'DesignerOrders': (<DesignerOrders utils={this.props.utils} handler={this.handleItemClick}/>),
    };
    this.funtionItems = {
      'BackToMain': () => {
        this.props.backToMain();
      }
    }
  }

  render() {
    let {activeItem} = this.state;
    return (
      <Fragment>
        <Menu pagination={true} fluid inverted={true} color={'blue'} fixed={'top'}>
          <MenuItem name='BackToMain' active={activeItem === 'BackToMain'} onClick={this.handleItemClick}>
            Назад
          </MenuItem>

          <MenuItem name='DesignerOrders' active={activeItem === 'DesignerOrders'} onClick={this.handleItemClick}>
            Проекты пользователя
          </MenuItem>
        </Menu>
        <div style={{paddingTop: '10px', marginTop: '64px'}}>
          {this.itemsView[activeItem]}
        </div>
      </Fragment>
    );
  }
}