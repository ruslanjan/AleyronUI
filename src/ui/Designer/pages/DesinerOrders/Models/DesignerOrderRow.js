import React, {Component, Fragment} from 'react';
import {
  Button,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Modal,
  ModalActions,
  ModalContent,
  ModalHeader,
  TableCell,
  TableRow
} from 'semantic-ui-react';

import Input from "semantic-ui-react/dist/es/elements/Input/Input";
import endpoints from "../../../../../config/endpoints";
import axios from "axios/index";
import _ from 'lodash';
import Man from "../../../../../utils/elements/Man";


export default class DesignerOrderRow extends Component {


  save = async () => {
    let {designerOrder} = this.state;
    await this.props.utils.loading(true);
    try {
      await axios({
        method: "post",
        url: endpoints.api.order.design_order.update,
        data: designerOrder,
        auth: this.props.utils.user,
        headers: {
          'Content-Type': 'application/json'
        },
      });
      await this.toast({
        visible: true,
        header: 'Панель администратора',
        color: 'green',
        body: 'Стоимость успешно сохранена!'
      });
      this.setState({blocked: designerOrder.cost !== 0});
      await this.props.onClose();
    } catch (e) {
      console.error(e);
      await this.toast({
        visible: true,
        color: 'red',
        header: 'Панель Администратора',
        body: 'Не удалось сохранить стоимость!'
      });
    } finally {
      await this.props.utils.loading(false);
    }
  };

  toast = async (state) => {
    this.props.utils.toast(state);
  };

  async componentDidMount(){
    await this.setState({designerOrder: _.cloneDeep(this.props.designerOrder)});
  }

  constructor(props) {
    super(props);
    this.state = {
      designerOrder: _.cloneDeep(this.props.designerOrder),
      modalOpened: false,
      blocked: this.props.designerOrder.cost !== 0,
    };
  }

  handleChange(event) {
    let {designerOrder} = this.state;
    designerOrder.cost = event;
    this.setState(
      {designerOrder: designerOrder}
    )
  }

  render() {
    let {modalOpened, designerOrder} = this.state;
    return <Modal
      onClose={() => this.modalOpened(false)}
      style={{marginTop: 0}}
      trigger={this.renderButton()}
      open={modalOpened}
    >
      <ModalHeader content={'Заказ Дизайна:' + this.state.designerOrder.invoice.title}/>
      <ModalContent scrolling>
        <Grid container columns={2}>
          <GridRow>
            <GridColumn>
              Дизайнер
            </GridColumn>
            <GridColumn>
              {this.props.utils.user.data.username}
            </GridColumn>
          </GridRow>
          <GridRow>
            <GridColumn>
              Клиента
            </GridColumn>
            <GridColumn>
              <Man invoice={this.state.designerOrder.invoice}/>
            </GridColumn>
          </GridRow>
          <GridRow>
            <GridColumn>
              Стоимость
            </GridColumn>
            <GridColumn>
              {this.state.blocked ?
                this.state.designerOrder.cost
                :
                <Form>
                  <FormField>
                    <Input fluid type="number"
                           defaultValue={designerOrder.cost}
                           onChange={(e) => {
                             this.handleChange(e.target.value)
                           }}/>
                  </FormField>
                </Form>
              }
            </GridColumn>
          </GridRow>
        </Grid>
      </ModalContent>
      {this.state.blocked ?
        <Fragment/>
        :
        <ModalActions>
          <Button positive content='Сохранить'
                  onClick={async () => {
                    await this.save();
                    await this.modalOpened(false);
                  }}/>
        </ModalActions>
      }
    </Modal>
  }

  async modalOpened(state) {
    await this.setState({modalOpened: state});
  }

  renderButton() {
    let {designerOrder} = this.state;
    return (<TableRow key={designerOrder.id} onClick={async () => {
        await this.modalOpened(true)
      }}>
        <TableCell>{designerOrder.id}</TableCell>
        <TableCell>{designerOrder.invoice.title}</TableCell>
        <TableCell><Man invoice={designerOrder.invoice}/></TableCell>
        <TableCell>{designerOrder.cost}</TableCell>
      </TableRow>
    )
  }
}