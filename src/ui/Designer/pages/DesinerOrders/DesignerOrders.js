import React, { Component, Fragment } from 'react'
import {
  Button,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Message,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHeader,
  TableHeaderCell,
  TableRow
} from 'semantic-ui-react';

import axios from 'axios'
import DesignerOrderRow from "./Models/DesignerOrderRow";
import endpoints from "../../../../config/endpoints";
import qs from 'qs';

export default class DesignerOrders extends Component {

  constructor(props) {
    super(props);
    this.state        = {
      designerOrders: [],
      page          : 0,
      success       : true
    };
    this.loadDataPage = this.loadDataPage.bind(this);
  }

  async componentDidMount() {
    await this.props.utils.loading(true);
    await this.loadDataPage();
    await this.props.utils.loading(false);
  }

  async loadDataPage(size = 12) {
    let { page } = this.state;
    this.props.utils.loading(true);
    try {
      let { data } = await axios(
          {
            method: 'get',
            url   : endpoints.api.manager.orders.getPage,
            params: {
              page, size,
              filters: [`designer.username=$eq|${this.props.utils.user.data.username}`, `invoice.status=$eq|FORMING`],
              sorts  : [('created:DESC')]
            },
            paramsSerializer: (params) => qs.stringify(params, { arrayFormat: 'brackets' }),
            auth  : this.props.utils.user
          });
      await this.setState({ designerOrders: data });
    } catch(e) {
      console.log(e);
      this.setState({ success: false })
    }
    finally {
      this.props.utils.loading(false);
    }
  }

  render() {
    if(!this.state.success) {
      this.props.utils.toast({
                               visible: true,
                               header : 'Панель дизайнера',
                               color  : 'red',
                               body   : 'Не известный ошибка случатся моя чинить, однако твоя не надейся, моя плохо чинить!'
                             });
    }
    return (
        <Fragment>
          {
            this.state.success ?
                <Grid container>
                  <GridRow>
                    <GridColumn>
                      <Table celled selectable>
                        <TableHeader>
                          <TableRow>
                            <TableHeaderCell colSpan={4}>
                              <Grid>
                                <GridRow textAlign={'right'}>
                                  <GridColumn textAlign={'right'}>
                                    <Button content={'перезагрузить'}
                                            primary
                                            onClick={async () => await this.loadDataPage()}
                                            labelPosition='left'
                                            icon='redo' />
                                  </GridColumn>
                                </GridRow>
                              </Grid>
                            </TableHeaderCell>
                          </TableRow>
                          <TableRow>
                            <TableHeaderCell>ID</TableHeaderCell>
                            <TableHeaderCell>Название</TableHeaderCell>
                            <TableHeaderCell>Клиент</TableHeaderCell>
                            <TableHeaderCell>Цена</TableHeaderCell>
                          </TableRow>
                        </TableHeader>
                        <TableBody>
                          {!!this.state.designerOrders.length ?
                              this.state.designerOrders.map((designerOrder) => (
                                  <DesignerOrderRow key={designerOrder.id} utils={this.props.utils}
                                                    designerOrder={designerOrder}
                                                    onClose={async () => await this.loadDataPage()}
                                  />
                              )) :
                              <TableRow>
                                <TableCell colSpan={4} content={'Заказов нет'} />
                              </TableRow>
                          }
                        </TableBody>
                        <TableFooter>
                          <TableRow>
                            <TableHeaderCell colSpan={9}>
                              <GridRow>
                                <GridColumn textAlign={'right'}>
                                  <Button primary onClick={async () => {
                                    await this.rewindPage();
                                  }}>
                                    <Icon name={'chevron left'} />
                                    Назад
                                  </Button>
                                  <Button primary onClick={async () => {
                                    await this.forwardPage();
                                  }}>
                                    Вперед
                                    <Icon name={'chevron right'} />
                                  </Button>
                                </GridColumn>
                              </GridRow>
                            </TableHeaderCell>
                          </TableRow>
                        </TableFooter>
                      </Table>
                    </GridColumn>
                  </GridRow>
                </Grid>
                :
                <div>
                  <Message error header='Error' content='can not load page ' />
                </div>
          }
        </Fragment>
    );
  }

  async forwardPage() {
    let { page } = this.state;
    await this.setState({ page: page + 1 });
    await this.loadDataPage();
    if(this.state.designerOrders.length <= 0) {
      await this.rewindPage();
    }
  }

  async rewindPage() {
    let { page } = this.state;
    await this.setState({ page: Math.max(page - 1, 0) });
    await this.loadDataPage();
  }

}