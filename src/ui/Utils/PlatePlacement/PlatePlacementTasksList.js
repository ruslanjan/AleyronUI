import React, { Component } from 'react';
import {
  Button,
  Grid,
  GridColumn,
  GridRow,
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableHeaderCell,
  TableRow,
} from 'semantic-ui-react';
import endpoints from '../../../config/endpoints';
import axios from 'axios';

export default class PlatePlacementTasksList extends Component {

  delTask = (id) => () => {
    axios({
            method: "post",
            params: { id: id },
            url   : endpoints.api.utils.plate_placement.tasks.deleteById,
            auth  : this.props.utils.user,
          }).then(() => {
                    this.loadDataPage();
                  },
    ).catch(() => {

      this.loadDataPage();
    });
    //socket.emit('auth.delUsers', {query: query, user: this.props.utils.user});
  };

  constructor(props) {

    super(props);
    this.state = {
      data: [],
      page: 0,
    };

    this.loadDataPage = this.loadDataPage.bind(this);
    this.loadDataPage();
  }

  loadDataPage(size = 12) {
    let { page } = this.state;
    this.props.utils.loading(true);
    axios({
            method: 'get',
            url   : endpoints.api.utils.plate_placement.tasks.getPage,
            auth  : this.props.utils.user,
            params: { page: page, size: size },
          }).then((res) => {

      this.setState({
                      data: res.data,
                    });
      this.props.utils.loading(false);
    }).catch(() => {
      this.props.utils.loading(false);
    });
  }

  render() {

    //noinspection JSUnresolvedFunction
    return (
        <div>
          <Grid container>
            <GridRow>
              <GridColumn>
                <Table celled>
                  <TableHeader>
                    <TableRow>
                      <TableHeaderCell>ID</TableHeaderCell>
                      <TableHeaderCell>Название</TableHeaderCell>
                      <TableHeaderCell>Создатель</TableHeaderCell>
                      <TableHeaderCell>Открыть</TableHeaderCell>
                      <TableHeaderCell>Удалить</TableHeaderCell>
                    </TableRow>
                  </TableHeader>
                  <TableBody>
                    {this.state.data.map((task) => (
                        <TableRow key={task.id}>
                          <TableCell>{task.id}</TableCell>
                          <TableCell>{task.name}</TableCell>
                          <TableCell>{task.user}</TableCell>
                          <TableCell><Button
                              onClick={this.props.openTask(task.id)}
                              icon={'unhide'} /></TableCell>
                          <TableCell><Button onClick={this.delTask(task.id)}
                                             icon={'delete'} /></TableCell>
                        </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </GridColumn>
            </GridRow>
            <GridRow columns={2}>
              <GridColumn>
                <Button floated={'right'} onClick={async () => {
                  let { page } = this.state;
                  await this.setState({ page: Math.max(page - 1, 0) });
                  this.loadDataPage();
                }}>Назад</Button>
              </GridColumn>
              <GridColumn>
                <Button floated={'left'} onClick={async () => {
                  let { page } = this.state;
                  await this.setState({ page: page + 1 });
                  this.loadDataPage();
                }}>Вперед</Button>
              </GridColumn>
            </GridRow>
          </Grid>
        </div>
    );
  }
}