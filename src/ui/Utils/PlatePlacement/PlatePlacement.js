/*
 * Copyright (c) 2018 Aleyron Systems, Inc - All Rights Reserved.
 * This file is part of com.maddyhome.idea.copyright.pattern.ProjectInfo@6edfcf60.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

import React, { Component, Fragment } from 'react';
import PlatePlacementMenu from "./PlatePlacementMenu";
import PlatePlacementTasksList from "./PlatePlacementTasksList";
import PlatePlacementCalc from "./PlatePlacementCalc";
import endpoints from '../../../config/endpoints';
import axios from "axios";

export default class PlatePlacement extends Component {

  constructor(props) {

    super(props);
    this.state = {
      input: null
    };

    this.state = {
      activePage: 'PlatePlacementCalc',
      pages     : {
        'PlatePlacementCalc'     : (
            <PlatePlacementCalc getInput={() => this.state.input} utils={this.props.utils} />),
        'PlatePlacementTasksList': (<PlatePlacementTasksList
            utils={this.props.utils}
            openTask={(id) => () => {
              this.props.utils.loading(true);
              //noinspection JSUnresolvedVariable
              axios({
                      method: 'get',
                      url   : endpoints.api.utils.plate_placement.tasks.getTaskById,
                      params: { id },
                      auth  : this.props.utils.user
                    })
                  .then((res) => {
                    this.setState({
                                    input     : res.data,
                                    activePage: "PlatePlacementCalc",
                                  });
                    this.props.utils.loading(false);
                  })
                  .catch(() => {

                    this.props.utils.loading(false);
                  })
            }}
        />),
      }
    };
  }

  render() {
    return (
        <Fragment>
          <PlatePlacementMenu activePage={this.state.activePage}
                              handleItemClick={(e, { name }) => {
                                this.setState({ activePage: name });
                              }} />
          {this.state.pages[this.state.activePage]}
        </Fragment>
    );
  }
}