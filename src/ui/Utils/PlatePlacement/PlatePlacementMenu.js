import React, { Component } from 'react'
import { Menu, MenuItem } from 'semantic-ui-react';

export default class PlatePlacementMenu extends Component {

  render() {

    return (
        <Menu inverted fluid>
          <MenuItem
              name='PlatePlacementCalc'
              active={this.props.activePage === 'PlatePlacementCalc'}
              onClick={this.props.handleItemClick}
          >
            Размещатель
          </MenuItem>

          <MenuItem
              name='PlatePlacementTasksList'
              active={this.props.activePage === 'PlatePlacementTasksList'}
              onClick={this.props.handleItemClick}
          >
            Список
          </MenuItem>
        </Menu>
    )
  }
}