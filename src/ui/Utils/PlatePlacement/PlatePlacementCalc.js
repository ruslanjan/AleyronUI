/*
 * Copyright (c) 2018 Aleyron Inc - All Rights Reserved.
 * Unauthorized copying of this file is strictly prohibited
 * Proprietary and confidential
 */

import React, { Component } from 'react';
import {
  Button,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Input,
  Label,
  Radio,
  Segment,
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableHeaderCell,
  TableRow
} from "semantic-ui-react";
import endpoints from '../../../config/endpoints';
import axios from 'axios';

class PlatePlacementCalc extends Component {
  constructor(props) {

    super(props);
    this.state = {
      input     : {
        name       : '',
        type       : "Чужой оборот",
        products   : [],
        plateNumber: 0,
        capacity   : 0,
      },
      result    : [],
      plateRes  : [],
      overSum   : 0,
      pageSum   : 0,
      productSum: 0,
    };
    //noinspection JSUnresolvedFunction
    if(this.props.getInput()) {
      //noinspection JSUnresolvedFunction
      this.state.input = this.props.getInput();
    }
  }

  calc() {

    let { type, products, plateNumber, capacity } = this.state.input;
    plateNumber                                   = Number(plateNumber);
    capacity                                      = Number(capacity);
    let result                                    = [];
    if(type !== "Чужой оборот") {
      capacity = Math.floor(capacity / 2);
    }
    let a = [];

    if(products.length === 0) {
      alert("нечего просчитывать");
      this.setState({ result: result });
      return;
    }
    if(plateNumber * capacity < products.length) {
      alert('нет решения');
      this.setState({ result: result });
      return;
    }
    for(let i = 0; i < products.length; ++i) {
      if(products[i].cnt === '') {
        alert('не введено количество у вида с названием "' + products[i].name + '"');
      }
      a.push([Number(products[i].cnt), [products[i].id, products[i].name]]);
    }
    a.sort();
    let checkPlate = function (ql, qr) {
      let l, r;
      l         = 0;
      r         = 1000000;
      let m     = -1;
      let check = function (m) {
        let sum = 0;
        for(let i = ql; i < qr; ++i) {
          sum += Math.ceil(a[i][0] / m);
        }
        return sum <= capacity;
      };
      while(r - l > 1) {
        m = ((r + l) >> 1);
        if(check(m, capacity)) {
          r = m;
        } else {
          l = m;
        }
      }
      if(check(r, capacity)) {
        let sum = 0;
        for(let i = ql; i < qr; ++i) {
          sum += r * Math.ceil(a[i][0] / r) - a[i][0];
        }
        return sum;
      } else {
        return 1000000000;
      }
    };

    let dp = [];
    let p  = [];
    for(let i = 0; i < products.length; ++i) {
      dp.push([]);
      p.push([]);
    }
    for(let i = 0; i < products.length; ++i) {
      for(let j = 0; j < plateNumber; ++j) {
        dp[i][j] = 1000000000;
      }
    }
    for(let i = 0; i < products.length; ++i) {
      dp[i][0] = checkPlate(0, i + 1);
    }
    p[0][0] = -1;
    for(let i = 1; i < products.length; ++i) {
      for(let j = 1; j < plateNumber; ++j) {
        for(let l = i; l > 0; --l) {
          if((i - l + 1 <= capacity) && dp[i][j] > dp[l - 1][j - 1] + (a[i][0] - a[l][0])) {
            p[i][j]  = l - 1;
            dp[i][j] = dp[l - 1][j - 1] + (a[i][0] - a[l][0]);
          }
        }
      }
    }
    if(dp[products.length - 1][plateNumber - 1] >= 1000000000) {
      alert('no solution');
    } else {
      let j  = products.length - 1;
      let c  = plateNumber - 1;
      let l  = p[j][c];
      result = [];
      for(let i = 0; i < plateNumber; ++i) {
        result.push([]);
      }
      while(j >= 0) {
        if(j === l) {
          c--;
          l = p[j][c];
        }
        result[c].push(a[j]);
        j--;
      }
    }
    let plateRes   = [];
    let overSum    = 0;
    let pageSum    = 0;
    let productSum = 0;
    let update     = function (id) {

      let getM                                  = function () {
        let l, r;
        l         = 0;
        r         = 1000000;
        let m     = -1;
        let check = function (m) {
          let sum = 0;
          for(let i = 0; i < result[id].length; ++i) {
            sum += Math.ceil(result[id][i][0] / m);
          }
          return sum <= capacity;
        };
        while(r - l > 1) {
          m = ((r + l) >> 1);
          if(check(m, capacity)) {
            r = m;
          } else {
            l = m;
          }
        }
        if(check(r, capacity)) {
          return r;
        } else {
          return 1000000000;
        }
      };
      let m                                     = getM();
      plateRes[id].pageSum                      = m;
      plateRes[id].overSum                      = 0;
      plateRes[id].DigitalPrintWorkStorageItems = [];
      plateRes[id].id                           = id + 1;
      //
      for(let i = 0; i < result[id].length; ++i) {
        plateRes[id].DigitalPrintWorkStorageItems.push({});
      }
      for(let i = 0; i < result[id].length; ++i) {
        //
        plateRes[id].DigitalPrintWorkStorageItems[i].over   = Math.ceil(result[id][i][0] / m) * m - result[id][i][0];
        productSum += Math.ceil(result[id][i][0] / m) * m;
        plateRes[id].DigitalPrintWorkStorageItems[i].number = Math.ceil(result[id][i][0] / m);
        plateRes[id].DigitalPrintWorkStorageItems[i].tname  = result[id][i][1][1];
        plateRes[id].DigitalPrintWorkStorageItems[i].id     = result[id][i][1][0];
        plateRes[id].overSum += plateRes[id].DigitalPrintWorkStorageItems[i].over;
      }
      overSum += plateRes[id].overSum;
      pageSum += plateRes[id].pageSum;
    };
    for(let i = 0; i < plateNumber; ++i) {
      plateRes.push({});
      update(i);
    }
    this.setState({
                    plateRes  : plateRes,
                    overSum   : overSum,
                    pageSum   : pageSum,
                    productSum: productSum,
                    result    : result,
                  });
  }

  render() {
    return (
        <Grid container padded='vertically'>
          <GridRow>
            <GridColumn>
              <Segment>
                <Grid>
                  <GridRow columns={2}>
                    <GridColumn>
                      <Form>
                        <FormField>
                          <label>Название</label>
                          <input onChange={(e) => {
                            let { input } = this.state;
                            input.name    = e.target.value;
                            this.setState({ input: input })
                          }}
                                 placeholder='Название'
                                 defaultValue={this.state.input.name}
                          />
                        </FormField>
                        <FormField>
                          <label>Сколько печатных листов (Заводов)</label>
                          <input onChange={(e) => {
                            let { input }     = this.state;
                            input.plateNumber = e.target.value;
                            this.setState({ input: input })
                          }} type={'number'}
                                 placeholder='Сколько печатных листов (Заводов)'
                                 defaultValue={this.state.input.plateNumber}
                          />
                        </FormField>
                        <FormField>
                          <label>Сколько помещается на печатном листе</label>
                          <input onChange={(e) => {
                            let { input }  = this.state;
                            input.capacity = e.target.value;
                            this.setState({ input: input })
                          }} type={'number'}
                                 placeholder='Сколько помещается на печатном листе'
                                 defaultValue={this.state.input.capacity}
                          />
                        </FormField>
                      </Form>
                    </GridColumn>
                    <GridColumn>
                      <Form>
                        <FormField>
                          Выбран: <b>{this.state.input.type}</b>
                        </FormField>
                        <FormField>
                          <Radio
                              label='Свой оборот'
                              name='radioGroup'
                              value='Свой оборот'
                              checked={this.state.input.type === 'Свой оборот'}
                              onChange={(e, { value }) => {
                                let { input } = this.state;
                                input.type    = value;
                                this.setState({ input: input })
                              }}
                          />
                        </FormField>
                        <FormField>
                          <Radio
                              label='Чужой оборот'
                              name='radioGroup'
                              value='Чужой оборот'
                              checked={this.state.input.type === 'Чужой оборот'}
                              onChange={(e, { value }) => {
                                let { input } = this.state;
                                input.type    = value;
                                this.setState({ input: input })
                              }}
                          />
                        </FormField>
                      </Form>
                    </GridColumn>
                  </GridRow>
                </Grid>
              </Segment>
            </GridColumn>
          </GridRow>
          <GridRow columns={2}>
            <GridColumn>
              <Segment>
                <h2>Количество видов: {this.state.input.products.length}</h2>
                <div>
                  <Button onClick={() => {
                    let { products } = this.state.input;
                    products.push({
                                    name: products.length + 1,
                                    id  : products.length,
                                    cnt : 0,
                                  });
                    let { input }  = this.state;
                    input.products = products;
                    this.setState({ input: input })
                  }} color={'green'} icon={'add'} />
                  <Button onClick={() => {
                    let { products } = this.state.input;
                    products.pop();
                    let { input }  = this.state;
                    input.products = products;
                    this.setState({ input: input })
                  }} color={'red'} icon={'minus'} />
                </div>
                <Table celled stackable>
                  <TableHeader>
                    <TableRow>
                      <TableHeaderCell>Имя</TableHeaderCell>
                      <TableHeaderCell>Количество</TableHeaderCell>
                    </TableRow>
                  </TableHeader>

                  <TableBody>
                    {this.state.input.products.map((product) => (
                        <TableRow key={product.id}>
                          <TableCell><Input fluid defaultValue={product.name} onChange={(e) => {
                            let { products }          = this.state.input;
                            products[product.id].name = e.target.value;
                            let { input }             = this.state;
                            input.products            = products;
                            this.setState({ input: input })
                          }} /></TableCell>
                          <TableCell><Input fluid defaultValue={product.cnt} onChange={(e) => {
                            let { products }         = this.state.input;
                            products[product.id].cnt = e.target.value;
                            let { input }            = this.state;
                            input.products           = products;
                            this.setState({ input: input })
                          }} type={'number'} /></TableCell>
                        </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Segment>
              <Grid padded={'vertically'}>
                <GridColumn>
                  <Segment>
                    <Button fluid color={'violet'} onClick={() => {
                      let input = JSON.parse(JSON.stringify(this.state.input));
                      delete input.id;
                      delete input.user;
                      this.props.utils.loading(true);
                      axios({
                              method: "post",
                              url   : endpoints.api.utils.plate_placement.tasks.create,
                              data  : input,
                              auth  : this.props.utils.user
                            })
                          .then(() => {
                            this.props.utils.loading(false);
                            alert("Сохраннено");
                          })
                          .catch(() => {
                            this.props.utils.loading(false);
                            alert("Ошибка, позовите админа");
                          });
                    }}>
                      Сохранить
                    </Button>
                  </Segment>
                </GridColumn>
              </Grid>
            </GridColumn>
            <GridColumn>
              <Segment>
                <Grid>
                  <GridRow>
                    <GridColumn>
                      <Button color={'green'} fluid onClick={this.calc.bind(this)}>
                        Посчитать
                      </Button>
                    </GridColumn>
                  </GridRow>
                  <GridRow>
                    <GridColumn>
                      <Table celled>
                        <TableHeader>
                          <TableRow>
                            <TableHeaderCell>Общий тираж</TableHeaderCell>
                            <TableHeaderCell>Всего листов</TableHeaderCell>
                            <TableHeaderCell>Всего излишки</TableHeaderCell>
                          </TableRow>
                        </TableHeader>
                        <TableBody>
                          <TableRow>
                            <TableCell><Label color='blue' ribbon>{this.state.productSum}</Label></TableCell>
                            <TableCell>{this.state.pageSum}</TableCell>
                            <TableCell>{this.state.overSum}</TableCell>
                          </TableRow>
                        </TableBody>
                      </Table>
                    </GridColumn>
                  </GridRow>
                  {this.state.plateRes.map((list) => (
                      <GridRow key={list.id}>
                        <GridColumn>
                          <h2>Пластина #{list.id}</h2>
                          <Table celled>
                            <TableHeader>
                              <TableRow>
                                <TableHeaderCell>Листов</TableHeaderCell>
                                <TableHeaderCell>Излишки</TableHeaderCell>
                              </TableRow>
                            </TableHeader>
                            <TableBody>
                              <TableRow>
                                <TableCell>{list.pageSum}</TableCell>
                                <TableCell>{list.overSum}</TableCell>
                              </TableRow>
                            </TableBody>
                          </Table>
                          <Table celled>
                            <TableHeader>
                              <TableRow>
                                <TableHeaderCell>Вид</TableHeaderCell>
                                <TableHeaderCell>Количество на пластине</TableHeaderCell>
                                <TableHeaderCell>Излишки</TableHeaderCell>
                              </TableRow>
                            </TableHeader>
                            <TableBody>
                              {list.DigitalPrintWorkStorageItems.map((type) => (
                                  <TableRow key={type.id}>
                                    <TableCell>{type.tname}</TableCell>
                                    <TableCell>{type.number}</TableCell>
                                    <TableCell>{type.over}</TableCell>
                                  </TableRow>
                              ))}
                            </TableBody>
                          </Table>
                        </GridColumn>
                      </GridRow>
                  ))}
                </Grid>
              </Segment>
            </GridColumn>
          </GridRow>
        </Grid>
    );
  }
}

export default PlatePlacementCalc;
