import React, { Component } from 'react';
import { Button, Header, Icon, Modal, ModalActions, ModalContent } from 'semantic-ui-react';
import axios from 'axios';
import endpoints from '../../../config/endpoints';

export default class MakePaid extends Component {
  constructor(props) {

    super(props);

    this.state = {
      id          : this.props.id,
      modalLoading: false,
      modalOpened : false
    }
  }

  async componentDidUpdate(prevProps) {
    if(this.props.id !== prevProps.id) {
      await this.setState({ id: this.props });
    }
  }

  render() {
    return (<Modal trigger={this.renderButton()}
                   open={this.state.modalOpened}
                   basic
                   onClose={() => this.modalOpened(false)}>
          <Header icon='money' content='Оплата' />
          <ModalContent>
            Вы уверены что хотите сделать это? Вы не сможете это изменить.
          </ModalContent>
          <ModalActions>
            <Button basic inverted onClick={() => this.modalOpened(false)}>
              <Icon name='remove' /> Отмена
            </Button>
            <Button primary inverted
                    loading={this.state.modalLoading}
                    onClick={() => this.markPaid(this.state.id)}>
              <Icon name='arrow circle down' /> Оплатить
            </Button>
          </ModalActions>
        </Modal>
    );
  }

  async modalOpened(state) {
    await this.setState({ modalOpened: state });
  }

  async markPaid(id) {
    await this.modalLoading(true);
    try {
      await axios({
                    method : 'post',
                    url    : endpoints.api.booker.invoices.status.setPaid,
                    auth   : this.props.utils.user,
                    data   : id,
                    headers: {
                      'Content-Type': 'application/json'
                    }
                  })
    } finally {
      await this.modalLoading(false);
      await this.modalOpened(false);
      //noinspection JSUnresolvedFunction
      await this.props.onDeleted();
    }
  }

  async modalLoading(state) {
    await this.setState({ modalLoading: state });
  }

  renderButton() {
    return (<Button disabled={this.props.disabled} icon={'currency'}
                    primary
                    content={'оплатить'}
                    onClick={() => this.modalOpened(true)} />);
  }

}