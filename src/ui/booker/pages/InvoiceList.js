import _ from 'lodash';
import React, { Component } from 'react';
import {
  Button,
  Dropdown,
  Form,
  FormField,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Input,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHeader,
  TableHeaderCell,
  TableRow,
} from 'semantic-ui-react';
import endpoints from '../../../config/endpoints';
import axios from 'axios';
import MakePaid from '../modals/MakePaid';
import Permission from '../../../utils/Permission';
import RenderHelper from "../../../utils/RenderHelper";
import qs from 'qs';

const allStatus = [
  { key: 'FORMING', value: 'FORMING', text: 'FORMING' },
  { key: 'PENDING', value: 'PENDING', text: 'PENDING' },
  { key: 'PAID', value: 'PAID', text: 'PAID' },
  { key: 'CANCELLED', value: 'CANCELLED', text: 'CANCELLED' },
  { key: 'NULL', value: 'NULL', text: 'ANY' },
];

export default class InvoiceList extends Component {
  constructor(props) {

    super(props);
    this.state = {
      page    : 0,
      size    : 10,
      invoices: [],
      filters : {}
    };
  }

  async componentDidUpdate() {

  }

  async componentDidMount() {
    await this.loadPage();
  }

  async setFilter(name, value) {
    let { filters } = this.state;

    if(value) {
      filters[name] = value;
    } else {
      delete filters[name];
    }
    await this.setState({ filters });

    clearTimeout(this.state.timeout);

    let timeout = setTimeout(async () => {
      await this.loadPage();
    }, 500);

    await this.setState({ timeout });
  }

  async loadPage() {
    await this.props.utils.loading(true);

    let _filters = _.map(Object.entries(this.state.filters), e => e.join(''));
    try {
      let { page, size, filters } = this.state;
      let request                 = filters.hasOwnProperty('_id') ? {
        method: 'get',
        url   : endpoints.api.booker.invoices.getById,
        params: {
          id: filters._id
        },
        auth  : this.props.utils.user
      } : {
        method          : 'get',
        url             : endpoints.api.booker.invoices.getPage,
        params          : {
          page, size,
          filters: _filters,
          sorts  : [('dateCreated:DESC')]
        },
        paramsSerializer: (params) => qs.stringify(params, { arrayFormat: 'brackets' }),
        auth            : this.props.utils.user
      };
      let { data }                = await axios(request);

      if(!(data instanceof Array)) data = [data];

      await this.setState({ invoices: data });

    } finally {
      await this.props.utils.loading(false);
    }
  }

  render() {
    return (
        <Grid container padded={'vertically'}>
          <GridRow>
            <GridColumn>
              <Table celled>
                <TableHeader>
                  <TableRow>
                    <TableHeaderCell>
                      ID
                    </TableHeaderCell>
                    <TableHeaderCell>
                      1C Счет
                    </TableHeaderCell>
                    <TableHeaderCell>
                      Статус
                    </TableHeaderCell>
                    <TableHeaderCell rowSpan={2}>
                      Клиент
                    </TableHeaderCell>
                    <TableHeaderCell rowSpan={2}>
                      Название
                    </TableHeaderCell>
                    <TableHeaderCell rowSpan={2}>
                      Цена
                    </TableHeaderCell>
                    <TableHeaderCell rowSpan={2}>
                      Действия
                    </TableHeaderCell>
                  </TableRow>
                  <TableRow>
                    <TableHeaderCell>
                      <Input fluid size={'mini'} type={'search'}
                             onChange={async (e, { value }) => {
                               await this.setFilter('_id', value);
                             }} />
                    </TableHeaderCell>
                    <TableHeaderCell>
                      <Input fluid size={'mini'} type={'search'}
                             onChange={async (e, { value }) => {
                               await this.setFilter('invoice1C=$eq|', value);
                             }} />
                    </TableHeaderCell>
                    <TableHeaderCell>
                      <Form size={'mini'}>
                        <FormField>
                          <Dropdown fluid
                                    options={allStatus}
                                    selection search
                                    defaultValue={('NULL')}
                                    onChange={async (e, { value }) => {
                                      if(value !== 'NULL') {
                                        await this.setFilter('status=$eq|', value);
                                      }
                                    }} />
                        </FormField>
                      </Form>
                    </TableHeaderCell>
                  </TableRow>
                </TableHeader>
                <TableBody>
                  {this.state.invoices.map((invoice) => {
                    return (
                        <TableRow key={invoice.id}>
                          <TableCell>
                            {invoice.id}
                          </TableCell>
                          <TableCell>
                            {invoice.invoice1C}
                          </TableCell>
                          <TableCell>
                            {RenderHelper.renderInvoiceStatus(invoice.status)}
                          </TableCell>
                          <TableCell>
                            {invoice.clientName}
                          </TableCell>
                          <TableCell>
                            {invoice.title}
                          </TableCell>
                          <TableCell>
                            KZT {invoice.cost}
                          </TableCell>
                          <TableCell>
                            <MakePaid utils={this.props.utils}
                                      id={invoice.id}
                                      disabled={(invoice.status !== 'PENDING') ||
                                      (!Permission.user(this.props.utils.user).granted('booker.setPaid'))}
                                      onDeleted={async () => await this.loadDataPage()} />
                          </TableCell>
                        </TableRow>
                    );
                  })}
                </TableBody>
                <TableFooter fullWidth>
                  <TableRow>
                    <TableHeaderCell />
                    <TableHeaderCell colSpan={6}><Grid>
                      <GridRow>
                        <GridColumn textAlign={'right'}>
                          <Button primary onClick={async () => {
                            await this.rewindPage();
                          }}>
                            <Icon name={'chevron left'} />
                            Назад
                          </Button>
                          <Button primary onClick={async () => {
                            await this.forwardPage();
                          }}>
                            Вперед
                            <Icon name={'chevron right'} />
                          </Button>
                        </GridColumn>
                      </GridRow>
                    </Grid>
                    </TableHeaderCell>
                  </TableRow>
                </TableFooter>
              </Table>
            </GridColumn>
          </GridRow>
        </Grid>
    );
  }

  // region pager
  async forwardPage() {
    let { page } = this.state;
    await this.setState({ page: page + 1 });
    await this.loadDataPage();
    if(this.state.invoices.length <= 0) {
      await this.rewindPage();
    }
  }

  async rewindPage() {
    let { page } = this.state;
    await this.setState({ page: Math.max(page - 1, 0) });
    await this.loadDataPage();
  }

  // endregion

}