import React, {Component, Fragment} from 'react';
import {Menu, MenuItem} from 'semantic-ui-react';
import InvoiceList from './pages/InvoiceList';

export default class Booker extends Component {
  handleItemClick = (e, { name, data }) => {
    if(name in this.funtionItems) {
      this.funtionItems[name]();
    } else {
      this.setState({ activeItem: name, data: data })
    }
  };

  constructor(props) {

    super(props);

    this.state        = {
      activeItem: 'InvoiceList',
      data: null
    };
    this.itemsView    = {
      'InvoiceList': (<InvoiceList  utils={this.props.utils} />),
    };
    this.funtionItems = {
      'BackToMain': () => {
        this.props.backToMain();
      }
    }
  }

  render() {
    let { activeItem } = this.state;
    return (
        <Fragment>
          <Menu pagination={true} fluid inverted={true} color={'blue'} fixed={'top'}>
            <MenuItem
                name='BackToMain'
                active={activeItem === 'BackToMain'}
                onClick={this.handleItemClick}
            >
              Назад
            </MenuItem>

            <MenuItem
                name='InvoiceList'
                active={activeItem === 'InvoiceList'}
                onClick={this.handleItemClick}
            >
                Список Счетов
            </MenuItem>


            <MenuItem position={'right'} header>Бугалтер</MenuItem>
          </Menu>
          <div style={{ paddingTop: '10px', marginTop: '32px' }}>
            {this.itemsView[activeItem]}
          </div>
        </Fragment>
    );
  }
}