import React, { Component, Fragment } from 'react';
import { Dropdown, DropdownItem, DropdownMenu, Menu, MenuItem, } from 'semantic-ui-react';
import Manager from './Manager/Manager';
import Admin from './Admin/Admin';
import Home from './Home/Home';
import PlatePlacement from './Utils/PlatePlacement/PlatePlacement';
import DigitalPrint from './DigitalPrint/DigitalPrint';
import Permission from '../utils/Permission';
import Booker from './booker/Booker';
import env from '../config/env';
import Designer from "./Designer/Designer";

export default class Main extends Component {
  state = { visible: false };

  handleItemClick = (e, { name }) => {
    if(name === 'logout') {
      this.props.logout();
    } else {
      if(name in this.functionItems) {
        this.functionItems[name]();
      } else {
        this.setState({ activeItem: name });
      }
    }
  };

  constructor(props) {

    super(props);
    this.state         = {
      activeItem: 'Home',
      permit    : {},
    };
    this.items         = {
      'Home'          : <Home utils={this.props.utils} />,
      'PlatePlacement': <PlatePlacement utils={this.props.utils}
      />,
    };
    this.functionItems = {
      'DigitalPrint': () => {
        //noinspection JSUnresolvedFunction
        this.props.changePage((
                                  <DigitalPrint backToMain={this.props.utils.backToMain}
                                                utils={this.props.utils}
                                  />));
      },
      'Admin'       : () => {
        //noinspection JSUnresolvedFunction
        this.props.changePage(
            <Admin backToMain={this.props.utils.backToMain}
                   utils={this.props.utils}
            />);
      },
      'Manager'     : () => {
        //noinspection JSUnresolvedFunction
        this.props.changePage(
            <Manager backToMain={this.props.utils.backToMain}
                     utils={this.props.utils}
            />,
        );
      },
      'Booker'      : () => {
        //noinspection JSUnresolvedFunction
        this.props.changePage(
            <Booker backToMain={this.props.utils.backToMain}
                    utils={this.props.utils} />
        )
      },
      'Designer'      : () => {
        //noinspection JSUnresolvedFunction
        this.props.changePage(
          <Designer backToMain={this.props.utils.backToMain}
                  utils={this.props.utils} />
        )
      }
    };
  }

  async componentDidMount() {
    if(env.status === 'DEBUG') {
      this.handleItemClick(null, { name: env.debug.auto.page });
    }
  }

  render() {
    const { activeItem } = this.state;

    return (
        <Fragment>
          <Menu className={'mainMenu'} inverted color={'red'} fixed={'left'}
                vertical size={'large'}>
            <MenuItem>
              {this.props.utils.user.username}
            </MenuItem>
            <MenuItem name='Home' active={activeItem === 'Home'}
                      onClick={this.handleItemClick}>
              Aleyron
            </MenuItem>
            <MenuItem
                disabled={!Permission.user(this.props.utils.user).granted('manager.ui')}
                // disabled={true}
                name='Manager'
                active={activeItem === 'Manager'}
                onClick={this.handleItemClick}>
              Менеджер
            </MenuItem>
            <MenuItem
                disabled={!Permission.user(this.props.utils.user).granted('booker.ui')}
                //                disabled={true}
                name='Booker'
                active={activeItem === 'Booker'}
                onClick={this.handleItemClick}>
              Бухгалтерия
            </MenuItem>
            <MenuItem
                disabled={!Permission.user(this.props.utils.user).granted('digital_print.ui')}
                name='DigitalPrint'
                active={activeItem === 'DigitalPrint'}
                onClick={this.handleItemClick}>
              Цифровая Печать
            </MenuItem>
            <MenuItem
                disabled={!Permission.user(this.props.utils.user).granted('admin.ui')}
                name='Admin' active={activeItem === 'Admin'}
                onClick={this.handleItemClick}>
              Админ
            </MenuItem>

            <MenuItem
              name='Designer' active={activeItem === 'Designer'}
              onClick={this.handleItemClick}>
              Дизайнер
            </MenuItem>

            <MenuItem
                disabled={true} name='Chat' active={activeItem === 'Chat'}
                      onClick={this.handleItemClick}>
              Чат
            </MenuItem>
            <Dropdown item text='Утилиты'>
              <DropdownMenu>
                <DropdownItem name='PlatePlacement'
                              active={activeItem === 'PlatePlacement'}
                              onClick={this.handleItemClick}>
                  Размещение На Пластину</DropdownItem>
                <DropdownItem disabled={true}>Кофемашина</DropdownItem>
              </DropdownMenu>
            </Dropdown>
            <MenuItem name='logout' active={activeItem === 'logout'}
                      onClick={this.handleItemClick}>
              Выход
            </MenuItem>
          </Menu>
          <div style={{
            height    : '100%',
            marginLeft: '252px',
            minWidth  : '550px',
            zIndex    : 102,
          }}>
            {this.items[activeItem]}
          </div>
        </Fragment>
    );
  }
}
