    import React, {Component, Fragment}     from 'react';
import AleyronMain                      from './pages/AleyronMain'
import AleyronBlog                      from './pages/AleyronBlog'
import AleyronNotices                   from './pages/AleyronNotices'

export default class Home extends Component {
    state = {
        currentPage: 'Main'
    };

    //noinspection JSUnusedGlobalSymbols
  pages = {
        Main: <AleyronMain/>,
        Blog: <AleyronBlog/>,
        Notices: <AleyronNotices/>
    };

    render() {
        return (
            <Fragment>
                <AleyronMain/>
                {/*<div>*/}
                    {/*<Button.Group widths='5'>*/}
                        {/*<Button animated onClick={() => this.setState({currentPage: 'Main'})}>*/}
                            {/*<Button.Content hidden>Главная</Button.Content>*/}
                            {/*<Button.Content visible>*/}
                                {/*<Icon name='home'/>*/}
                            {/*</Button.Content>*/}
                        {/*</Button>*/}
                        {/*<Button animated='vertical' onClick={() => this.setState({currentPage: 'Blog'})}>*/}
                            {/*<Button.Content hidden> Блог </Button.Content>*/}
                            {/*<Button.Content visible>*/}
                                {/*<Icon name='newspaper'/>*/}
                            {/*</Button.Content>*/}
                        {/*</Button>*/}
                        {/*<Button animated='fade' onClick={() => this.setState({currentPage: 'Notices'})}>*/}
                            {/*<Button.Content visible>*/}
                                {/*<Icon name='file alternate'/>*/}
                            {/*</Button.Content>*/}
                            {/*<Button.Content hidden> Блог </Button.Content>*/}
                        {/*</Button>*/}
                    {/*</Button.Group>*/}
                {/*</div>*/}
                {/*{this.pages[this.state.currentPage]}*/}
            </Fragment>
        )
    }
}