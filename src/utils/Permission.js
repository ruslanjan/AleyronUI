import _ from 'lodash';

function user(user) {
  return {
    granted: (permission) => {

      let privileges = user.data.privileges;

      let items = _.reduce(user.data.roles, (a, b) => a.concat(b.privileges), []);



      privileges = _.without(
        privileges.concat(
          items
        ), null, undefined);

      for(let perm of privileges) {
        let pattern = new RegExp(`^${perm.pattern}$`);
        let result  = pattern.exec(permission);
        if(!(null == result)) {
          return true;
        }
      }

      return false;
    }
  };
}

export default {
  user: user
}