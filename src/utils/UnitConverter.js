import _ from 'lodash';
import React, { Fragment } from 'react';

const units = {
  g   : { render: 'г', canonical: 'g' },
  kg  : { render: 'кг', canonical: 'g' },
  m   : { render: 'м', canonical: 'm' },
  cm  : { render: 'см', canonical: 'm' },
  mm  : { render: 'мм', canonical: 'm' },
  sqm : { render: <Fragment>м<sup>2</sup></Fragment>, canonical: 'sqm' },
  sqcm: { render: <Fragment>см<sup>2</sup></Fragment>, canonical: 'sqm' },
  sqmm: { render: <Fragment>мм<sup>2</sup></Fragment>, canonical: 'sqm' },
  ml  : { render: 'мл', canonical: 'ml' },
  ltr : { render: 'л', canonical: 'ml' },
  pcs : { render: 'шт', canonical: 'pcs' },
};

const conversions = {
  m  : { m: 1.0, cm: 100.0, mm: 1000.0 },
  sqm: { sqm: 1.0, sqcm: 10000.0, sqmm: 1000000.0 },
  ml : { ltr: 1.0, ml: 1000.0 },
  g  : { kg: 1.0, g: 1000.0 },
  pcs: { pcs: 1.0 }
};

const serverUnits = {
  METER       : 'm',
  MILLILITER  : 'ml',
  SQUARE_METER: 'sqm',
  PIECE       : 'pcs',
  GRAMME      : 'g'

};

const round = (nValue, rem = 3) => {
  let shift = Math.pow(10.0, rem);
  return Math.round(nValue * shift) / shift;
};

export default class UnitConverter {
  static get dropdownUnits() {
    return _.map(Object.keys(units),
                 (unit) => ({ key: unit, value: unit, text: UnitConverter.renderUnit(unit) })
    );
  }

  //noinspection JSUnusedGlobalSymbols
  static get listUnits() {
    return units;
  }

  static renderUnit(unit) {
    if(!units.hasOwnProperty(unit)) {
      return unit;
    }

    return units[unit].render;
  }

  static normalizeValue(value) {
    return {
      withUnit: unit => {
        if(!units.hasOwnProperty(unit)) {
          return [value, unit];
        }

        return UnitConverter
            .convertValue(value)
            .withUnit(unit)
            .toUnit(units[unit].canonical);
      }
    };
  }

  static readUnit(serverUnit) {
    return serverUnits[serverUnit];
  }

  static writeUnit(clientUnit) {
    let unit = clientUnit;
    _.each(Object.keys(serverUnits), (key) => {
      if(serverUnits[key] === clientUnit) {
        unit = key;
      }
    });
    return unit;
  }

  static convertValue(value) {
    return {
      withUnit: unit => {
        let canonical = units[unit].canonical;

        return {
          toUnit       : tUnit => {
            if(!units.hasOwnProperty(unit) || !units.hasOwnProperty(tUnit)) {
              return [value, unit];
            }
            if(canonical !== units[tUnit].canonical) {
              return [value, unit];
            }

            let nValue = value * conversions[canonical][tUnit] / conversions[canonical][unit];

            return [round(nValue), tUnit];
          },
          units        : Object.keys(conversions[canonical]),
          dropdownUnits: _.map(Object.keys(conversions[canonical]),
                               e => ({ key: e, value: e, text: UnitConverter.renderUnit(e) }))
        };
      }
    }
  }
}