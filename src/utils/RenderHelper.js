import _ from 'lodash';
import moment from 'moment/moment';
import { Icon, Label, Popup } from 'semantic-ui-react';
import React, { Fragment } from 'react';
import UnitConverter from './UnitConverter';

export default class RenderHelper {
  static renderPrivileges(privileges) {
    return _.map(privileges, e => e.name).join(', ');
  }

  static renderRoles(roles) {
    return _.map(roles, e => e.name).join(', ');
  }

  static renderDate(date) {
    return moment(date)
        .format('HH:mm:ss, DD.MM.YY');
  }

  //noinspection JSUnusedGlobalSymbols
  /** @deprecated */
  static renderCause(cause) {
    switch(cause) {
    case 'DEFECT':
      return <Fragment><Icon color={'red'} name={'warning circle'} /> Брак</Fragment>;
    case 'ORDER':
      return <Fragment><Icon color={'green'} name={'check circle'} /> Заказ</Fragment>;
    case 'INCOME':
      return <Fragment><Icon color={'blue'} name={'arrow circle down'} /> Пополнение</Fragment>;
    case 'EDIT':
      return <Fragment><Icon color={'purple'} name={'dot circle'} /> Редактирование</Fragment>;
    default:
      return <Fragment><Icon color={'red'} name={'warning circle'} /> Unknown</Fragment>;
    }

  }

  //noinspection JSUnusedGlobalSymbols
  /** @deprecated */
  static renderOperationType(type) {
    switch(type) {
    case 'INCOME':
      return <Fragment><Icon color={'blue'} name={'arrow circle down'} /> Приходящий</Fragment>;
    case 'OUTCOME':
      return <Fragment><Icon color={'red'} name={'arrow circle up'} /> Исходящий</Fragment>;
    default:
      return <Fragment><Icon color={'grey'} name={'cirle question'} /> Неизвестно</Fragment>;
    }
  }

  static renderCost(cost) {
    if(cost === null) {
      return 'KZT';
    }
    return `${this.renderCost(null)} ${Math.ceil(cost)}`;
  }

  static renderInvoiceStatus(status) {
    switch(status) {
    case 'FORMING':
      return <Fragment><Icon color={'grey'} name={'cog'} /> Формируется</Fragment>;
    case 'PENDING':
      return <Fragment><Icon color={'blue'} name={'clock'} /> Ждет оплаты</Fragment>;
    case 'PAID':
      return <Fragment><Icon color={'green'} name={'money'} /> Оплачено</Fragment>;
    case 'CANCELLED':
      return <Fragment><Icon color={'orange'} name={'times circle'} /> Отменено</Fragment>;
    default:
      return <Fragment><Icon color={'red'} name={'warning circle'} /> Неизвестно</Fragment>;
    }
  }

  static withUtils(utils) {
    const _utils = utils;
    return class RHWithUtils {
      static renderUser(user) {
        if(user === _utils.user.username) {
          return <Label color={'blue'}>
            <Icon name={'user'} /> {user} (you)
          </Label>
        } else {
          return <Label>
            <Icon name={'user'} /> {user}
          </Label>
        }
      }

      static renderUserText(user) {
        if(user === _utils.user.username) {
          return <Fragment>
            <Icon name={'user'} /> {user} (you)
          </Fragment>
        } else {
          return <Fragment>
            <Icon name={'user'} /> {user}
          </Fragment>
        }
      }
    }
  }

  static renderOrderType(type) {
    switch(type) {
    case 'STICKER':
      return <Fragment><Icon name={'sticky note'} color={'blue'} /> Стикер</Fragment>;
    case 'WIDE_FORMAT_PRINT':
      return <Fragment><Icon name={'text width'} color={'blue'} /> Широко-форматная</Fragment>;
    case 'DIGITAL_PRINT':
      return <Fragment><Icon name={'file text'} color={'blue'} /> Цифровая печать</Fragment>;
    case 'DESIGN':
      return <Fragment><Icon name={'paint brush'} color={'blue'} /> Дизаин</Fragment>;
    default:
      return <Fragment><Icon color={'red'} name={'warning circle'} /> Неизвестно</Fragment>;
    }
  }

  static renderOrderTypeForDropdown(type) {
    switch(type) {
    case 'STICKER':
      return { icon: <Icon name={'sticky note'} color={'blue'} />, text: 'Стикер' };
    case 'WIDE_FORMAT_PRINT':
      return { icon: <Icon name={'text width'} color={'blue'} />, text: 'Широко-форматная' };
    case 'DIGITAL_PRINT':
      return { icon: <Icon name={'file text'} color={'blue'} />, text: 'Цифровая печать' };
    case 'DESIGN':
      return { icon: <Icon name={'paint brush'} color={'blue'} />, text: 'Дизаин' };
    default:
      return { icon: <Icon color={'red'} name={'warning circle'} />, text: 'Неизвестно' };
    }
  }

  static renderOrderTypeLabel(type) {
    switch(type) {
    case 'STICKER':
      return <Label as={'span'}
                    style={{ marginTop: 3 }}
                    color={'violet'}><Icon name={'sticky note'} /> Стикер</Label>;
    case 'WIDE_FORMAT_PRINT':
      return <Label as={'span'}
                    style={{ marginTop: 3 }}
                    color={'violet'}><Icon name={'text width'} /> Широко-форматная</Label>;
    case 'DIGITAL_PRINT':
      return <Label as={'span'}
                    style={{ marginTop: 3 }}
                    color={'violet'}><Icon name={'file text'} /> Цифровая печать</Label>;
    case 'DESIGN':
      return <Label as={'span'}
                    style={{ marginTop: 3 }}
                    color={'violet'}><Icon name={'paint brush'} /> Дизаин</Label>;
    default:
      return <Label as={'span'}
                    style={{ marginTop: 3 }}
                    color={'red'}><Icon name={'warning sign'} /> Неизвестно</Label>;
    }
  }

  static renderOrderStatus(status) {
    switch (status) {
      case
      'WAITING':
        return <Label as={'span'}
                      style={{marginTop: 3}}
                      color={'violet'}><Icon name={'time'}/> Ожидает</Label>;
      case'PICKING_UP_MATERIALS':
        return <Label as={'span'}
                      style={{marginTop: 3}}
                      color={'violet'}><Icon name={'dolly'}/> Сбор материалов</Label>;
      case'PROCESSING':
        return <Label as={'span'}
                      style={{marginTop: 3}}
                      color={'violet'}><Icon name={'file text'}/> Обработка</Label>;
      case 'PRE_PRINT_WORK':
        return <Label as={'span'}
                      style={{marginTop: 3}}
                      color={'violet'}><Icon name={'backward'}/> Допечатные работы</Label>;
      case 'PRINT':
        return <Label as={'span'}
                      style={{marginTop: 3}}
                      color={'violet'}><Icon name={'print'}/> Печать</Label>;
      case 'POST_PRINT_WORK':
        return <Label as={'span'}
                      style={{marginTop: 3}}
                      color={'violet'}><Icon name={'forward'}/> Послепечатные работы</Label>;
      case'FINISHED':
        return <Label as={'span'}
                      style={{marginTop: 3}}
                      color={'green'}><Icon name={'paint brush'}/> Завершен</Label>;
      case'FAILURE':
        return <Label as={'span'}
                      style={{marginTop: 3}}
                      color={'violet'}><Icon name={'paint brush'}/> Провал</Label>;
      default:
        return <Label as={'span'}
                      style={{marginTop: 3}}
                      color={'red'}><Icon name={'warning sign'}/> Нет статуса</Label>;
    }
  }

  static renderOrderStatusForDropdown(status) {
    switch(status) {
      case
      'WAITING':
        return { icon: <Icon name={'time'} color={'violet'} />, text: 'Ожидает' };
      case'PICKING_UP_MATERIALS':
        return { icon: <Icon name={'dolly'} color={'violet'} />, text: 'Сбор материалов' };
      case'PROCESSING':
        return { icon: <Icon name={'file text'} color={'violet'} />, text: 'Обработка' };
      case 'PRE_PRINT_WORK':
        return { icon: <Icon name={'backward'} color={'violet'} />, text: 'Допечатные работы' };
      case 'PRINT':
        return { icon: <Icon name={'print'} color={'violet'} />, text: 'Печать' };
      case 'POST_PRINT_WORK':
        return { icon: <Icon name={'forward'} color={'violet'} />, text: 'Послепечатные работы' };
      case'FINISHED':
        return { icon: <Icon name={'paint brush'} color={'green'} />, text: 'Завершен' };
      case'FAILURE':
        return { icon: <Icon name={'paint brush'} color={'violet'} />, text: 'Провал' };
      default:
        return { icon: <Icon name={'warning sign'} color={'red'} />, text: 'Нет статуса' };
    }
  }

  static renderPrintWorkType(type) {
    switch(type) {
    case 'PRE':
      return <Fragment><Icon name={'caret square left'} color={'blue'} /> Допечатные работы</Fragment>;
    case 'POST':
      return <Fragment><Icon name={'caret square right'} color={'blue'} /> Послепечатные работы</Fragment>;
    default:
      return <Fragment><Icon color={'red'} name={'warning circle'} /> Неизвестно</Fragment>;
    }
  }

  static renderPrintWorkTypeForDropdown(type) {
    switch(type) {
    case 'PRE':
      return { icon: <Icon name={'caret square left'} color={'blue'} />, text: 'Допечатные работы' };
    case 'POST':
      return { icon: <Icon name={'caret square right'} color={'blue'} />, text: 'Послепечатные работы' };
    default:
      return { icon: <Icon color={'red'} name={'warning circle'} />, text: 'Неизвестно' };
    }
  }

  static renderBlockSize(blockSize) {
    if(!blockSize) return 'unavailable';
    return `${blockSize.divisor} action${blockSize.divisor > 1 ? 's' :
        ''} per ${blockSize.dividend} item${blockSize.dividend > 1 ? 's' : ''}`;
  }

  static renderMaterialLabel(mat) {
    if(mat) {
      return <Label color={'violet'}
                    detail={`${mat.quantity} ${UnitConverter.renderUnit(
                        mat.unit)}`}><Icon name={'warehouse'} />{mat.name}</Label>;
    } else {
      return <Label color={'red'} icon={'warehouse'}>Поврежденный материал</Label>;
    }
  }

  static renderMaterial(mat) {
    if(mat) {
      return <Fragment><Icon name={'warehouse'} color={'violet'} />{mat.name}</Fragment>;
    } else {
      return <Fragment><Icon name={'warehouse'} color={'red'} />неизвестно</Fragment>;
    }
  }

  static renderPattern(pattern) {
    return <Popup
        wide
        content={pattern}
        trigger={<Label color={'teal'} icon={'code'}> Pattern</Label>}
    />;
  }
}