import React, {Component, Fragment} from 'react';
import {Header, Icon, Label, Popup, Segment, Table, TableBody, TableCell, TableRow} from "semantic-ui-react";
import _ from "lodash";

const style = {
  borderRadius: 0.1,
  width: '200px'
};

export default class Man extends Component {

  constructor(props) {
    super(props);
    if (this.props.invoice) {
      this.state = {
        isUser: false,
        name: this.props.invoice.clientName,
        phone: this.props.invoice.clientPhoneNumber,
        email: this.props.invoice.clientEmail,
        clientDescription: this.props.invoice.clientDescription
      }
    } else {
      let {user} = this.props;
      this.state = {
        isUser: true,
        name: user.firstName + ' ' + user.lastName,
        phone: user.phone,
        email: user.email
      };
    }
  }

  async componentDidUpdate(oldProps) {
    if (!_.isEqual(this.props.invoice, oldProps.invoice)) {
      if (this.props.invoice) {
        await this.setState({
          isUser: false,
          name: this.props.invoice.clientName,
          phone: this.props.invoice.clientPhoneNumber,
          email: this.props.invoice.clientEmail,
          clientDescription: this.props.invoice.clientDescription
        })
      } else {
        let {user} = this.props;
        await this.setState({
          isUser: true,
          name: user.firstName + ' ' + user.lastName,
          phone: user.phone,
          email: user.email
        })
      }
    }
  }


  render() {
    return (
      <Popup trigger={this.renderButton()} flowing hoverable
             style={style}
             content={
               <Fragment>
                 <Header as='h4' content={this.state.isUser ? "Пользователь" : "Клиент"}/>

                 <Segment inverted color='green' compact>
                   <Table color={'green'} inverted>
                     <TableBody>
                       <TableRow>
                         {this.state.name ?
                           <TableCell textAlign='left'>
                             <p><b>Имя:</b> {this.state.name}</p>
                           </TableCell>
                           : <Fragment/>}
                       </TableRow>
                       {this.state.email ?
                         <TableRow>
                           <TableCell textAlign='left'>
                             <p><b>Почта:</b> {this.state.email}</p>
                           </TableCell>
                         </TableRow>
                         : <Fragment/>}
                       {this.state.phone ?
                         <TableRow>
                           <TableCell>
                             <p><b>Телефон:</b> {this.state.phone}</p>
                           </TableCell>
                         </TableRow>
                         : <Fragment/>
                       }
                       {this.state.clientDescription ?
                         <TableRow>
                           <TableCell>
                             <p><b>Описание: </b>{this.state.clientDescription}</p>
                           </TableCell>
                         </TableRow>
                         : <Fragment/>
                       }
                     </TableBody>
                   </Table>

                 </Segment>
               </Fragment>}
      />

    )
  };

  async modalLoading(state) {
    await this.setState({modalLoading: state});
  };

  async modalOpened(state) {
    await this.setState({modalOpened: state});
  };

  renderButton() {
    return (<Label as={'span'}
                   style={{marginTop: 3}}
                   onClick={async () => await this.modalOpened(true)}
                   color={'blue'}
      >
        <Icon name={'user'}
              icon={'user'}
        />
        {this.state.name}
      </Label>
    )
      ;
  };
}