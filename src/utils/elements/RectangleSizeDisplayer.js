import _ from 'lodash';
import React, { Component, Fragment } from 'react';
import { Dropdown } from 'semantic-ui-react';
import UnitConverter from '../UnitConverter';

export default class UnitValueDisplayer extends Component {
  constructor(props) {

    super(props);

    this.state = {
      variants: [],
      width   : 0,
      height  : 0,
      unit    : ''
    };

    this.update = this.update.bind(this);
  }

  async componentDidMount() {
    await this.update();
  }

  async componentDidUpdate(prevProps) {
    if(!_.isEqual(this.props, prevProps)) {
      let { width, height, unit } = this.props;
      this.setState({ width, height, unit });
    }
  }

  async update() {
    let { width, height, unit } = this.props;
    let cW                      = UnitConverter.convertValue(width).withUnit(unit);
    let cH                      = UnitConverter.convertValue(height).withUnit(unit);
    let variants                = [];

    for(let _unit of cW.units) {
      let [uW, u] = cW.toUnit(_unit);
      let [uH]    = cH.toUnit(_unit);
      uW          = UnitConverter.renderUnit(uW);
      uH          = UnitConverter.renderUnit(uH);

      variants.push(
          { key: _unit, value: _unit, text: <Fragment>{uW} {u} &times; {uH} {u}</Fragment> }
      );
    }
    await this.setState({ unit, variants });
  }

  render() {
    return (<Dropdown inline options={this.state.variants}
                      value={this.state.unit} onChange={(e, { value }) => this.setState({ unit: value })} />);
  }
}