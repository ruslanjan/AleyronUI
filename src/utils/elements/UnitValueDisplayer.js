import React, { Component, Fragment } from 'react';
import { Dropdown } from 'semantic-ui-react';
import UnitConverter from '../UnitConverter';

//noinspection JSUnusedGlobalSymbols
export default class UnitValueDisplayer extends Component {
  constructor(props) {

    super(props);

    this.state = {
      variants: [],
      value   : '',
      unit    : ''
    };

    this.update = this.update.bind(this);
  }

  async componentDidMount() {
    await this.update();
  }

  async componentDidUpdate(prevProps) {
    let { value, unit } = this.props;

    if(value !== prevProps.value || unit !== prevProps.unit) {
      await this.update();
    }
  }

  async update() {
    let { value, unit } = this.props;
    let converter       = UnitConverter.convertValue(value).withUnit(unit);
    let variants        = [];

    for(let _unit of converter.units) {
      let [uV, u] = converter.toUnit(_unit);
      u           = UnitConverter.renderUnit(u);

      variants.push(
          { key: _unit, value: _unit, text: <Fragment>{uV} {u}</Fragment> }
      );
    }
    await this.setState({ unit, variants });
  }

  render() {
    return (<Dropdown inline options={this.state.variants}
                      value={this.state.unit} onChange={(e, { value }) => this.setState({ unit: value })} />);
  }
}