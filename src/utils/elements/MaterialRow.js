import React, {Component} from "react";
import {TableCell, TableRow} from "semantic-ui-react";

export default class MaterialRow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      material: this.props.material,
    }
  };

  componentDidMount() {
    this.setState({material: this.props.material});
  };

  render() {
    let {material} = this.state;
    if (material.item) {
      return (<TableRow key={material.id}>
        <TableCell content={material.id}/><TableCell content={material.item.name}/><TableCell
        content={material.quantity}/>
      </TableRow>)
    } else {
      return (<TableRow key={material.id}>
        <TableCell colSpan={3} content={'Материал поврежден'}/>
      </TableRow>)
    }
  };
}